package szczepanski.gerard.cargo.simulator.simulator.domain.payment;

import java.time.LocalDateTime;

import lombok.Builder;
import lombok.Getter;

/**
 * Object value that represents history record for payment.
 */
@Getter
@Builder
public class MoneyTransactionHistoryRecord {

	private final LocalDateTime date;
	private final String title;
	private final Money amount;
	private final Money balanceBeforePayment;
	private final Money balanceAfterPayment;
	private final MoneyTransactionType paymentType;
	
}
