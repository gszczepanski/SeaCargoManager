package szczepanski.gerard.cargo.simulator.simulator.domain.location;

import java.util.Map;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModel;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.Currency;

@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class Country extends AbstractModel {
	private static final long serialVersionUID = 3837502907110483942L;
	
	private final String code;
	private final String name;
	
	private final Map<String, City> cities;
	private final Currency currency;
	
	void addCity(City city) {
		cities.put(city.getName(), city);
	}
}
