package szczepanski.gerard.cargo.simulator.simulator.domain.cargo;

public enum ProductUnit {
	
	QUANTITY,
	WEIGHT;
	
}
