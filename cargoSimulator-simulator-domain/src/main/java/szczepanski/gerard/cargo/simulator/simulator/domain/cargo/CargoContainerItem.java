package szczepanski.gerard.cargo.simulator.simulator.domain.cargo;

import java.math.BigDecimal;

import lombok.Getter;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.Dimensions;

@Getter
public abstract class CargoContainerItem {
	
	protected Dimensions dimensions;
	protected BigDecimal weight;
	
}
