package szczepanski.gerard.cargo.simulator.simulator.domain.route;

import org.springframework.stereotype.Component;

import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModelRepository;

@Component
public class RouteRepository extends AbstractModelRepository<Route> {

}
