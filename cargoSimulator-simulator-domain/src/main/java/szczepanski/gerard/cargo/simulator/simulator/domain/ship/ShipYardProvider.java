package szczepanski.gerard.cargo.simulator.simulator.domain.ship;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
public class ShipYardProvider {
	
	@Getter
	@Setter
	private ShipYard shipYard;

	public ShipYardProvider() {
		super();
	}
	
}
