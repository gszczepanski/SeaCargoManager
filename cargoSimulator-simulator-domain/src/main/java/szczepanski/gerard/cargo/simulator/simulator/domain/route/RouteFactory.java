package szczepanski.gerard.cargo.simulator.simulator.domain.route;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;
import szczepanski.gerard.cargo.simulator.common.util.ParamAssertion;
import szczepanski.gerard.cargo.simulator.simulator.domain.location.LocationPoint;
import szczepanski.gerard.cargo.simulator.simulator.domain.port.Port;

@Getter
@Setter
@Component
public class RouteFactory {
	
	private static final int MINIMUM_NUMBER_OF_PORTS_TO_CREATE_ROUTE = 2;
	
	private RoutesGraph routesGraph;
	
	public Route generateRoute(LocalDateTime routeStartDate, List<Port> routePorts) {
		ParamAssertion.collectionContainsAtLeastElements(routePorts, MINIMUM_NUMBER_OF_PORTS_TO_CREATE_ROUTE);
		List<LocationPoint> routePoints = generateRoutePoints(routePorts);
		return new Route(routePorts, routePoints, routeStartDate);
	}
	
	private List<LocationPoint> generateRoutePoints(List<Port> routePorts) {
		List<LocationPoint> portsPoints = routePorts.stream().map(p -> p.getCity().getLocationPoint()).collect(Collectors.toList());
		return routesGraph.generateRoutePointsList(portsPoints);
	}
	
}
