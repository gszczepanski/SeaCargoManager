package szczepanski.gerard.cargo.simulator.simulator.domain.bank;

import szczepanski.gerard.cargo.simulator.common.collections.CollectionFactory;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.Money;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.MoneyOwner;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.MoneyTransactionHistoryRecord;

import java.util.List;

public class Bank implements MoneyOwner {

    private Money balance;
    private final List<MoneyTransactionHistoryRecord> paymentHistory = CollectionFactory.list();

    public Bank(Money balance) {
        this.balance = balance;
    }

    @Override
    public boolean canPay(Money amountOfMoney) {
        if (balance.getCurrency().equals(amountOfMoney.getCurrency())) {
            return balance.getValue().doubleValue() >= amountOfMoney.getValue().doubleValue();
        }
        return false;
    }

    @Override
    public void addTransactionHistory(MoneyTransactionHistoryRecord record) {
        paymentHistory.add(record);
    }

    @Override
    public void addMoney(Money money) {
        this.balance = balance.add(money);
    }

    @Override
    public void subtractMoney(Money money) {
        this.balance = balance.substract(money);
    }

    @Override
    public Money getCurrentBalance() {
        return balance;
    }

}
