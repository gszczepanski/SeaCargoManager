package szczepanski.gerard.cargo.simulator.simulator.domain.ship;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModelRepository;

@Component
public class ShipRepository extends AbstractModelRepository<Ship> {

	public List<Ship> findAllSaillingShips() {
		return dataSource.values().stream().filter(s -> s.getState() == ShipState.SAILING).collect(Collectors.toList());
	}
	
	public boolean isRouteInAnyShip(String routeUuid) {
		for (Ship ship: getAll()) {
			Optional<String> optionalCurrentRouteUuid = ship.getCurrentRouteUuid();  
			if (optionalCurrentRouteUuid.isPresent()) {
				if (optionalCurrentRouteUuid.get().equals(routeUuid)) {
					return true;
				}
			}
		}
		
		return false;
	}

}
