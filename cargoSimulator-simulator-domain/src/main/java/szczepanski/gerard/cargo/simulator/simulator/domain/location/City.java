package szczepanski.gerard.cargo.simulator.simulator.domain.location;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModel;

@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class City extends AbstractModel {
	private static final long serialVersionUID = -1570500652191087836L;
	
	private final String name;
	private final Country country;
	private final LocationPoint locationPoint;
	
	private int numberOfCitizens;
}
