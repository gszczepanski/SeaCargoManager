package szczepanski.gerard.cargo.simulator.simulator.domain.common;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import szczepanski.gerard.cargo.simulator.common.collections.CollectionFactory;

public abstract class AbstractModelRepository<M extends AbstractModel> {
	
	protected final Map<String, M> dataSource = CollectionFactory.map();
	
	public void save(M value) {
		String id = value.getUuid();
		dataSource.put(id, value);
	}
	
	public void saveMany(Iterable<M> values) {
		values.forEach(this::save);
	}
	
	public M findOne(String id) {
		return dataSource.get(id);
	}
	
	public List<M> getByIds(List<String> ids) {
		return ids.stream().map(dataSource::get).collect(Collectors.toList());
	}
	
	public Collection<M> getAll() {
		return dataSource.values();
	}
	
	public List<M> getAll(Comparator<M> sort) {
		return dataSource.values().stream().sorted(sort).collect(Collectors.toList());
	}
	
	public boolean isInRepository(String uuid) {
		return dataSource.containsKey(uuid);
	}
	
	
}
