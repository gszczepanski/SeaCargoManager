package szczepanski.gerard.cargo.simulator.simulator.domain.company;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import szczepanski.gerard.cargo.simulator.simulator.domain.location.City;
import szczepanski.gerard.cargo.simulator.simulator.domain.person.Person;

@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class BasicCompanyInfo {
	
	private final CompanyType companyType;
	private final City city;
	private String name;
	private int reputationPoints;
	private String companyColor;
	
	@Setter
	private Person ceo;
	
	public void addReputationPoints(int reputationPoints) {
		this.reputationPoints += reputationPoints;
	}
	
	public void substractReputationPoints(int reputationPoints) {
		this.reputationPoints -= reputationPoints;
	}
	
}
