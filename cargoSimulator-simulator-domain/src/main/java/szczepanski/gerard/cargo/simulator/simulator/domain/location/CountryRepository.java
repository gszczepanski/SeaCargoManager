package szczepanski.gerard.cargo.simulator.simulator.domain.location;

import org.springframework.stereotype.Component;

import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModelRepository;

@Component
public class CountryRepository extends AbstractModelRepository<Country> { 
	
}
