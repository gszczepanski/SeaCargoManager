package szczepanski.gerard.cargo.simulator.simulator.domain.message;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModelRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.time.SimulatorTime;

@Component
public class MessageRepository extends AbstractModelRepository<Message> {

	public List<Message> getUnreadMessagesFromLatestHours(int numberOfHours) {
		LocalDateTime maxPastTime = SimulatorTime.getTime().minus(numberOfHours - 1, ChronoUnit.HOURS);
		return getAll().stream()
				.filter(m -> m.getTime().isAfter(maxPastTime) && !m.isReaded())
				.sorted((m1, m2) -> m1.getTime().compareTo(m2.getTime()))
				.collect(Collectors.toList());
	}
	
	public int getNumberOfUnreadedMessages() {
		return (int) getAll().stream()
				.filter(m -> !m.isReaded())
				.count();
	}

}
