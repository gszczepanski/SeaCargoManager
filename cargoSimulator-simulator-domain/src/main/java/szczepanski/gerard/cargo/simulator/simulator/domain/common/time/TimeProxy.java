package szczepanski.gerard.cargo.simulator.simulator.domain.common.time;

import java.time.LocalDateTime;

import org.springframework.stereotype.Component;

@Component
public class TimeProxy {

	public void updateTime(LocalDateTime time) {
		SimulatorTime.instance().updateTime(time);
	}
}
