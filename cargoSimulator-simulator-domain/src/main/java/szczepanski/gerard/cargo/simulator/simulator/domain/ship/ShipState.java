package szczepanski.gerard.cargo.simulator.simulator.domain.ship;

public enum ShipState {
	
	DOCKED,
	SAILING;
	
}
