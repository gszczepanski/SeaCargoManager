package szczepanski.gerard.cargo.simulator.simulator.domain.cargo;

import java.math.BigDecimal;
import java.util.List;

import szczepanski.gerard.cargo.simulator.common.collections.CollectionFactory;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.Dimensions;

public class CargoContainer {
	private static final BigDecimal INITIAL_WEIGHT = new BigDecimal("0.00");
	
	private final String containerNo;
	private final Dimensions dimensions;
	
	private BigDecimal containerWeight = INITIAL_WEIGHT;
	private final List<CargoContainerItem> items = CollectionFactory.list();

	CargoContainer(String containerNo, Dimensions dimensions) {
		this.containerNo = containerNo;
		this.dimensions = dimensions;
	}
	
	public int countMaxCapacityForItem(CargoContainerItem item) {
		if (!dimensions.willFit(item.getDimensions())) {
			return 0;
		}
		
		double containerArea = dimensions.twoDimArea();
		
		
		return 1;
	}
	
}
