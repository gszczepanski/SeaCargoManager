package szczepanski.gerard.cargo.simulator.simulator.domain.cargo;

import java.math.BigDecimal;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModel;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.Dimensions;

@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class Product extends AbstractModel {
	private static final long serialVersionUID = 1720425823458806927L;
	
	private final ProductType productType;
	private final String productName;
	private final ProductUnit productUnit;
	private final Dimensions dimensions;
	private final BigDecimal averageWeight;
	private final BigDecimal averagePricePerUnitInUSD;
	
}
