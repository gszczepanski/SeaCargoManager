package szczepanski.gerard.cargo.simulator.simulator.domain.bank;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BankProvider {
	
	private static Bank BANK;

	public static Bank get() {
		return BANK;
	}
	
	public static void set(Bank bank) {
		BANK = bank; 
	}
	
}
