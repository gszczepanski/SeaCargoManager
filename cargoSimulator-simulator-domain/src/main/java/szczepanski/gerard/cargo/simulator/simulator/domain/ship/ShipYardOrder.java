package szczepanski.gerard.cargo.simulator.simulator.domain.ship;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.simulator.domain.company.SeaTransportCompany;
import szczepanski.gerard.cargo.simulator.simulator.domain.port.Port;

@Getter
@RequiredArgsConstructor
public class ShipYardOrder {
	
	private final ShipModel shipModel;
	private final SeaTransportCompany company;
	private final Port startPort;
	private final LocalDateTime buildTime;
	
}
