package szczepanski.gerard.cargo.simulator.simulator.domain.company;

public enum CompanyType {
	
	TRADING,
	SEA_TRANSPORT, 
	LAND_TRANSPORT;
	
}
