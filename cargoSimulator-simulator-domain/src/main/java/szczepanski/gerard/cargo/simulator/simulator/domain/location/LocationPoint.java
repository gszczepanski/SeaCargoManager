package szczepanski.gerard.cargo.simulator.simulator.domain.location;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class LocationPoint implements Serializable {
	private static final long serialVersionUID = 140974836495096286L;

	private static final int METER = 1000;

	private final double latitude;
	private final double longitude;

	public double calculateDistanceToLocationPoint(LocationPoint other) {
		double theta = longitude - other.getLongitude();

		double distance = Math.sin(degreeToRadians(latitude)) * Math.sin(degreeToRadians(other.getLatitude()))
				+ Math.cos(degreeToRadians(latitude)) * Math.cos(degreeToRadians(other.getLatitude())) * Math.cos(degreeToRadians(theta));
		distance = Math.acos(distance);
		distance = radiansToDegree(distance);
		distance = distance * 60 * 1.1515 * 1.609344;

		return Math.round(distance * METER);
	}

	private double degreeToRadians(double degree) {
		return degree * (Math.PI / 180);
	}

	private double radiansToDegree(double radians) {
		return radians * 180 / Math.PI;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(latitude);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(longitude);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LocationPoint other = (LocationPoint) obj;
		if (Double.doubleToLongBits(latitude) != Double.doubleToLongBits(other.latitude))
			return false;
		if (Double.doubleToLongBits(longitude) != Double.doubleToLongBits(other.longitude))
			return false;
		return true;
	}

}
