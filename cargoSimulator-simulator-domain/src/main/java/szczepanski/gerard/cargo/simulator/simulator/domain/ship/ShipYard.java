package szczepanski.gerard.cargo.simulator.simulator.domain.ship;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import szczepanski.gerard.cargo.simulator.common.collections.CollectionFactory;
import szczepanski.gerard.cargo.simulator.common.eventbus.EventBusProvider;
import szczepanski.gerard.cargo.simulator.common.util.ParamAssertion;
import szczepanski.gerard.cargo.simulator.simulator.domain.bank.BankProvider;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.time.SimulatorTime;
import szczepanski.gerard.cargo.simulator.simulator.domain.company.SeaTransportCompany;
import szczepanski.gerard.cargo.simulator.simulator.domain.message.MessageEvent;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.MoneyBookkeeper;
import szczepanski.gerard.cargo.simulator.simulator.domain.port.Port;

public class ShipYard {
	
	private final List<ShipYardOrder> orders = CollectionFactory.list();
	
	public LocalDateTime buyShip(ShipModel shipModel, SeaTransportCompany company, Port startPort) {
		ParamAssertion.isNotNull(shipModel, company, startPort);
		MoneyBookkeeper.createMoneyTransaction(shipModel.getPrice(), company.getCompanyFinancesInfo(), BankProvider.get(), "Buy Ship " + shipModel.getName());
		
		int buildDays = shipModel.countBuildDays();
		LocalDateTime finishBuildingTime = SimulatorTime.getTime().plusDays(buildDays).truncatedTo(ChronoUnit.HOURS);
		
		ShipYardOrder order = new ShipYardOrder(shipModel, company, startPort, finishBuildingTime);
		orders.add(order);
		
		return finishBuildingTime;
	}
	
	public void processBuilding() {
		
		orders.removeIf(order -> {
			if (SimulatorTime.getTime().isAfter(order.getBuildTime()) || SimulatorTime.getTime().isEqual(order.getBuildTime())) {
				buildShip(order);
				return true;
			}
			
			return false;
		});
	}
	
	private void buildShip(ShipYardOrder order) {
		Port startPort = order.getStartPort();
		ShipModel shipModel = order.getShipModel();
		SeaTransportCompany company = order.getCompany();
		
		Ship createdShip = new Ship(shipModel, company);
		startPort.getDockedShips().add(createdShip);
		createdShip.setDockedToPort(startPort);
		
		int reputationPoitsForBuy = shipModel.getReputationPointsForBuy();
		company.getCompanyInfo().addReputationPoints(reputationPoitsForBuy);
		
		EventBusProvider.get().post(new ShipYardShipCreatedEvent(createdShip));
		EventBusProvider.get().post(new MessageEvent("ship.yard.create.ship", createdShip.getUuid(), startPort.getName()));
	}
	
}
