package szczepanski.gerard.cargo.simulator.simulator.domain.route;

import java.time.LocalDateTime;
import java.util.List;

import szczepanski.gerard.cargo.simulator.simulator.domain.location.City;
import szczepanski.gerard.cargo.simulator.simulator.domain.order.OrderedProduct;

public class Supply {
	
	private LocalDateTime deliveryDate;
	private City destinationCity;
	private List<OrderedProduct> supplyProducts;
	
}
