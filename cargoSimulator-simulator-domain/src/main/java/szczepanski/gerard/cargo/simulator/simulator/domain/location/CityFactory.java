package szczepanski.gerard.cargo.simulator.simulator.domain.location;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.util.XmlUtils;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModelFactory;

/**
 * 
 * @author Gerard Szczepanski
 */
@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CityFactory extends AbstractModelFactory<City> {

	private static final String UUID = "uuid";
	private static final String NAME = "name";
	private static final String COUNTRY_ID = "countryId";
	private static final String LATITUDE = "latitude";
	private static final String LONGITUDE = "longitude";
	private static final String NUMBER_OF_CITIZENS = "numberOfCitizens";
	
	private final CountryRepository countryRepository;

	@Override
	public City createFromXmlElement(Element element) {
		Country country = countryRepository.findOne(XmlUtils.getTagValue(COUNTRY_ID, element));
		LocationPoint locationPoint = getLocationPoint(element);
		int numberOfCitizens = Integer.valueOf(XmlUtils.getTagValue(NUMBER_OF_CITIZENS, element));
		
		City city = City.builder()
				.name(xmlValue(NAME, element))
				.country(country)
				.locationPoint(locationPoint)
				.numberOfCitizens(numberOfCitizens)
				.build();
		
		city.setUuid(xmlValue(UUID, element));
		country.addCity(city);
		
		return city;
	}
	
	private LocationPoint getLocationPoint(Element element) {
		double latitude = Double.valueOf(xmlValue(LATITUDE, element));
		double longitude = Double.valueOf(xmlValue(LONGITUDE, element));
		
		return new LocationPoint(latitude, longitude);
	}

}
