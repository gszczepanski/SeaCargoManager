package szczepanski.gerard.cargo.simulator.simulator.domain.company;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.util.RandomUtils;
import szczepanski.gerard.cargo.simulator.common.util.XmlUtils;
import szczepanski.gerard.cargo.simulator.simulator.domain.location.City;
import szczepanski.gerard.cargo.simulator.simulator.domain.location.CityRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.person.Person;
import szczepanski.gerard.cargo.simulator.simulator.domain.person.PersonRepository;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BasicCompanyInfoFactory {

	private static final String NAME = "name";
	private static final String CITY_ID = "cityId";
	private static final String REPUTATION_POINTS = "reputationPoints";
	private static final String COMPANY_TYPE = "companyType";
	private static final String CEO_PERSON_ID = "ceoPersonId";


	private final PersonRepository personRepository;
	private final CityRepository cityRepository;

	public BasicCompanyInfo createFromXmlElement(Element element) {
		City city = cityRepository.findOne(XmlUtils.getTagValue(CITY_ID, element));
		Person person = personRepository.findOne(XmlUtils.getTagValue(CEO_PERSON_ID, element));
		Integer reputationPoints = Integer.valueOf(XmlUtils.getTagValue(REPUTATION_POINTS, element));
		CompanyType companyType = CompanyType.valueOf(XmlUtils.getTagValue(COMPANY_TYPE, element));
		
		return BasicCompanyInfo.builder()
				.name(XmlUtils.getTagValue(NAME, element))
				.reputationPoints(reputationPoints)
				.companyType(companyType)
				.city(city)
				.ceo(person)
				.companyColor(RandomUtils.randomColor())
				.build();
	}

}
