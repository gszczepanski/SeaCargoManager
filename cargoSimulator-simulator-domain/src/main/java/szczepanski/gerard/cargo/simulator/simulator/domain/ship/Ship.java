package szczepanski.gerard.cargo.simulator.simulator.domain.ship;

import java.io.Serializable;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.log4j.Logger;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import szczepanski.gerard.cargo.simulator.common.config.UuidPrefix;
import szczepanski.gerard.cargo.simulator.common.eventbus.EventBusProvider;
import szczepanski.gerard.cargo.simulator.common.util.ParamAssertion;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModel;
import szczepanski.gerard.cargo.simulator.simulator.domain.company.SeaTransportCompany;
import szczepanski.gerard.cargo.simulator.simulator.domain.location.LocationPoint;
import szczepanski.gerard.cargo.simulator.simulator.domain.message.MessageEvent;
import szczepanski.gerard.cargo.simulator.simulator.domain.port.Port;
import szczepanski.gerard.cargo.simulator.simulator.domain.route.Route;
import szczepanski.gerard.cargo.simulator.simulator.domain.route.RouteInformationObject;
import szczepanski.gerard.cargo.simulator.simulator.domain.route.RouteState;

public class Ship extends AbstractModel {
	private static final long serialVersionUID = 5059936252406787887L;

	private static final Logger LOG = Logger.getLogger(Ship.class);
	private static final AtomicLong SHIP_UUID_PROVIDER = new AtomicLong();

	@Getter
	private final SeaTransportCompany ownerCompany;
	@Getter
	private final ShipModel shipModel;
	private final ShipEngine shipEngine;
	@Getter
	private ShipState state = ShipState.DOCKED;
	private Route route;
	@Getter @Setter
	private Port dockedToPort;

	public Ship(ShipModel shipModel, SeaTransportCompany ownerCompany) {
		setUuid(UuidPrefix.SHIP_UUID_PREFIX + SHIP_UUID_PROVIDER.incrementAndGet());
		
		this.shipModel = shipModel;
		this.ownerCompany = ownerCompany;
		this.shipEngine = new ShipEngine(shipModel.getMaximumSpeed() * 1000);
		
		ownerCompany.getSeaTransportCompanyOwnership().getShips().add(this);
	}

	public void setRoute(Route route) {
		ParamAssertion.isNotNull(route);
		this.route = route;
	}

	public void sail() {
		if (state == ShipState.DOCKED || route.isFinished()) {
			return;
		}

		double distance = shipEngine.calculateMeterTravelledInHour();
		route.addTravelledDistanceToRoute(distance);

		if (route.isFinished()) {
			state = ShipState.DOCKED;
			Port destinationPort = route.destinationPort();
			destinationPort.dockShip(this);
		}
	}

	public void startCourse() {
		if (canStartCourse()) {
			LOG.debug(String.format("%s Ship %s is starting the course from %s to %s. Total kilometers to sail: %s", route.getRouteStartDate(), getUuid(),
					route.startPort().getName(), route.destinationPort().getName(), (route.getTotalDistance() / 1000)));
			
			route.startRoute();
			Port startPort = route.startPort();
			startPort.launchOutShipFromPort(this);
			state = ShipState.SAILING;

			EventBusProvider.get().post(new MessageEvent("ship.is.starting.course", getUuid(), route.startPort().getName(), route.destinationPort().getName(),
					(route.getTotalDistance() / 1000)));
		}
	}

	private boolean canStartCourse() {
		if (route == null || route.getRouteState() == RouteState.FINISHED) {
			return false;
		}
		if (route.getRouteState() == RouteState.PLANNED && !route.shoudRouteBeStarted()) {
			return false;
		}

		return true;
	}

	public LocationPoint currentPosition() {
		return route.currentPosition();
	}

	protected Optional<String> getCurrentRouteUuid() {
		if (route != null) {
			return Optional.of(route.getUuid());
		}

		return Optional.empty();
	}

	public ShipInformationObject informationObject() {
		RouteInformationObject routeInformationObject = null;
		if (route != null) {
			routeInformationObject = route.informationObject();
		}
		return ShipInformationObject.builder().uuid(getUuid()).state(state.toString()).routeInfoObj(Optional.ofNullable(routeInformationObject)).build();
	}

	@RequiredArgsConstructor
	private static class ShipEngine implements Serializable {
		private static final long serialVersionUID = -1553331254778118376L;

		private final double maximumSpeed;
		private double speed = 0;

		public double calculateMeterTravelledInHour() {
			speed = maximumSpeed; // TODO calculate hour speed
			return speed;
		}
	}

	@Override
	public String toString() {
		String routeUuid = "ROUTE NOT ASSIGNED";

		if (route != null) {
			routeUuid = route.getUuid();
		}

		return "Ship [uuid=" + getUuid() + ", company=" + ownerCompany.getUuid() + ", state=" + state + ", routeUuid=" + routeUuid + "]";
	}

}
