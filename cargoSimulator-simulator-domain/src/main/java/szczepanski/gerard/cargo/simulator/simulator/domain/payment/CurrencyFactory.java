package szczepanski.gerard.cargo.simulator.simulator.domain.payment;

import java.math.BigDecimal;

import org.springframework.stereotype.Component;
import org.w3c.dom.Element;

import szczepanski.gerard.cargo.simulator.common.util.XmlUtils;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModelFactory;

@Component
public class CurrencyFactory extends AbstractModelFactory<Currency> {
	
	private static final String UUID = "uuid";
	private static final String CODE = "code";
	private static final String VALUE_OF_DOLLAR = "valueOfDollar";

	@Override
	public Currency createFromXmlElement(Element element) {
		BigDecimal valueOfDollar = new BigDecimal(XmlUtils.getTagValue(VALUE_OF_DOLLAR, element));
		
		Currency currency = Currency.builder()
				.code(XmlUtils.getTagValue(CODE, element))
				.valueOfDollar(valueOfDollar)
				.build();		
		currency.setUuid(XmlUtils.getTagValue(UUID, element));
		
		return currency;
	}

}
