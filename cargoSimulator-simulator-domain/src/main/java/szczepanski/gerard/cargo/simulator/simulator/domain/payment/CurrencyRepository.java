package szczepanski.gerard.cargo.simulator.simulator.domain.payment;

import org.springframework.stereotype.Component;

import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModelRepository;

@Component
public class CurrencyRepository extends AbstractModelRepository<Currency>{

}
