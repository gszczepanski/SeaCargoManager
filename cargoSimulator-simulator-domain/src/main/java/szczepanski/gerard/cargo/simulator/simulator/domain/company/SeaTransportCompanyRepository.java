package szczepanski.gerard.cargo.simulator.simulator.domain.company;

import org.springframework.stereotype.Component;

import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModelRepository;

@Component
public class SeaTransportCompanyRepository extends AbstractModelRepository<SeaTransportCompany> { 
	
}
