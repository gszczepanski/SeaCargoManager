package szczepanski.gerard.cargo.simulator.simulator.domain.company;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModel;

@Getter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public abstract class Company extends AbstractModel {
	private static final long serialVersionUID = 3571731389789897976L;
	
	protected BasicCompanyInfo companyInfo;
	protected CompanyFinancesInfo companyFinancesInfo;

}
