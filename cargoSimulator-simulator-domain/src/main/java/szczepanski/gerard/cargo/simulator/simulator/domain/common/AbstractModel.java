package szczepanski.gerard.cargo.simulator.simulator.domain.common;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public abstract class AbstractModel implements Serializable {
	private static final long serialVersionUID = 1162406797658512370L;
	
	private String uuid;
	
}
