package szczepanski.gerard.cargo.simulator.simulator.domain.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Dimensions {
	
	private final double L;
	private final double W;
	private final double H;
	
	public boolean willFit(Dimensions other) {
		return other.getL() < L && other.getW() < W && other.getH() < H;
	}
	
	public double twoDimArea() {
		return L * W;
	}
	
}

