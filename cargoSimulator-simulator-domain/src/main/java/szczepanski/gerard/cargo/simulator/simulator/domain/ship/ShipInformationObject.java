package szczepanski.gerard.cargo.simulator.simulator.domain.ship;

import java.util.Optional;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import szczepanski.gerard.cargo.simulator.simulator.domain.route.RouteInformationObject;

@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class ShipInformationObject {
	
	private final String uuid;
	private final String state;
	
	private Optional<RouteInformationObject> routeInfoObj;
}
