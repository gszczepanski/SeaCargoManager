package szczepanski.gerard.cargo.simulator.simulator.domain.person;

import java.time.LocalDate;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModel;

@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class Person extends AbstractModel {
	private static final long serialVersionUID = -3095406005356337783L;
	
	private final String firstName;
	private final String lastName;
	private final Nationality nationality;
	private final LocalDate birthDate;
	
}
