package szczepanski.gerard.cargo.simulator.simulator.domain.person;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModelFactory;

/**
 * 
 * @author Gerard Szczepanski
 */
@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PersonFactory extends AbstractModelFactory<Person> {

	private static final String UUID = "uuid";
	private static final String FIRST_NAME = "firstName";
	private static final String LAST_NAME = "firstName";
	private static final String NATIONALITY_ID = "nationalityId";
	private static final String BIRTH_DATE = "birthDate";
	
	private final NationalityRepository nationalityRepository;

	@Override
	public Person createFromXmlElement(Element element) {
		Nationality nationality = nationalityRepository.findOne(xmlValue(NATIONALITY_ID, element));
		LocalDate birthDate = LocalDate.parse(xmlValue(BIRTH_DATE, element));
		
		Person person = Person.builder()
				.firstName(xmlValue(FIRST_NAME, element))
				.lastName(xmlValue(LAST_NAME, element))
				.nationality(nationality)
				.birthDate(birthDate)
				.build();
		
		person.setUuid(xmlValue(UUID, element));
		return person;
	}
}
