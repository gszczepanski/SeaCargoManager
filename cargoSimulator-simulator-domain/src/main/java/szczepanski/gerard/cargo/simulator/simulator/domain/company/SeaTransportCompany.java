package szczepanski.gerard.cargo.simulator.simulator.domain.company;

import lombok.Getter;

@Getter
public class SeaTransportCompany extends Company {
	private static final long serialVersionUID = -8545257769584146511L;
	
	private final SeaTransportCompanyOwnership seaTransportCompanyOwnership;
	
	SeaTransportCompany(BasicCompanyInfo companyInfo, CompanyFinancesInfo companyFinancesInfo,
			SeaTransportCompanyOwnership seaTransportCompanyOwnership) {
		super(companyInfo, companyFinancesInfo);
		this.seaTransportCompanyOwnership = seaTransportCompanyOwnership;
	}

	static SeaTransportCompanyBuilder builder() {
		return new SeaTransportCompanyBuilder();
	}
	
	static class SeaTransportCompanyBuilder {
		
		private BasicCompanyInfo companyInfo;
		private CompanyFinancesInfo companyFinancesInfo;
		private SeaTransportCompanyOwnership seaTransportCompanyOwnership;
		
		public SeaTransportCompanyBuilder companyInfo(BasicCompanyInfo companyInfo) {
			this.companyInfo = companyInfo;
			return this;
		}
		
		public SeaTransportCompanyBuilder companyFinancesInfo(CompanyFinancesInfo companyFinancesInfo) {
			this.companyFinancesInfo = companyFinancesInfo;
			return this;
		}
		
		public SeaTransportCompanyBuilder companyOwnership(SeaTransportCompanyOwnership seaTransportCompanyOwnership) {
			this.seaTransportCompanyOwnership = seaTransportCompanyOwnership;
			return this;
		}
		
		public SeaTransportCompany build() {
			return new SeaTransportCompany(companyInfo, companyFinancesInfo, seaTransportCompanyOwnership);
		}
	}
	
}
