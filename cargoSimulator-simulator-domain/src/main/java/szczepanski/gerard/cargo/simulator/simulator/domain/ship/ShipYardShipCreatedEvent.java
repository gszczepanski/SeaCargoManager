package szczepanski.gerard.cargo.simulator.simulator.domain.ship;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class ShipYardShipCreatedEvent {
	
	private final Ship createdShip;
	
}
