package szczepanski.gerard.cargo.simulator.simulator.domain.payment;

public enum MoneyTransactionType {
	
	INCREASE,
	DECREASE;
	
}
