package szczepanski.gerard.cargo.simulator.simulator.domain.route;

import java.time.LocalDateTime;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import szczepanski.gerard.cargo.simulator.simulator.domain.location.LocationPoint;

@Builder
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class RouteInformationObject {
	
	private final String uuid;
	private final LocationPoint currentLocation;
	private final String startPortUuid;
	private final String startPortName;
	
	private final String endPortUuid;
	private final String endPortName;
	
	private final double travelledDistance;
	private final double totalDistance;
	private final String routeState;
	
	private LocalDateTime startDate;
	
}
