package szczepanski.gerard.cargo.simulator.simulator.domain.common;

import org.w3c.dom.Element;

import szczepanski.gerard.cargo.simulator.common.util.XmlUtils;

public abstract class AbstractModelFactory<M extends AbstractModel> {
	
	protected static final String UUID = "uuid";
	
	public abstract M createFromXmlElement(Element element);
	
	protected final String xmlValue(String tagName, Element element) {
		return XmlUtils.getTagValue(tagName, element);
	}
}
