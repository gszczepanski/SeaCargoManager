package szczepanski.gerard.cargo.simulator.simulator.domain.ship;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import szczepanski.gerard.cargo.simulator.common.util.RandomUtils;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModel;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.Money;

@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class ShipModel extends AbstractModel {
	private static final long serialVersionUID = 8732059472120209686L;
	
	private final String name;
	private final int maximumSpeed;
	private Money price;
	private final int minimumBuildDays;
	private final int maximumBuildDays;
	private final int durability;
	private final int teu;
	private final int reputationPointsForBuy;
	
	public int countBuildDays() {
		return RandomUtils.randomFromRange(minimumBuildDays, maximumBuildDays);
	}
	
}
