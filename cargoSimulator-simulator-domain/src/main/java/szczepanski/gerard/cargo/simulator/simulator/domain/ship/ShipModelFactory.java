package szczepanski.gerard.cargo.simulator.simulator.domain.ship;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.util.XmlUtils;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModelFactory;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.Currency;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.CurrencyRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.Money;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ShipModelFactory extends AbstractModelFactory<ShipModel> {

	private static final String NAME = "name";
	private static final String MAXIMUM_SPEED = "maximumSpeed";
	private static final String PRICE = "price";
	private static final String PRICE_CURRENCY_ID = "currencyId";
	private static final String PRICE_VALUE = "value";
	private static final String MINIMUM_BUILD_DAYS = "minimumBuildDays";
	private static final String MAXIMUM_BUILD_DAYS = "maximumBuildDays";
	private static final String DURABILITY = "durability";
	private static final String TEU = "teu";
	private static final String REPUTATION_POINTS_FOR_BUY = "reputationPointsForBuy";
	
	private final CurrencyRepository currencyRepository;

	@Override
	public ShipModel createFromXmlElement(Element element) {
		Element priceElement  = (Element) element.getElementsByTagName(PRICE).item(0);
		Currency currencyForPrice = currencyRepository.findOne(xmlValue(PRICE_CURRENCY_ID, priceElement));
		Money shipModelPrice = Money.of(xmlValue(PRICE_VALUE, priceElement), currencyForPrice);
		
		ShipModel shipModel = ShipModel.builder()
										.name(XmlUtils.getTagValue(NAME, element))	
										.maximumSpeed(Integer.valueOf(xmlValue(MAXIMUM_SPEED, element)))
										.price(shipModelPrice)
										.minimumBuildDays(Integer.valueOf(xmlValue(MINIMUM_BUILD_DAYS, element)))
										.maximumBuildDays(Integer.valueOf(xmlValue(MAXIMUM_BUILD_DAYS, element)))
										.durability(Integer.valueOf(xmlValue(DURABILITY, element)))
										.teu(Integer.valueOf(xmlValue(TEU, element)))
										.reputationPointsForBuy(Integer.valueOf(xmlValue(REPUTATION_POINTS_FOR_BUY, element)))
										.build();
		
		shipModel.setUuid(xmlValue(UUID, element));
		return shipModel;
	}

}
