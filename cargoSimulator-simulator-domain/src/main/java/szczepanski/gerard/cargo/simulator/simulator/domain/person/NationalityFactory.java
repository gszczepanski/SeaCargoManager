package szczepanski.gerard.cargo.simulator.simulator.domain.person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.util.XmlUtils;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModelFactory;
import szczepanski.gerard.cargo.simulator.simulator.domain.location.Country;
import szczepanski.gerard.cargo.simulator.simulator.domain.location.CountryRepository;

/**
 * 
 * @author Gerard Szczepanski
 */
@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class NationalityFactory extends AbstractModelFactory<Nationality> {

	private static final String UUID = "uuid";
	private static final String NAME = "name";
	private static final String COUNTRY_ID = "countryId";
	
	private final CountryRepository countryRepository;

	@Override
	public Nationality createFromXmlElement(Element element) {
		Country country = countryRepository.findOne(XmlUtils.getTagValue(COUNTRY_ID, element));
		
		Nationality nationality = Nationality.builder()
				.name(xmlValue(NAME, element))
				.country(country)
				.build();
		
		nationality.setUuid(xmlValue(UUID, element));
		return nationality;
	}
}
