package szczepanski.gerard.cargo.simulator.simulator.domain.contract;

import java.util.List;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModel;
import szczepanski.gerard.cargo.simulator.simulator.domain.company.TradingCompany;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.Money;
import szczepanski.gerard.cargo.simulator.simulator.domain.route.Supply;

@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class ContractOffer extends AbstractModel {
	
	private static final long serialVersionUID = 2895070422902363391L;
	
	private final TradingCompany customer;
	private final ContractPayment contractPayment;
	private final List<Supply> supplies;
	private boolean isValid;
	
	private Money price;
	
}
