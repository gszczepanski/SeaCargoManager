package szczepanski.gerard.cargo.simulator.simulator.domain.payment;

import java.math.BigDecimal;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.exception.CargoSimulatorRuntimeException;
import szczepanski.gerard.cargo.simulator.common.util.ParamAssertion;

@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class Money {
	
	private final BigDecimal value;
	private final Currency currency;
	
	public static Money of(String value, Currency currency) {
		ParamAssertion.isNotNull(value, currency);
		ParamAssertion.isNumberRepresentation(value);
		
		return new Money(new BigDecimal(value), currency);
	}
	
	public Money add(Money money) {
		checkIfCurrencyIsTheSame(money);
		
		BigDecimal totalValue = getValue().add(money.getValue());
		return new Money(totalValue, getCurrency());
	}
	
	public Money substract(Money money) {
		checkIfCurrencyIsTheSame(money);
		
		BigDecimal substractedValue = getValue().subtract(money.getValue());
		return new Money(substractedValue, getCurrency());
	}
	
	private void checkIfCurrencyIsTheSame(Money money) {
		if (!getCurrency().equals(money.getCurrency())) {
			throw new MoneyCurrencyException();
		}
	}
	
	public Money multiply(String value) {
		ParamAssertion.isNumberRepresentation(value);
		
		BigDecimal multipliedValue = getValue().multiply(new BigDecimal(value));
		return new Money(multipliedValue, getCurrency());
	}
	
	static class MoneyCurrencyException extends CargoSimulatorRuntimeException {
		private static final long serialVersionUID = 3727062156828214426L;

		public MoneyCurrencyException() {
			super("Given money currencies are not the same");
		}
	}
	
}
