package szczepanski.gerard.cargo.simulator.simulator.domain.person;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModel;
import szczepanski.gerard.cargo.simulator.simulator.domain.location.Country;

@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class Nationality extends AbstractModel {
	private static final long serialVersionUID = -680237149979978504L;
	
	private final String name;
	private final Country country;
	
}
