package szczepanski.gerard.cargo.simulator.simulator.domain.message;

import lombok.Getter;

@Getter
public class MessageEvent {
	
	private final Message message;
	
	public MessageEvent(String messageCode, Object... args) {
		this.message = new Message(messageCode, args);
	}
	
	public MessageEvent(String messageCode) {
		this.message = new Message(messageCode, new Object[]{});
	}
	
	
}
