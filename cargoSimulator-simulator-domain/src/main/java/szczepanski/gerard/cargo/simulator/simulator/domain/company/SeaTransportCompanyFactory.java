package szczepanski.gerard.cargo.simulator.simulator.domain.company;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.util.XmlUtils;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModelFactory;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.Currency;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.CurrencyRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.Money;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SeaTransportCompanyFactory extends AbstractModelFactory<SeaTransportCompany> {

	private static final String BASIC_COMPANY_INFO = "companyInfo";
	private static final String COMPANY_BALANCE = "companyBalance";
	private static final String COMPANY_BALANCE_CURRENCY_ID = "currencyId";
	private static final String COMPANY_BALANCE_VALUE = "value";
	
	private final BasicCompanyInfoFactory basicCompanyInfoFactory;
	private final CurrencyRepository currencyRepository;
	
	@Override
	public SeaTransportCompany createFromXmlElement(Element element) {
		Element companyInfoElement  = (Element) element.getElementsByTagName(BASIC_COMPANY_INFO).item(0);
		BasicCompanyInfo basicCompanyInfo = basicCompanyInfoFactory.createFromXmlElement(companyInfoElement);
		
		Element companyBalanceElement  = (Element) element.getElementsByTagName(COMPANY_BALANCE).item(0);
		Currency startingCompanyBalanceCurrency = currencyRepository.findOne(XmlUtils.getTagValue(COMPANY_BALANCE_CURRENCY_ID, companyBalanceElement)); 
		Money startingCompanyBalance = Money.of(XmlUtils.getTagValue(COMPANY_BALANCE_VALUE, companyBalanceElement), startingCompanyBalanceCurrency);
		
		SeaTransportCompany company = SeaTransportCompany.builder()
				.companyInfo(basicCompanyInfo)
				.companyFinancesInfo(new CompanyFinancesInfo(startingCompanyBalance))
				.companyOwnership(new SeaTransportCompanyOwnership())
				.build();
		
		company.setUuid(XmlUtils.getTagValue(UUID, element));
		return company;
	}

}
