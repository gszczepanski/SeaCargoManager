package szczepanski.gerard.cargo.simulator.simulator.domain.port;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import szczepanski.gerard.cargo.simulator.common.eventbus.EventBusProvider;
import szczepanski.gerard.cargo.simulator.common.util.ParamAssertion;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModel;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.time.SimulatorTime;
import szczepanski.gerard.cargo.simulator.simulator.domain.location.City;
import szczepanski.gerard.cargo.simulator.simulator.domain.message.MessageEvent;
import szczepanski.gerard.cargo.simulator.simulator.domain.ship.Ship;

@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class Port extends AbstractModel {
	private static final long serialVersionUID = 8043987561375901350L;

	private static final Logger LOG = Logger.getLogger(Port.class);
	
	private final String name;
	private final City city;
	
	private final List<Ship> dockedShips;
	
	public void dockShip(Ship ship) {
		ParamAssertion.isNotNull(ship);
		dockedShips.add(ship);
		ship.setDockedToPort(this);
		
		LOG.debug(String.format("%s Ship %s has docked to port %s", SimulatorTime.getTime(), ship.getUuid(), name));
		EventBusProvider.get().post(new MessageEvent("port.ship.has.docked.to.port", ship.getUuid(), name));
	}
	
	public void releaseReadyShips() {
		dockedShips.forEach(s -> {
			s.startCourse();
		});
	}
	
	public void launchOutShipFromPort(Ship ship) {
		dockedShips.remove(ship);
		ship.setDockedToPort(null);
	}

	@Override
	public String toString() {
		String dockedShipsString = dockedShips.stream().map(Ship::getUuid).collect(Collectors.joining(", "));
		return "Port [uuid=" + getUuid() + ", name=" + name + ", city=" + city.getUuid() + ", dockedShips=" + dockedShipsString + "]";
	}
	
}
