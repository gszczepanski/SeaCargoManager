package szczepanski.gerard.cargo.simulator.simulator.domain.location;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.collections.CollectionFactory;
import szczepanski.gerard.cargo.simulator.common.util.XmlUtils;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModelFactory;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.Currency;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.CurrencyRepository;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CountryFactory extends AbstractModelFactory<Country> {

	private static final String UUID = "uuid";
	private static final String CODE = "code";
	private static final String NAME = "name";
	private static final String CURRENCY_ID = "currencyId";

	private final CurrencyRepository currencyRepository;

	@Override
	public Country createFromXmlElement(Element element) {
		Currency currency = currencyRepository.findOne(XmlUtils.getTagValue(CURRENCY_ID, element));

		Country country = Country.builder()
					.code(xmlValue(CODE, element))
					.name(xmlValue(NAME, element))
					.cities(CollectionFactory.map())
					.currency(currency)
					.build();

		country.setUuid(xmlValue(UUID, element));
		return country;
	}

}
