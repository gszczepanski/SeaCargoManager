package szczepanski.gerard.cargo.simulator.simulator.domain.payment;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.util.ParamAssertion;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.time.SimulatorTime;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MoneyBookkeeper {

	public static void createMoneyTransaction(Money money, MoneyOwner fromAccount, MoneyOwner toAccount, String title) {
		ParamAssertion.isNotNull(money, fromAccount, toAccount);
		checkIfFromAccountHasAcceptableBalance(money, fromAccount);

		processFromAccount(money, fromAccount, title);
		processToAccount(money, toAccount, title);
	}

	private static void checkIfFromAccountHasAcceptableBalance(Money money, MoneyOwner fromAccount) {
		if (!fromAccount.canPay(money)) {
			throw new CanNotCreateMoneyTransactionException();
		}
	}

	private static void processFromAccount(Money money, MoneyOwner fromAccount, String title) {
		Money moneyBeforeTransaction = fromAccount.getCurrentBalance();
		fromAccount.subtractMoney(money);
		
		MoneyTransactionHistoryRecord fromAccountHistoryRecord = MoneyTransactionHistoryRecord.builder()
					.amount(money)
					.balanceBeforePayment(moneyBeforeTransaction)
					.balanceAfterPayment(fromAccount.getCurrentBalance())
					.date(SimulatorTime.getTime())
					.title(title)
					.paymentType(MoneyTransactionType.DECREASE)
					.build();
		
		fromAccount.addTransactionHistory(fromAccountHistoryRecord);
	}

	private static void processToAccount(Money money, MoneyOwner toAccount, String title) {
		Money moneyBeforeTransaction = toAccount.getCurrentBalance();
		toAccount.addMoney(money);
		
		MoneyTransactionHistoryRecord fromAccountHistoryRecord = MoneyTransactionHistoryRecord.builder()
					.amount(money)
					.balanceBeforePayment(moneyBeforeTransaction)
					.balanceAfterPayment(toAccount.getCurrentBalance())
					.date(SimulatorTime.getTime())
					.title(title)
					.paymentType(MoneyTransactionType.INCREASE)
					.build();
		
		toAccount.addTransactionHistory(fromAccountHistoryRecord);
	}

}
