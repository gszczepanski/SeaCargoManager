package szczepanski.gerard.cargo.simulator.simulator.domain.contract;

public enum ContractPayment {
	
	FOR_EACH_SUPPLY,
	FOR_TOTAL;
	
}
