package szczepanski.gerard.cargo.simulator.simulator.domain.route;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.IntStream;

import lombok.Getter;
import szczepanski.gerard.cargo.simulator.common.config.UuidPrefix;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModel;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.time.SimulatorTime;
import szczepanski.gerard.cargo.simulator.simulator.domain.location.LocationPoint;
import szczepanski.gerard.cargo.simulator.simulator.domain.port.Port;

public class Route extends AbstractModel {
	private static final long serialVersionUID = -6492067083178212025L;

	private static final AtomicLong ROUTE_UUID_PROVIDER = new AtomicLong();

	private final List<Port> routePorts;
	private final List<LocationPoint> routePoints;

	@Getter
	private final double totalDistance;
	private double distanceTravelled = 0.00;
	private double distanceToNextRoutePoint = 0.00;
	private int actualRoutePointIndex = 0;

	@Getter
	private RouteState routeState = RouteState.PLANNED;
	@Getter
	private final LocalDateTime routeStartDate;
	@Getter
	private LocalDateTime routeEndDate;

	Route(List<Port> routePorts, List<LocationPoint> routePoints, LocalDateTime routeStartDate) {
		setUuid(UuidPrefix.ROUTE_UUID_PREFIX + ROUTE_UUID_PROVIDER.incrementAndGet());
		this.routePorts = routePorts;
		this.routePoints = routePoints;
		this.routeStartDate = routeStartDate;
		this.totalDistance = calculateTotalDistance();
	}

	private double calculateTotalDistance() {
		return IntStream.range(0, routePoints.size() - 1)
				.mapToDouble(i -> routePoints.get(i).calculateDistanceToLocationPoint(routePoints.get(i + 1))).sum();
	}

	public void startRoute() {
		if (!shoudRouteBeStarted()) {
			return;
		}
		routeState = RouteState.ON_ROAD;
		distanceToNextRoutePoint = calculateNextPointDistance();
	}

	private double calculateNextPointDistance() {
		LocationPoint currentPoint = routePoints.get(actualRoutePointIndex);
		LocationPoint nextLocationPoint = routePoints.get(actualRoutePointIndex + 1);
		return distanceToNextRoutePoint + currentPoint.calculateDistanceToLocationPoint(nextLocationPoint);
	}

	public void addTravelledDistanceToRoute(double distance) {
		distanceTravelled += distance;
		if (distanceTravelled >= distanceToNextRoutePoint) {
			goToNextRoutePoint();
		}
	}

	private void goToNextRoutePoint() {
		actualRoutePointIndex++;
		if (shouldRouteBeFinished()) {
			routeState = RouteState.FINISHED;
			routeEndDate = SimulatorTime.getTime();
			return;
		}
		distanceToNextRoutePoint = calculateNextPointDistance();
	}

	private boolean shouldRouteBeFinished() {
		return routePoints.size() - 1 == actualRoutePointIndex;
	}

	public boolean isFinished() {
		return routeState == RouteState.FINISHED;
	}

	public Port startPort() {
		return routePorts.get(0);
	}

	public Port destinationPort() {
		return routePorts.get(routePorts.size() - 1);
	}

	public boolean shoudRouteBeStarted() {
		LocalDateTime gameTime = SimulatorTime.getTime();
		return routeState == RouteState.PLANNED && (gameTime.isEqual(routeStartDate) || gameTime.isAfter(routeStartDate));
	}

	public LocationPoint currentPosition() {
		return routePoints.get(actualRoutePointIndex);
	}

	public final RouteInformationObject informationObject() {
		return RouteInformationObject.builder().uuid(getUuid()).currentLocation(routePoints.get(actualRoutePointIndex)).endPortUuid(destinationPort().getUuid())
				.endPortName(destinationPort().getName()).startPortUuid(startPort().getUuid()).startPortName(startPort().getName())
				.travelledDistance(distanceTravelled).totalDistance(totalDistance).startDate(routeStartDate).routeState(routeState.getReadableString()).build();
	}

	@Override
	public String toString() {
		String startPortUuid = startPort().getUuid();
		String destinationPortUuid = destinationPort().getUuid();

		return "Route [uuid=" + getUuid() + ", startPortUuid=" + startPortUuid + ", destinationPortUuid=" + destinationPortUuid + ", totalDistance="
				+ totalDistance + ", distanceTravelled=" + distanceTravelled + ", distanceToNextRoutePoint=" + distanceToNextRoutePoint + ", routeState="
				+ routeState + ", routeStartDate=" + routeStartDate + ", routeEndDate=" + routeEndDate + "]";
	}

}
