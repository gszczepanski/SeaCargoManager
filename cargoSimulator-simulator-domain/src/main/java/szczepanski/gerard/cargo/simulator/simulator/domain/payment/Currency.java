package szczepanski.gerard.cargo.simulator.simulator.domain.payment;

import java.math.BigDecimal;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModel;

@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public final class Currency extends AbstractModel {
	private static final long serialVersionUID = -8599220097435984348L;
	
	private final String code;
	private BigDecimal valueOfDollar;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Currency other = (Currency) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		return true;
	}
	
}
