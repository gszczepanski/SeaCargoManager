package szczepanski.gerard.cargo.simulator.simulator.domain.company;

public enum CompanyReputation {
	
	VIP,
	A,
	B,
	C,
	D,
	E;
	
}
