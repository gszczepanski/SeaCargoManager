package szczepanski.gerard.cargo.simulator.simulator.domain.company;

public class CourierCompany extends Company {
	private static final long serialVersionUID = -2967166230729729764L;

	CourierCompany(BasicCompanyInfo companyInfo, CompanyFinancesInfo companyFinancesInfo) {
		super(companyInfo, companyFinancesInfo);
	}

}
