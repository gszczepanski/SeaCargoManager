package szczepanski.gerard.cargo.simulator.simulator.domain.route;

import lombok.Getter;

public enum RouteState {
	
	PLANNED("Planned"),
	ON_ROAD("On road"),
	FINISHED("Finished");
	
	@Getter
	private final String readableString;

	private RouteState(String readableString) {
		this.readableString = readableString;
	}
	
	
}
