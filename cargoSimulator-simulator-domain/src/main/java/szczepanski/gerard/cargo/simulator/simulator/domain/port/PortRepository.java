package szczepanski.gerard.cargo.simulator.simulator.domain.port;

import java.util.Optional;

import org.springframework.stereotype.Component;

import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModelRepository;

@Component
public class PortRepository extends AbstractModelRepository<Port> {

	public Optional<Port> getPortForCity(String cityUuid) {
		return getAll().stream().filter(p -> p.getCity().getUuid().equals(cityUuid)).findFirst();
	}

}
