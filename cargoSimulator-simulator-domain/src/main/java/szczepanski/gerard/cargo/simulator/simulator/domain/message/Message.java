package szczepanski.gerard.cargo.simulator.simulator.domain.message;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicLong;

import lombok.Getter;
import szczepanski.gerard.cargo.simulator.common.config.UuidPrefix;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModel;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.time.SimulatorTime;

@Getter
public class Message extends AbstractModel {
	private static final long serialVersionUID = 4309791874789616591L;

	private static final AtomicLong UUID_GENERATOR = new AtomicLong();
	
	private final LocalDateTime time;
	private final Object[] args;
	private final String messageCode;
	private boolean readed;
	
	Message(String messageCode, Object... args) {
		setUuid(UuidPrefix.MESSAGE_UUID_PREFIX + UUID_GENERATOR.incrementAndGet());
		this.time = SimulatorTime.getTime();
		this.messageCode = messageCode;
		this.args = args;
	}

	@Override
	public String toString() {
		return "Message [time=" + time + ", args=" + Arrays.toString(args) + ", messageCode=" + messageCode + ", readed=" + readed + "]";
	}
}
