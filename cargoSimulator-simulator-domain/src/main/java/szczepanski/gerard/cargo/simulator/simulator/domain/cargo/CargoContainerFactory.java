package szczepanski.gerard.cargo.simulator.simulator.domain.cargo;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Component;

import szczepanski.gerard.cargo.simulator.simulator.domain.common.Dimensions;

@Component
public class CargoContainerFactory {
	
	private static final AtomicLong CONTAINER_NO_GENERATOR = new AtomicLong();
	private static final String CONTAINER_NO_PREFIX = "CON";
	
	public CargoContainer create(double lenght, double width, double height) {
		Dimensions dim = new Dimensions(lenght, width, lenght);
		String containerNo = CONTAINER_NO_PREFIX + CONTAINER_NO_GENERATOR.get();
		
		return new CargoContainer(containerNo, dim);
	}
	
}
