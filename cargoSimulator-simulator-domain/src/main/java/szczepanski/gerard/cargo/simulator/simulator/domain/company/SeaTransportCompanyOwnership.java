package szczepanski.gerard.cargo.simulator.simulator.domain.company;

import java.util.List;

import lombok.Getter;
import szczepanski.gerard.cargo.simulator.common.collections.CollectionFactory;
import szczepanski.gerard.cargo.simulator.simulator.domain.ship.Ship;

@Getter
public class SeaTransportCompanyOwnership {
	
	private final List<Ship> ships = CollectionFactory.list();
	
}
