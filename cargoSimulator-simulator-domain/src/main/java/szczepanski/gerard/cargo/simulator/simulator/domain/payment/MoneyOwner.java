package szczepanski.gerard.cargo.simulator.simulator.domain.payment;

public interface MoneyOwner {
	
	boolean canPay(Money amountOfMoney);
	
	void addTransactionHistory(MoneyTransactionHistoryRecord record);
	
	void addMoney(Money money);
	
	void subtractMoney(Money money);
	
	Money getCurrentBalance();
	
}
