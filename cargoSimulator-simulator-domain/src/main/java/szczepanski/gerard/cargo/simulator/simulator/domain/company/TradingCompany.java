package szczepanski.gerard.cargo.simulator.simulator.domain.company;

import java.time.LocalDateTime;
import java.util.List;

import szczepanski.gerard.cargo.simulator.simulator.domain.cargo.Product;
import szczepanski.gerard.cargo.simulator.simulator.domain.contract.ContractOffer;
import szczepanski.gerard.cargo.simulator.simulator.domain.location.City;

public class TradingCompany extends Company {
	private static final long serialVersionUID = -1767509437871924163L;
	
	private List<SeaTransportCompany> trustedCompanies;
	private List<Product> tradingProducts;
	private List<City> citiesForSupply;
	
	TradingCompany(BasicCompanyInfo companyInfo, CompanyFinancesInfo companyFinancesInfo) {
		super(companyInfo, companyFinancesInfo);
	}

	public ContractOffer createContractOffer(String productId, LocalDateTime dateFrom, LocalDateTime dateTo) {
		
		
		
		return null;
	}
	
}
