package szczepanski.gerard.cargo.simulator.simulator.domain.company;

import java.util.List;

import lombok.Getter;
import szczepanski.gerard.cargo.simulator.common.collections.CollectionFactory;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.Money;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.MoneyOwner;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.MoneyTransactionHistoryRecord;

public class CompanyFinancesInfo implements MoneyOwner {
	
	private Money balance;
	
	@Getter
	private final List<MoneyTransactionHistoryRecord> paymentHistory = CollectionFactory.list();
	
	public CompanyFinancesInfo(Money startBalance) {
		this.balance = startBalance;
	}
	
	@Override
	public boolean canPay(Money amountOfMoney) {
		if (balance.getCurrency().equals(amountOfMoney.getCurrency())) {
			return balance.getValue().doubleValue()  >= amountOfMoney.getValue().doubleValue();
		}
		return false;
	}

	@Override
	public void addTransactionHistory(MoneyTransactionHistoryRecord record) {
		paymentHistory.add(record);
	}

	@Override
	public void addMoney(Money money) {
		this.balance = balance.add(money);
	}

	@Override
	public void subtractMoney(Money money) {
		this.balance = balance.substract(money);
	}

	@Override
	public Money getCurrentBalance() {
		return balance;
	}

}
