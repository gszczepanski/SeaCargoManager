package szczepanski.gerard.cargo.simulator.simulator.domain.ship;

import org.springframework.stereotype.Component;

import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModelRepository;

@Component
public class ShipModelRepository extends AbstractModelRepository<ShipModel> {

	
}
