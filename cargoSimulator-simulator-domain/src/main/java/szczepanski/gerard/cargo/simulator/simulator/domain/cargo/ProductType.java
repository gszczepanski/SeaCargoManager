package szczepanski.gerard.cargo.simulator.simulator.domain.cargo;

public enum ProductType {
	
	CLOTHES,
	ELECTRONICS,
	FOOD;
	
}
