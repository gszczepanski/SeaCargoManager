package szczepanski.gerard.cargo.simulator.simulator.domain.person;

import org.springframework.stereotype.Component;

import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModelRepository;

@Component
public class NationalityRepository extends AbstractModelRepository<Nationality> { 
	
}
