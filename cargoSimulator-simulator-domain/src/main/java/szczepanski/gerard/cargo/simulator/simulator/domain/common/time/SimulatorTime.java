package szczepanski.gerard.cargo.simulator.simulator.domain.common.time;

import java.time.LocalDateTime;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SimulatorTime {
	
	private static SimulatorTime instance;
	
	private LocalDateTime simulatorTime;
	
	static SimulatorTime instance() {
		initializeIfNeed();
		return instance;
	}
	
	public static LocalDateTime getTime() {
		initializeIfNeed();
		return instance.simulatorTime;
	}
	
	void updateTime(LocalDateTime time) {
		simulatorTime = time;
	}
	
	private static void initializeIfNeed() {
		if (instance == null) {
			instance = new SimulatorTime();
		}
	}

	
}
