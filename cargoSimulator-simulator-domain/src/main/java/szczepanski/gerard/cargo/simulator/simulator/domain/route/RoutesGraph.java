package szczepanski.gerard.cargo.simulator.simulator.domain.route;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import es.usc.citius.hipster.algorithm.Algorithm;
import es.usc.citius.hipster.algorithm.Hipster;
import es.usc.citius.hipster.graph.GraphBuilder;
import es.usc.citius.hipster.graph.GraphSearchProblem;
import es.usc.citius.hipster.graph.HipsterGraph;
import es.usc.citius.hipster.model.impl.WeightedNode;
import es.usc.citius.hipster.model.problem.SearchProblem;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.collections.CollectionFactory;
import szczepanski.gerard.cargo.simulator.common.exception.CargoSimulatorRuntimeException;
import szczepanski.gerard.cargo.simulator.simulator.domain.location.LocationPoint;

/**
 * Facade for Routes Graph.
 * 
 * @author Gerard Szczepanski
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class RoutesGraph {

	private final HipsterGraph<LocationPoint, Double> graph;

	public static RoutesGraph initialize(Map<String, LocationPoint> routePoints, Map<String, List<String>> routePointsConnections) {
		HipsterGraph<LocationPoint, Double> graph = generateRoutesGraph(routePoints, routePointsConnections);
		return new RoutesGraph(graph);
	}

	private static HipsterGraph<LocationPoint, Double> generateRoutesGraph(Map<String, LocationPoint> routePoints,
			Map<String, List<String>> routePointsConnections) {
		GraphBuilder<LocationPoint, Double> graphBuilder = GraphBuilder.<LocationPoint, Double>create();
		Set<Entry<String, LocationPoint>> entries = routePoints.entrySet();

		for (Entry<String, LocationPoint> entry : entries) {
			String routeUuid = entry.getKey();
			LocationPoint routePoint = entry.getValue();

			List<String> uuidsForNearRoutePoints = routePointsConnections.get(routeUuid);
			uuidsForNearRoutePoints.forEach(uuidForNearRoute -> {
				LocationPoint nearRoutePoint = routePoints.get(uuidForNearRoute);
				validateIfRoutePointExist(nearRoutePoint, uuidForNearRoute, routeUuid);
				graphBuilder.connect(routePoint).to(nearRoutePoint).withEdge(routePoint.calculateDistanceToLocationPoint(nearRoutePoint));
			});
		}

		return graphBuilder.createUndirectedGraph();
	}

	private static void validateIfRoutePointExist(LocationPoint nearRoutePoint, String uuidForNearRoute, String parentUuid) {
		if (nearRoutePoint == null) {
			throw new CargoSimulatorRuntimeException(
					String.format("Error creating route point with uuid: %s. Missing route point with uuid: %s", parentUuid, uuidForNearRoute));
		}
	}

	public List<LocationPoint> generateRoutePointsList(List<LocationPoint> locationPoints) {
		LocationPoint startPoint = locationPoints.get(0);
		LocationPoint destinationPoint = locationPoints.get(locationPoints.size() - 1);
		GraphSearchProblem.FromVertex<LocationPoint> path = GraphSearchProblem.startingFrom(startPoint);

//		for (int i = 1; i < locationPoints.size() - 1; i++) {
//			path = path.goalAt(locationPoints.get(i));
//		}
		
		SearchProblem<Double, LocationPoint, WeightedNode<Double, LocationPoint, Double>> p = path.in(graph).takeCostsFromEdges().build();
		Algorithm<Double, LocationPoint, WeightedNode<Double, LocationPoint, Double>>.SearchResult searchResult = Hipster.createAStar(p).search(destinationPoint);
		return searchResult.getOptimalPaths().get(0);
	}
	
	public Set<LocationPoint> getAllLocationPoints() {
		Set<LocationPoint> locationPoints = CollectionFactory.set();
		
		graph.edges().forEach(e -> {
			locationPoints.add(e.getVertex1());
			locationPoints.add(e.getVertex2());
		});
		
		return locationPoints;
	}
	
}
