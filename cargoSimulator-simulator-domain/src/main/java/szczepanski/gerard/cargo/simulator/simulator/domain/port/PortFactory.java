package szczepanski.gerard.cargo.simulator.simulator.domain.port;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.collections.CollectionFactory;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModelFactory;
import szczepanski.gerard.cargo.simulator.simulator.domain.location.City;
import szczepanski.gerard.cargo.simulator.simulator.domain.location.CityRepository;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PortFactory extends AbstractModelFactory<Port> {

	private static final String NAME = "name";
	private static final String CITY_ID = "cityId";

	private final CityRepository cityRepository;

	@Override
	public Port createFromXmlElement(Element element) {
		City city = cityRepository.findOne(xmlValue(CITY_ID, element));

		Port port = Port.builder()
					.name(xmlValue(NAME, element))
					.city(city)
					.dockedShips(CollectionFactory.list())
					.build();

		port.setUuid(xmlValue(UUID, element));
		return port;
	}

}
