package szczepanski.gerard.cargo.simulator.simulator.domain.payment;

import org.testng.Assert;
import org.testng.annotations.Test;

import szczepanski.gerard.cargo.simulator.common.exception.ParamAssertionException;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.Money.MoneyCurrencyException;

public class MoneyTest {

	@Test
	public void createMoneySuccess() {
		// Arrange
		Currency pln = Currency.builder().code("PLN").build();
		String value = "50";
		
		//Act
		Money money = Money.of(value, pln);
		
		//Assert
		Assert.assertEquals(money.getValue().intValue(), 50);
		Assert.assertEquals(money.getCurrency(), pln);
	}
	
	@Test(expectedExceptions = ParamAssertionException.class)
	public void createMoneyFailureIfValueIsNotNumeric() {
		// Arrange
		Currency pln = Currency.builder().code("PLN").build();
		String value = "hello world";
		
		//Act
		Money.of(value, pln);
	}
	
	@Test(expectedExceptions = ParamAssertionException.class)
	public void createMoneyFailureIfCurrencyIsNull() {
		// Arrange
		String value = "99";
		
		//Act
		Money.of(value, null);
	}

	@Test
	public void addMoneyWithTheSameCurrencyTest() {
		// Arrange
		Currency pln = Currency.builder().code("PLN").build();
		int expectedTotalValue = 50;

		Money firstMoney = Money.of("27.00", pln);
		Money secondMoney = Money.of("23.00", pln);

		// Act
		Money totalMoney = firstMoney.add(secondMoney);

		// Assert
		Assert.assertEquals(totalMoney.getValue().intValue(), expectedTotalValue);
		Assert.assertEquals(totalMoney.getCurrency(), pln);

		// Check if added values not changed
		Assert.assertEquals(firstMoney.getValue().intValue(), 27);
		Assert.assertEquals(firstMoney.getCurrency(), pln);

		Assert.assertEquals(secondMoney.getValue().intValue(), 23);
		Assert.assertEquals(secondMoney.getCurrency(), pln);
	}

	@Test(expectedExceptions = MoneyCurrencyException.class)
	public void addMoneyWithAnotherCurrencyThrowsException() {
		// Arrange
		Currency pln = Currency.builder().code("PLN").build();
		Currency eur = Currency.builder().code("EUR").build();

		Money firstMoney = Money.of("27.00", pln);
		Money secondMoney = Money.of("23.00", eur);

		// Act
		firstMoney.add(secondMoney);
	}

	@Test
	public void substractMoneyTest() {
		// Arrange
		Currency pln = Currency.builder().code("PLN").build();
		int expectedTotalValue = 4;

		Money firstMoney = Money.of("27.00", pln);
		Money secondMoney = Money.of("23.00", pln);

		// Act
		Money totalMoney = firstMoney.substract(secondMoney);

		// Assert
		Assert.assertEquals(totalMoney.getValue().intValue(), expectedTotalValue);
		Assert.assertEquals(totalMoney.getCurrency(), pln);

		// Check if added values not changed
		Assert.assertEquals(firstMoney.getValue().intValue(), 27);
		Assert.assertEquals(firstMoney.getCurrency(), pln);

		Assert.assertEquals(secondMoney.getValue().intValue(), 23);
		Assert.assertEquals(secondMoney.getCurrency(), pln);
	}

	@Test(expectedExceptions = MoneyCurrencyException.class)
	public void substractMoneyWithAnotherCurrencyThrowsException() {
		// Arrange
		Currency pln = Currency.builder().code("PLN").build();
		Currency eur = Currency.builder().code("EUR").build();

		Money firstMoney = Money.of("27.00", pln);
		Money secondMoney = Money.of("23.00", eur);

		// Act
		firstMoney.substract(secondMoney);
	}

	@Test
	public void multiplyMoneyTest() {
		// Arrange
		Currency pln = Currency.builder().code("PLN").build();
		int expectedTotalValue = 100;
		Money money = Money.of("20.00", pln);

		// Act
		Money multipliedMoney = money.multiply("5");

		// Assert
		Assert.assertEquals(multipliedMoney.getValue().intValue(), expectedTotalValue);
		Assert.assertEquals(multipliedMoney.getCurrency(), pln);

		// Check if added values not changed
		Assert.assertEquals(money.getValue().intValue(), 20);
		Assert.assertEquals(money.getCurrency(), pln);
	}

	@Test(expectedExceptions = ParamAssertionException.class)
	public void multiplyMoneyThrowsExceptionIfValueIsNotNumeric() {
		// Arrange
		Currency pln = Currency.builder().code("PLN").build();
		Money money = Money.of("20.00", pln);

		// Act
		money.multiply("hello world");
	}

}
