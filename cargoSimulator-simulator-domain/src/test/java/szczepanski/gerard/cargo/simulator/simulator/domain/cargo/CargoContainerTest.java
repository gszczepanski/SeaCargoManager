package szczepanski.gerard.cargo.simulator.simulator.domain.cargo;

import java.math.BigDecimal;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;

import szczepanski.gerard.cargo.simulator.simulator.domain.common.Dimensions;

public class CargoContainerTest {
	
	CargoContainerFactory containerFactory;
	
	@BeforeTest
	public void beforeTest() {
		containerFactory = new CargoContainerFactory();
	}
	
	//@Test
	public void testCountMaxCapacityForProductWhenContainerIsEmpty() {
		// Arrange
		CargoContainerItem car = new StubItem();
		car.dimensions = new Dimensions(4.0, 1.5, 1.2);
		car.weight = new BigDecimal("1590");
		
		CargoContainer cargoContainer = containerFactory.create(25.0, 5.0, 5.0);
		
		int expectedMaxCapacity = 16;
		
		// Act
		int maxCapacity = cargoContainer.countMaxCapacityForItem(car);
		
		// Assert
		Assert.assertEquals(maxCapacity, expectedMaxCapacity);
	}
	
	
	private class StubItem extends CargoContainerItem {
		
	}
}
