package szczepanski.gerard.cargo.simulator.simulator.domain.route

import spock.lang.Specification
import szczepanski.gerard.cargo.simulator.simulator.domain.common.time.SimulatorTime

import java.time.LocalDateTime

import static szczepanski.gerard.cargo.simulator.simulator.domain.route.RouteState.*

class RouteSpec extends Specification implements RouteTrait {

    def "should not start route when route is already started"() {
        given:
        Route route = route()
        route.routeState = FINISHED
        route.distanceToNextRoutePoint = 0

        when:
        route.startRoute()

        then:
        route.distanceToNextRoutePoint == 0
    }

    def "should set on road on start route when route is not started yet"() {
        given:
        Route route = route()
        SimulatorTime.instance().updateTime(LocalDateTime.now().plusDays(1))

        when:
        route.startRoute()

        then:
        route.routeState == ON_ROAD
    }

    def "should calculate next point distance on route start"() {
        given:
        Route route = route()
        SimulatorTime.instance().updateTime(LocalDateTime.now().plusDays(1))

        when:
        route.startRoute()

        then:
        route.distanceToNextRoutePoint != 0.00
    }

    def "should sail until reach the destination point"() {
        given:
        Route route = route()
        SimulatorTime.instance().updateTime(LocalDateTime.now().plusDays(1))
        route.startRoute()
        double shipSpeed = 37
        int latestPointIndex = routePoints().size() - 1

        when:
        while(!route.isFinished()) {
            route.addTravelledDistanceToRoute(shipSpeed)
        }

        then:
        route.actualRoutePointIndex == latestPointIndex
    }

    def "should route be finished after sailed to destination point"() {
        given:
        Route route = route()
        SimulatorTime.instance().updateTime(LocalDateTime.now().plusDays(1))
        route.startRoute()
        double shipSpeed = 37

        when:
        while(!route.isFinished()) {
            route.addTravelledDistanceToRoute(shipSpeed)
        }

        then:
        route.routeState == FINISHED
    }
}
