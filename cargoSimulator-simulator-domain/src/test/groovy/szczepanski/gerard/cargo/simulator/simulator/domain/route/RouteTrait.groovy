package szczepanski.gerard.cargo.simulator.simulator.domain.route

import szczepanski.gerard.cargo.simulator.simulator.domain.location.LocationPoint
import szczepanski.gerard.cargo.simulator.simulator.domain.port.Port

import java.time.LocalDateTime

trait RouteTrait {

    def "route"() {
        Port startPort = Port.builder()
                                .name("Glasgow")
                                .build()

        Port destinationPort = Port.builder()
                                    .name("Oslo")
                                    .build()
        List<Port> ports = [startPort, destinationPort]

        return new Route(ports, routePoints(), LocalDateTime.now())
    }

    def "routePoints"() {
        return  [
                new LocationPoint(56.455017, 1.293452),
                new LocationPoint(56.455118, 1.293291),
                new LocationPoint(56.455017, 1.293123),
                new LocationPoint(56.455017, 1.293099),
                new LocationPoint(56.455017, 1.292978),
                new LocationPoint(56.455017, 1.293132),
                new LocationPoint(56.455017, 1.293923),
                new LocationPoint(56.455017, 1.293412)
        ]
    }


}