package szczepanski.gerard.cargo.simulator.console.client;

public class CargoSimulatorConsoleShellRuntimeException extends RuntimeException {
	private static final long serialVersionUID = 7870533132077067555L;
	
	public CargoSimulatorConsoleShellRuntimeException() {
		super();
	}

	public CargoSimulatorConsoleShellRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public CargoSimulatorConsoleShellRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	public CargoSimulatorConsoleShellRuntimeException(String message) {
		super(message);
	}

	public CargoSimulatorConsoleShellRuntimeException(Throwable cause) {
		super(cause);
	}



}
