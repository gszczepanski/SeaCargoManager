package szczepanski.gerard.cargo.simulator.console.client;

import org.apache.log4j.Logger;

public class ConsoleShell {
	private static final Logger LOG = Logger.getLogger(ConsoleShell.class);
	
	private static final String CONSOLE_VERSION = "0.1";
	private static final Integer CONSOLE_COMMUNICATION_PORT = 6789;
	
	public static void main(String... args) {
		LOG.info(String.format("Sea Cargo Manager Shell version [%s]", CONSOLE_VERSION));
		ConsoleClient client = new ConsoleClient(CONSOLE_COMMUNICATION_PORT);
		
		try {
			client.connectToApp();
		} catch (CargoSimulatorConsoleShellRuntimeException e) {
			LOG.info("ERROR: Connection with SeaCargoManager running appication is closed. Closing console.");
		}
	}
}
