package szczepanski.gerard.cargo.simulator.console.client;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import org.apache.log4j.Logger;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ConsoleClient {
	private static final Logger LOG = Logger.getLogger(ConsoleClient.class);

	private static final String APP_RESPONSE_PREFIX = ">>> SeaCargoManager: ";
	private static final int MAX_RESPONSE_WAIT_ATTEMPTS = 6000;
	private static final int ONE_SECOND_IN_MILLIS = 1000;

	private final Integer port;

	private Socket socket;
	private BufferedReader console = new BufferedReader(new InputStreamReader(System.in));

	private DataInputStream in;
	private DataOutputStream out;

	public void connectToApp() {
		LOG.info("Connecting to host...");
		createConnection();
		communicate();
	}

	private void createConnection() {
		String host = obtainHost();
		tryToEstablishConnection(host);
		LOG.info(String.format("Successfully connected to SeaCargoManager Application on host: %s port: %s", host, port));
	}

	private String obtainHost() {
		try {
			return InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			displayConnectionErrorMessage();
			throw new CargoSimulatorConsoleShellRuntimeException();
		}
	}

	private void tryToEstablishConnection(String host) {
		while (true) {
			try {
				socket = new Socket(host, port);
				in = new DataInputStream(socket.getInputStream());
				out = new DataOutputStream(socket.getOutputStream());
				break;
			} catch (IOException e) {
				displayConnectionErrorMessage();
				waitBeforeReconnect();
			}
		}
	}

	private void displayConnectionErrorMessage() {
		LOG.info("ERROR: Unable to connect to host. Check if SeaCargoSimulator is already running");
		LOG.info("If this not help, check if console listening is set up in SeaCargoManager settings");
		LOG.info("Retrying...");
	}

	private void waitBeforeReconnect() {
		try {
			Thread.sleep(ONE_SECOND_IN_MILLIS);
		} catch (InterruptedException e) {
			LOG.info("Fatal error");
			throw new CargoSimulatorConsoleShellRuntimeException(e);
		}
	}

	private void communicate() {
		LOG.info("Application is now listening to console");

		while (true) {
			try {
				String communicate = console.readLine();
				sendCommunicate(communicate);
				waitForResponse();
				recieveResponse();
			} catch (IOException e) {
				LOG.info("Unable to read from keyboard. FAILURE");
				throw new CargoSimulatorConsoleShellRuntimeException();
			} catch (InterruptedException e) {
				LOG.info("Connection iterrupted. FAILURE");
				throw new CargoSimulatorConsoleShellRuntimeException();
			}
		}
	}

	private void sendCommunicate(String message) {
		try {
			out.writeUTF(message);
		} catch (IOException e) {
			LOG.info("Unable to send response. Respond failure");
			throw new CargoSimulatorConsoleShellRuntimeException();
		}
	}

	private void waitForResponse() throws IOException, InterruptedException {
		int attempts = 0;
		while (in.available() == 0 && attempts < MAX_RESPONSE_WAIT_ATTEMPTS) {
			attempts++;
			Thread.sleep(10);
			
			if (attempts % 100 == 0) {
				System.out.println("Waiting for answer...");
			}
		}
	}

	private void recieveResponse() throws IOException {
		if (in.available() == 0) {
			LOG.info("The answer message waiting time has been exceeded.");
			return;
		}

		String messageFromApp = in.readUTF();
		System.out.println(APP_RESPONSE_PREFIX + messageFromApp);
	}
}
