package szczepanski.gerard.cargo.simulator.program.main;

import org.apache.log4j.Logger;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import szczepanski.gerard.cargo.simulator.common.config.GlobalImages;
import szczepanski.gerard.cargo.simulator.common.config.GlobalVariables;
import szczepanski.gerard.cargo.simulator.common.context.ApplicationContextProvider;
import szczepanski.gerard.cargo.simulator.program.config.ConsoleListenerConfigurer;
import szczepanski.gerard.cargo.simulator.program.config.ProgramSettings;
import szczepanski.gerard.cargo.simulator.program.config.UserInterfaceConfigRegister;
import szczepanski.gerard.cargo.simulator.ui.view.simulator.WelcomeSceneFactory;

public class Main extends Application {
	private static final Logger LOG = Logger.getLogger(Main.class);

	public static void main(String... args) {
		loadComponents();
		configureUI();
		launch(args);
	}

	private static void loadComponents() {
		LOG.info("Loading Spring beans");
		ApplicationContextProvider.loadContext();
	}

	private static void configureUI() {
		LOG.info("Configure UI");
		UserInterfaceConfigRegister.registerInterfaceBeans();
	}
	
	private static void configureConsoleListener() {
		LOG.info("Configuring Console Listener");
		ConsoleListenerConfigurer.configureConsoleListener();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		ProgramSettings settings = ProgramSettings.load();
		
		WelcomeSceneFactory welcomeSceneFactory = ApplicationContextProvider.getBean(WelcomeSceneFactory.class);
		Scene welcomeScene = welcomeSceneFactory.createComponent();

		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent e) {
				Platform.exit();
				System.exit(0);
			}
		});
		
		if (settings.isConsolePortEnabled()) {
			Platform.runLater(() -> {
				configureConsoleListener();
			});
		}

		primaryStage.setTitle(GlobalVariables.DISPLAY_PROGRAM_TITLE);
		primaryStage.setResizable(false);
		primaryStage.setScene(welcomeScene);
		primaryStage.getIcons().add(GlobalImages.PROGRAM_ICON);
		primaryStage.show();
	}

}
