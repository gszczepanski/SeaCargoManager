package szczepanski.gerard.cargo.simulator.program.config;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.config.DevelopmentVariables;
import szczepanski.gerard.cargo.simulator.common.context.ApplicationContextProvider;
import szczepanski.gerard.cargo.simulator.console.program.endpoint.listener.ConsoleListenerRunner;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConsoleListenerConfigurer {
	
	public static void configureConsoleListener() {
		//TODO check if there is a need to run console listener
		ConsoleListenerRunner consoleListenerRunner = ApplicationContextProvider.getBean(ConsoleListenerRunner.class);
		consoleListenerRunner.startConsoleListening(DevelopmentVariables.CONSOLE_COMMUNICATION_PORT);
	}
	
}
