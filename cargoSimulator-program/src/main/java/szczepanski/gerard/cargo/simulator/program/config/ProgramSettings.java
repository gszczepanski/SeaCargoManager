package szczepanski.gerard.cargo.simulator.program.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import szczepanski.gerard.cargo.simulator.common.config.GlobalVariables;
import szczepanski.gerard.cargo.simulator.common.exception.CargoSimulatorRuntimeException;


@Getter
@Setter(AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProgramSettings {
	
	private static final String CONSOLE_PORT_ENABLED_KEY = "consolePortEnabled";
	private boolean consolePortEnabled;
	
	public static ProgramSettings load() {
		Properties properties = loadProperties();
		ProgramSettings settings = new ProgramSettings();
		
		settings.setConsolePortEnabled(Boolean.valueOf(properties.getProperty(CONSOLE_PORT_ENABLED_KEY)));
		//TODO other settings
		
		return settings;
	}
	
	private static Properties loadProperties() {
		Properties properties = new Properties();

		try {
			InputStream in = new FileInputStream(GlobalVariables.PROGRAM_SETTINGS_PROPERTIES_PATH);
			properties.load(in);
			in.close();
			return properties;
		} catch (IOException | NullPointerException e) {
			throw new CargoSimulatorRuntimeException("settings.properties cannot be localized in game folder");
		}
	}
	
}
