package szczepanski.gerard.cargo.simulator.program.config;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.context.ApplicationContextProvider;
import szczepanski.gerard.cargo.simulator.ui.view.common.FactoryKeys;
import szczepanski.gerard.cargo.simulator.ui.view.common.PaneFactoryProvider;
import szczepanski.gerard.cargo.simulator.ui.view.common.SceneFactoryProvider;
import szczepanski.gerard.cargo.simulator.ui.view.game.GameSceneFactory;
import szczepanski.gerard.cargo.simulator.ui.view.game.MessageFooterPaneFactory;
import szczepanski.gerard.cargo.simulator.ui.view.game.TopToolbarPaneFactory;
import szczepanski.gerard.cargo.simulator.ui.view.game.world.map.ShipInfoPaneFactory;
import szczepanski.gerard.cargo.simulator.ui.view.game.world.map.WorldMapPaneFactory;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UserInterfaceConfigRegister {

	public static void registerInterfaceBeans() {
		registerScenes();
		registerPanes();
	}

	private static void registerScenes() {
		SceneFactoryProvider provider = ApplicationContextProvider.getBean(SceneFactoryProvider.class);
		
		provider.registerFactory(FactoryKeys.GAME_SCENE, ApplicationContextProvider.getBean(GameSceneFactory.class));
	}

	private static void registerPanes() {
		PaneFactoryProvider provider = ApplicationContextProvider.getBean(PaneFactoryProvider.class);
		
		provider.registerFactory(FactoryKeys.GAME_TOP_TOOLBAR_PANE, ApplicationContextProvider.getBean(TopToolbarPaneFactory.class));
		provider.registerFactory(FactoryKeys.GAME_WORLD_MAP_PANE, ApplicationContextProvider.getBean(WorldMapPaneFactory.class));
		provider.registerFactory(FactoryKeys.GAME_FOOTER_PANE, ApplicationContextProvider.getBean(MessageFooterPaneFactory.class));
		provider.registerFactory(FactoryKeys.SHIP_INFO_PANE, ApplicationContextProvider.getBean(ShipInfoPaneFactory.class));
	}

}
