package szczepanski.gerard.cargo.simulator.initializer.loader;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import szczepanski.gerard.cargo.simulator.common.util.XmlUtils;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModel;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModelFactory;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.AbstractModelRepository;

public abstract class ModelLoader {

	public abstract void loadModel();
	
	protected <K, M extends AbstractModel> void loadModelFromXml(String xmlPath, String tagName, AbstractModelFactory<M> modelFactory, AbstractModelRepository<M> modelRepository) {
		Document xmlDocument = XmlUtils.loadXmlDocument(xmlPath);
		xmlDocument.getDocumentElement().normalize();
		NodeList nodeList = xmlDocument.getElementsByTagName(tagName);
		
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) node;
				M modelObj = modelFactory.createFromXmlElement(element);
				modelRepository.save(modelObj);
			}
		}
	}

}
