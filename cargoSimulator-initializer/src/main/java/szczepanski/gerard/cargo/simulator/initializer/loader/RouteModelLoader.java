package szczepanski.gerard.cargo.simulator.initializer.loader;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.collections.CollectionFactory;
import szczepanski.gerard.cargo.simulator.common.config.GlobalVariables;
import szczepanski.gerard.cargo.simulator.common.util.XmlUtils;
import szczepanski.gerard.cargo.simulator.simulator.domain.location.LocationPoint;
import szczepanski.gerard.cargo.simulator.simulator.domain.route.RouteFactory;
import szczepanski.gerard.cargo.simulator.simulator.domain.route.RoutesGraph;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class RouteModelLoader extends ModelLoader {
	private static final Logger LOG = Logger.getLogger(RouteModelLoader.class);
	
	private static final String UUID = "uuid";
	private static final String LATITUDE = "latitude";
	private static final String LONGITUDE = "longitude";
	private static final String NEAR_ROUTE_POINT = "nearRoutePoint";
	
	private static final String ROUTE_POINTS_XML = GlobalVariables.MODEL_FOLDER_PATH + "routePoints.xml";
	private static final String ROUTE_POINT_TAG_NAME = "routePoint";
	
	private final RouteFactory routeFactory;
	
	private Map<String, List<String>> connectionsMap;
	private Map<String, LocationPoint> routePointsMap; 
	
	@Override
	public void loadModel() {
		LOG.info("Loading routePoints");
		NodeList nodeList = XmlUtils.loadNodeListFromSpecifiedTag(ROUTE_POINTS_XML, ROUTE_POINT_TAG_NAME);
		generateMaps(nodeList);
		RoutesGraph routesGraph = RoutesGraph.initialize(routePointsMap, connectionsMap);
		routeFactory.setRoutesGraph(routesGraph);
	}
	
	private void generateMaps(NodeList nodeList) {
		createMaps();
		loadDataFromNodesToMaps(nodeList);
	}
	
	private void createMaps() {
		connectionsMap = CollectionFactory.map();
		routePointsMap = CollectionFactory.map();
	}
	
	private void loadDataFromNodesToMaps(NodeList nodeList) {
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) node;
				readPoint(element);
			}
		}
	}
	
	private void readPoint(Element element) {
		String uuid = XmlUtils.getTagValue(UUID, element);
		Double latitude = Double.valueOf(XmlUtils.getTagValue(LATITUDE, element));
		Double longitude = Double.valueOf(XmlUtils.getTagValue(LONGITUDE, element));
		LocationPoint routeLocationPoint = new LocationPoint(latitude, longitude);
		routePointsMap.put(uuid, routeLocationPoint);
		readPointConnections(uuid, element);
	}
	
	private void readPointConnections(String uuid, Element element) {
		NodeList nodeList = element.getElementsByTagName(NEAR_ROUTE_POINT);
		List<String> nearRoutePoints = CollectionFactory.list(nodeList.getLength());
		
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			nearRoutePoints.add(node.getTextContent());
		}
		
		connectionsMap.put(uuid, nearRoutePoints);
	}
	
}
