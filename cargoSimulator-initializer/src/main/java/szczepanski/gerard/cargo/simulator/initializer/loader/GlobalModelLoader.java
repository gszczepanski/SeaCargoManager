package szczepanski.gerard.cargo.simulator.initializer.loader;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class GlobalModelLoader {
	private static final Logger LOG = Logger.getLogger(GlobalModelLoader.class);
	
	private final PaymentModelLoader paymentModelLoader;
	private final LocationModelLoader locationModelLoader;
	private final RouteModelLoader routeModelLoader;
	private final ShipModelLoader shipModelLoader;
	private final PersonModelLoader personModelLoader;
	private final CompanyModelLoader companyModelLoader;
	
	public void loadModel() {
		LOG.info("Loading simulator model...");
		paymentModelLoader.loadModel();
		locationModelLoader.loadModel();
		routeModelLoader.loadModel();
		shipModelLoader.loadModel();
		personModelLoader.loadModel();
		companyModelLoader.loadModel();
	}
	
}
