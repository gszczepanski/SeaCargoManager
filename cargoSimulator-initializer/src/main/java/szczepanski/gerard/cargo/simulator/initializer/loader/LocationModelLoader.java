package szczepanski.gerard.cargo.simulator.initializer.loader;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.config.GlobalVariables;
import szczepanski.gerard.cargo.simulator.simulator.domain.location.CityFactory;
import szczepanski.gerard.cargo.simulator.simulator.domain.location.CityRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.location.CountryFactory;
import szczepanski.gerard.cargo.simulator.simulator.domain.location.CountryRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.port.PortFactory;
import szczepanski.gerard.cargo.simulator.simulator.domain.port.PortRepository;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class LocationModelLoader extends ModelLoader {
	private static final Logger LOG = Logger.getLogger(LocationModelLoader.class);
	
	private static final String COUNTRY_XML = GlobalVariables.MODEL_FOLDER_PATH + "country.xml";
	private static final String COUNTRY_TAG_NAME = "country";
	private static final String CITY_XML = GlobalVariables.MODEL_FOLDER_PATH + "city.xml";
	private static final String CITY_TAG_NAME = "city";
	private static final String PORT_XML = GlobalVariables.MODEL_FOLDER_PATH + "port.xml";
	private static final String PORT_TAG_NAME = "port";
	
	private final CountryFactory countryFactory;
	private final CountryRepository countryRepository;
	private final CityFactory cityFactory;
	private final CityRepository cityRepository;
	private final PortFactory portFactory;
	private final PortRepository portRepository;
	
	@Override
	public void loadModel() {
		LOG.info("Loading Countries");
		loadModelFromXml(COUNTRY_XML, COUNTRY_TAG_NAME, countryFactory, countryRepository);
		LOG.info("Loading Cities");
		loadModelFromXml(CITY_XML, CITY_TAG_NAME, cityFactory, cityRepository);
		LOG.info("Loading Ports");
		loadModelFromXml(PORT_XML, PORT_TAG_NAME, portFactory, portRepository);
	}
	
}
