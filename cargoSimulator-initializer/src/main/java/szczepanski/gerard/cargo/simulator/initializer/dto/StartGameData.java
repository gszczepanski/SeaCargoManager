package szczepanski.gerard.cargo.simulator.initializer.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
@AllArgsConstructor
public class StartGameData {
	
	private final String playerFirstName;
	private final String playerLastName;
	private final LocalDate playerBirthDate;
	private final String playerNationalityUuid;
	private final String playerCompanyUuid;
	
	private final int numberOfAiTradingCompanies;
	private final int numberOfAiSeaTransportCompanies;
	private final int numberOfAiLandTransportCompanies;
}
