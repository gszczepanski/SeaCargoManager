package szczepanski.gerard.cargo.simulator.initializer.loader;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.config.GlobalVariables;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.CurrencyFactory;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.CurrencyRepository;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class PaymentModelLoader extends ModelLoader {
	private static final Logger LOG = Logger.getLogger(PaymentModelLoader.class);
	
	private static final String CURRENCY_XML = GlobalVariables.MODEL_FOLDER_PATH + "currency.xml";
	private static final String CURRENCY_TAG_NAME = "currency";
	
	private final CurrencyFactory currencyFactory;
	private final CurrencyRepository currencyRepository;
	
	@Override
	public void loadModel() {
		LOG.info("Loading currencies");
		loadModelFromXml(CURRENCY_XML, CURRENCY_TAG_NAME, currencyFactory, currencyRepository);
	}
	
}
