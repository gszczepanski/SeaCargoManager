package szczepanski.gerard.cargo.simulator.initializer.loader;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.config.GlobalVariables;
import szczepanski.gerard.cargo.simulator.simulator.domain.person.NationalityFactory;
import szczepanski.gerard.cargo.simulator.simulator.domain.person.NationalityRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.person.PersonFactory;
import szczepanski.gerard.cargo.simulator.simulator.domain.person.PersonRepository;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class PersonModelLoader extends ModelLoader {
	private static final Logger LOG = Logger.getLogger(PersonModelLoader.class);
	
	private static final String NATIONALITY_XML = GlobalVariables.MODEL_FOLDER_PATH + "nationality.xml";
	private static final String NATIONALITY_TAG_NAME = "nationality";
	private static final String PERSON_XML = GlobalVariables.MODEL_FOLDER_PATH + "person.xml";
	private static final String PERSON_TAG_NAME = "person";
	
	private final NationalityFactory nationalityFactory;
	private final NationalityRepository nationalityRepository;
	private final PersonFactory personFactory;
	private final PersonRepository personRepository;

	@Override
	public void loadModel() {
		LOG.info("Loading Nationalities");
		loadModelFromXml(NATIONALITY_XML, NATIONALITY_TAG_NAME, nationalityFactory, nationalityRepository);
		LOG.info("Loading Persons");
		loadModelFromXml(PERSON_XML, PERSON_TAG_NAME, personFactory, personRepository);
	}

}
