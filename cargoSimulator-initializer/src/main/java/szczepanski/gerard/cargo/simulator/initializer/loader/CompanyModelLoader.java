package szczepanski.gerard.cargo.simulator.initializer.loader;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.config.GlobalVariables;
import szczepanski.gerard.cargo.simulator.simulator.domain.company.SeaTransportCompanyFactory;
import szczepanski.gerard.cargo.simulator.simulator.domain.company.SeaTransportCompanyRepository;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class CompanyModelLoader extends ModelLoader {
	private static final Logger LOG = Logger.getLogger(CompanyModelLoader.class);
	
	private static final String SEA_TRANSPORT_COMPANY_XML = GlobalVariables.MODEL_FOLDER_PATH + "seaTransportCompany.xml";
	private static final String SEA_TRANSPORT_COMPANY_TAG_NAME = "seaTransportCompany";
	
	private final SeaTransportCompanyFactory seaTransportCompanyFactory;
	private final SeaTransportCompanyRepository seaTransportCompanyRepository;

	@Override
	public void loadModel() {
		LOG.info("Loading Sea Transport Companies");
		loadModelFromXml(SEA_TRANSPORT_COMPANY_XML, SEA_TRANSPORT_COMPANY_TAG_NAME, seaTransportCompanyFactory, seaTransportCompanyRepository);
	}

}
