package szczepanski.gerard.cargo.simulator.initializer.loader;

import lombok.RequiredArgsConstructor;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import szczepanski.gerard.cargo.simulator.common.config.GlobalVariables;
import szczepanski.gerard.cargo.simulator.simulator.domain.ship.ShipModelFactory;
import szczepanski.gerard.cargo.simulator.simulator.domain.ship.ShipModelRepository;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class ShipModelLoader extends ModelLoader {
    private static final Logger LOG = Logger.getLogger(ShipModelLoader.class);

    private static final String SHIP_MODEL_XML = GlobalVariables.MODEL_FOLDER_PATH + "shipModel.xml";
    private static final String SHIP_MODEL_TAG_NAME = "shipModel";

    private final ShipModelFactory shipModelFactory;
    private final ShipModelRepository shipModelRepository;

    @Override
    public void loadModel() {
        LOG.info("Loading Ship Models");
        loadModelFromXml(SHIP_MODEL_XML, SHIP_MODEL_TAG_NAME, shipModelFactory, shipModelRepository);
    }

}
