package szczepanski.gerard.cargo.simulator.initializer.initializer;

import lombok.RequiredArgsConstructor;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import szczepanski.gerard.cargo.simulator.common.util.ParamAssertion;
import szczepanski.gerard.cargo.simulator.engine.player.HumanPlayerContext;
import szczepanski.gerard.cargo.simulator.engine.player.Player;
import szczepanski.gerard.cargo.simulator.engine.player.PlayerFactory;
import szczepanski.gerard.cargo.simulator.engine.player.PlayerType;
import szczepanski.gerard.cargo.simulator.initializer.dto.StartGameData;
import szczepanski.gerard.cargo.simulator.simulator.domain.person.Nationality;
import szczepanski.gerard.cargo.simulator.simulator.domain.person.NationalityRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.person.Person;
import szczepanski.gerard.cargo.simulator.simulator.domain.person.PersonRepository;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class HumanPlayerInitializer {
    private static final Logger LOG = Logger.getLogger(HumanPlayerInitializer.class);

    private static final String PLAYER_PERSON_UUID = "playerPerson";

    private final NationalityRepository nationalityRepository;
    private final PersonRepository personRepository;
    private final PlayerFactory playerFactory;

    public void createHumanPlayer(StartGameData data) {
        ParamAssertion.isNotNull(data);
        LOG.info("Creating human player");
        Person playerPerson = createPersonFromStartGameData(data);
        personRepository.save(playerPerson);

        Player humanPlayer = playerFactory.createPlayer(PLAYER_PERSON_UUID, data.getPlayerCompanyUuid(), PlayerType.HUMAN);
        HumanPlayerContext.newInstance(humanPlayer);
    }

    private Person createPersonFromStartGameData(StartGameData data) {
        Nationality personNationality = nationalityRepository.findOne(data.getPlayerNationalityUuid());

        Person playerPerson = Person.builder()
                .firstName(data.getPlayerFirstName())
                .lastName(data.getPlayerLastName())
                .birthDate(data.getPlayerBirthDate())
                .nationality(personNationality)
                .build();

        playerPerson.setUuid(PLAYER_PERSON_UUID);
        return playerPerson;
    }


}
