package szczepanski.gerard.cargo.simulator.initializer.initializer;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.simulator.domain.bank.Bank;
import szczepanski.gerard.cargo.simulator.simulator.domain.bank.BankProvider;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.Currency;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.CurrencyRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.Money;
import szczepanski.gerard.cargo.simulator.simulator.domain.ship.ShipYard;
import szczepanski.gerard.cargo.simulator.simulator.domain.ship.ShipYardProvider;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class GlobalModelInitializer {
	
	private static final Logger LOG = Logger.getLogger(GlobalModelInitializer.class);
	private static final String USC_CURRENCY_ID = "currency001";
	
	private final CurrencyRepository currencyRepository;
	private final ShipYardProvider shipYardProvider;
	
	public void initializeModelComponents() {
		LOG.info("Setting up ShipYard");
		shipYardProvider.setShipYard(new ShipYard());	
		
		LOG.info("Setting up Bank");
		setUpSimulatorBank();
	}
	
	private void setUpSimulatorBank() {
		Currency usdCurrency = currencyRepository.findOne(USC_CURRENCY_ID);
		Money initialBankBalance = Money.of("300000000", usdCurrency);
		BankProvider.set(new Bank(initialBankBalance));
	}
}
