package szczepanski.gerard.cargo.simulator.initializer.initializer;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import szczepanski.gerard.cargo.simulator.engine.engine.GameEngine;
import szczepanski.gerard.cargo.simulator.initializer.dto.StartGameData;
import szczepanski.gerard.cargo.simulator.initializer.loader.GlobalModelLoader;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class GameInitializer {

    private final GlobalModelLoader modelLoader;
    private final GlobalModelInitializer globalModelInitializer;
    private final HumanPlayerInitializer humanPlayerInitializer;
    private final ComputerPlayerInitializer computerPlayerInitializer;
    private final GameEngine gameEngine;

    public final void initialize(StartGameData data) {
        modelLoader.loadModel();
        globalModelInitializer.initializeModelComponents();
        humanPlayerInitializer.createHumanPlayer(data);
        computerPlayerInitializer.createComputerPlayers();
        gameEngine.startNewGame();
    }

}
