package szczepanski.gerard.cargo.simulator.initializer.initializer;

import lombok.RequiredArgsConstructor;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import szczepanski.gerard.cargo.simulator.common.collections.CollectionFactory;
import szczepanski.gerard.cargo.simulator.engine.computer.intelligence.ComputerIntelligence;
import szczepanski.gerard.cargo.simulator.engine.computer.intelligence.ComputerIntelligenceFactory;
import szczepanski.gerard.cargo.simulator.engine.computer.intelligence.ComputerIntelligencePool;
import szczepanski.gerard.cargo.simulator.engine.player.HumanPlayerContext;
import szczepanski.gerard.cargo.simulator.engine.player.Player;
import szczepanski.gerard.cargo.simulator.engine.player.PlayerType;
import szczepanski.gerard.cargo.simulator.simulator.domain.company.Company;
import szczepanski.gerard.cargo.simulator.simulator.domain.company.SeaTransportCompany;
import szczepanski.gerard.cargo.simulator.simulator.domain.company.SeaTransportCompanyRepository;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class ComputerPlayerInitializer {
    private static final Logger LOG = Logger.getLogger(ComputerPlayerInitializer.class);

    private final SeaTransportCompanyRepository seaTransportCompanyRepository;
    private final ComputerIntelligenceFactory computerIntelligenceFactory;
    private final ComputerIntelligencePool computerIntelligencePool;

    public void createComputerPlayers() {
        Set<Company> allComputerCompanies = getAllComputerCompanies();

        allComputerCompanies.forEach(c -> {
            Player computerPlayer = createComputerPlayer(c);
            ComputerIntelligence computerIntelligenceForPlayer = computerIntelligenceFactory.createForPlayer(computerPlayer);
            computerIntelligencePool.register(computerIntelligenceForPlayer);
        });

        LOG.info(String.format("Created %s Computer intelligence players", computerIntelligencePool.getComputerIntelligenceSet().size()));
    }

    private Set<Company> getAllComputerCompanies() {
        Set<Company> companies = CollectionFactory.set();

        String humanCompanyUuid = HumanPlayerContext.get().getPlayer().getCompany().getUuid();
        Collection<SeaTransportCompany> seaTransportCompanies = seaTransportCompanyRepository.getAll().stream()
                .filter(c -> !c.getUuid().equals(humanCompanyUuid)).collect(Collectors.toSet());

        companies.addAll(seaTransportCompanies);
        // TODO add other companies

        return companies;
    }

    private Player createComputerPlayer(Company company) {
        Player computerPlayer = Player.builder().playerType(PlayerType.SI).company(company).person(company.getCompanyInfo().getCeo()).build();
        return computerPlayer;
    }

}
