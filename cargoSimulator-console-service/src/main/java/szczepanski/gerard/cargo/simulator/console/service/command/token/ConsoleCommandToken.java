package szczepanski.gerard.cargo.simulator.console.service.command.token;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ConsoleCommandToken {
	
	CMD("Commands allowed: \nCREATE\nSET\nFIND\nLIST\nSAVE\nDATE\nADDMONEY\nBANK BALANCE\nLOCATIONPOINTS", false),
	ADDMONEY("ADDMONEY [companyUuid] [value]", false),
	LOCATIONPOINTS("LOCATIONPOINTS [ON/OFF]", false),
	BANK("BANK BALANCE", false),
	CREATE("CREATE [domain] args", true),
	BUY("BUY [domain] args", true),
	SET("SET [domain] args", true),
	FIND("FIND [domain] [uuid]", true),
	LIST("LIST [domain] [ASC/DESC]", true),
	DATE("DATE futureDate(dd.mm.yyyy) futureTime(HH:MM)", false),
	SAVE("SAVE saveName", false);
	
	private final String helpString;
	private final boolean needDispatchingToken;
	
}
