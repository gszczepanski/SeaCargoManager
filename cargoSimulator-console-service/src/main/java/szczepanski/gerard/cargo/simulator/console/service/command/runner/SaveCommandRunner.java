package szczepanski.gerard.cargo.simulator.console.service.command.runner;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.util.ParamAssertion;
import szczepanski.gerard.cargo.simulator.console.service.command.token.ConsoleCommandToken;
import szczepanski.gerard.cargo.simulator.storage.service.storage.StorageService;

@Component("saveCommandRunner")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class SaveCommandRunner implements ConsoleCommandRunner {
	
	private static final String SAVE_SCHEMA = "SAVE proper schema: SAVE saveName";
	private static final String SAVE_SUCCESS = "Save success";
	
	private final StorageService storageService;
	
	@Override
	public String run(ConsoleCommandToken commandToken, List<String> arguments) {
		ParamAssertion.isNotNull(commandToken, arguments);
		
		if (arguments.size() != 1) {
			return SAVE_SCHEMA;
		}
		
		String fileName = arguments.get(0);
		storageService.saveGameState(fileName);
		
		return SAVE_SUCCESS;
	}
	
	
	
}
	
	
	
