package szczepanski.gerard.cargo.simulator.console.service.command.resolver;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.console.service.command.dispatcher.ConsoleCommandDispatcherFacade;
import szczepanski.gerard.cargo.simulator.console.service.command.token.ConsoleCommandToken;
import szczepanski.gerard.cargo.simulator.console.service.command.token.ConsoleDispatchingToken;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class ConsoleCommandResolverImpl implements ConsoleCommandResolver {

	private static final String INCORRECT_TOKEN = "Command string is incorrect. Check valid commands by typing CMD";
	
	private static final int COMMAND_TOKEN_INDEX = 0;
	private static final int DISPATCH_TOKEN_INDEX = 1;
	
	private final ConsoleCommandDispatcherFacade consoleCommandDispatcherFacade;
	private final TokenParser tokenParser = new TokenParser();
	
	@Override
	public String resolveTokens(List<String> tokens) {
		try {
			return processParseTokens(tokens);
		} catch(TokenIsNotValidException ex) {
			return INCORRECT_TOKEN;
		}
	}
	
	private String processParseTokens(List<String> tokens) throws TokenIsNotValidException {
		ConsoleCommandToken commandToken = tokenParser.parseCommandToken(tokens.get(COMMAND_TOKEN_INDEX));
		if (tokens.size() == 1 || commandToken == ConsoleCommandToken.CMD) {
			return commandToken.getHelpString();
		}
		
		List<String> arguments;
		if (commandToken.isNeedDispatchingToken()) {
			ConsoleDispatchingToken dispatchingToken = tokenParser.parseDispatchingToken(tokens.get(DISPATCH_TOKEN_INDEX));
			arguments = tokens.subList(2, tokens.size());
			return consoleCommandDispatcherFacade.dispatch(commandToken, dispatchingToken, arguments);
		} else {
			arguments = tokens.subList(1, tokens.size());
			return consoleCommandDispatcherFacade.dispatch(commandToken, arguments);
		}
	}
}
