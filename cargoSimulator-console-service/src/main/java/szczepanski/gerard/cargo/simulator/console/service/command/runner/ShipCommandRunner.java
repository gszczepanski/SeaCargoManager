package szczepanski.gerard.cargo.simulator.console.service.command.runner;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.util.DateTimeUtils;
import szczepanski.gerard.cargo.simulator.common.util.ParamAssertion;
import szczepanski.gerard.cargo.simulator.console.service.command.token.ConsoleCommandToken;
import szczepanski.gerard.cargo.simulator.simulator.domain.company.SeaTransportCompanyRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.port.PortRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.ship.Ship;
import szczepanski.gerard.cargo.simulator.simulator.domain.ship.ShipModelRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.ship.ShipRepository;
import szczepanski.gerard.cargo.simulator.simulator.service.ship.businesslogic.ShipyardService;

@Component("shipCommandRunner")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class ShipCommandRunner implements ConsoleCommandRunner {
	
	private static final String NOT_SUPPORTED_OPERATION = "Not supported operation for Ship";
	private static final String CREATE_SHIP_SCHEMA = "Buy Ship proper schema: BUY SHIP shipModelUuid companyUuid portUuid";
	private static final Comparator<Ship> ASC_SORT = (s1, s2) -> {return s1.getUuid().compareTo(s2.getUuid());}; 
	private static final Comparator<Ship> DESC_SORT = (s1, s2) -> {return s2.getUuid().compareTo(s1.getUuid());}; 
	
	private final ShipyardService shipyardService;
	private final SeaTransportCompanyRepository seaTransportCompanyRepository;
	private final ShipModelRepository shipModelRepository;
	private final ShipRepository shipRepository;
	private final PortRepository portRepository;
	
	@Override
	public String run(ConsoleCommandToken commandToken, List<String> arguments) {
		ParamAssertion.isNotNull(commandToken, arguments);
		String executionMessage = NOT_SUPPORTED_OPERATION;
		
		switch (commandToken) {
		case BUY:
			executionMessage = buy(arguments);
			break;
		case LIST:
			executionMessage = show(arguments);
			break;
		case FIND:
			executionMessage = find(arguments);
			break;
		default:
			break;
		}
		
		return executionMessage;
	}
	
	private String buy(List<String> arguments) {
		if (arguments.size() != 3) {
			return CREATE_SHIP_SCHEMA;
		}
		
		String shipModelUuid = arguments.get(0);
		if (!shipModelRepository.isInRepository(shipModelUuid)) {
			return "Could not find ship model with uuid " + shipModelUuid;
		}
		
		String companyUuid = arguments.get(1);
		if (!seaTransportCompanyRepository.isInRepository(companyUuid)) {
			return "Could not find sea transport company with uuid " + companyUuid;
		}
		
		String portUuid = arguments.get(2);
		if (!portRepository.isInRepository(portUuid)) {
			return "Could not find port with uuid " + portUuid;
		}
		
		LocalDateTime shipBuildTime = shipyardService.buyShip(shipModelUuid, companyUuid, portUuid);
		return String.format("Ship has been ordered. Estimated delivery time: %s", DateTimeUtils.getDateTime(shipBuildTime));
	}
	
	private String show(List<String> arguments) {
		Comparator<Ship> order = ASC_SORT;
		if (!arguments.isEmpty() && arguments.get(0).toUpperCase().equals("DESC")) {
			order = DESC_SORT;
		}
		
		return "\n" + shipRepository.getAll().stream()
				.sorted(order)
				.map(Ship::toString)
				.collect(Collectors.joining("\n"));
	}
	
	private String find(List<String> arguments) {
		if (arguments.isEmpty()) {
			return "You must specify Uuid of Ship";
		}
		String uuid = arguments.get(0);
		Ship ship = shipRepository.findOne(uuid);
		
		if (ship == null) {
			return String.format("Ship with uuid %s not found", uuid);
		}
		
		return ship.toString();
	}
	
}
	
	
	
