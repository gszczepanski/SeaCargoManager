package szczepanski.gerard.cargo.simulator.console.service.command.runner;

import java.util.List;

import szczepanski.gerard.cargo.simulator.console.service.command.token.ConsoleCommandToken;

public interface ConsoleCommandRunner {
	
	String run(ConsoleCommandToken commandToken, List<String> arguments);
	
}
