package szczepanski.gerard.cargo.simulator.console.service.command.resolver;

import szczepanski.gerard.cargo.simulator.common.exception.CargoSimulatorException;

class TokenIsNotValidException extends CargoSimulatorException {
	private static final long serialVersionUID = -5476350493454506122L;

}
