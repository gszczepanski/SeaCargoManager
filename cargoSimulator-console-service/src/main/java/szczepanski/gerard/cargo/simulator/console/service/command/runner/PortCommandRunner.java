package szczepanski.gerard.cargo.simulator.console.service.command.runner;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.util.ParamAssertion;
import szczepanski.gerard.cargo.simulator.console.service.command.token.ConsoleCommandToken;
import szczepanski.gerard.cargo.simulator.simulator.domain.port.Port;
import szczepanski.gerard.cargo.simulator.simulator.domain.port.PortRepository;

@Component("portCommandRunner")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class PortCommandRunner implements ConsoleCommandRunner {
	
	private static final String NOT_SUPPORTED_OPERATION = "Not supported operation for Port";
	private static final Comparator<Port> ASC_SORT = (s1, s2) -> {return s1.getUuid().compareTo(s2.getUuid());}; 
	private static final Comparator<Port> DESC_SORT = (s1, s2) -> {return s2.getUuid().compareTo(s1.getUuid());}; 
	
	private final PortRepository portRepository;
	
	@Override
	public String run(ConsoleCommandToken commandToken, List<String> arguments) {
		ParamAssertion.isNotNull(commandToken, arguments);
		String executionMessage = NOT_SUPPORTED_OPERATION;
		
		switch (commandToken) {
		case LIST:
			executionMessage = show(arguments);
			break;
		case FIND:
			executionMessage = find(arguments);
			break;
		default:
			break;
		}
		
		return executionMessage;
	}
	
	private String show(List<String> arguments) {
		Comparator<Port> order = ASC_SORT;
		if (!arguments.isEmpty() && arguments.get(0).toUpperCase().equals("DESC")) {
			order = DESC_SORT;
		}
		
		return "\n" + portRepository.getAll().stream()
				.sorted(order)
				.map(Port::toString)
				.collect(Collectors.joining("\n"));
	}
	
	private String find(List<String> arguments) {
		if (arguments.isEmpty()) {
			return "You must specify Uuid of Port";
		}
		String uuid = arguments.get(0);
		Port port = portRepository.findOne(uuid);
		
		if (port == null) {
			return String.format("Port with uuid %s not found", uuid);
		}
		
		return port.toString();
	}
	
}
	
	
	
