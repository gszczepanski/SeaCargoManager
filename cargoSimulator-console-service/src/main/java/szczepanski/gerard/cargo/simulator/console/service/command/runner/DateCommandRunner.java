package szczepanski.gerard.cargo.simulator.console.service.command.runner;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import szczepanski.gerard.cargo.simulator.common.util.DateTimeUtils;
import szczepanski.gerard.cargo.simulator.common.util.ParamAssertion;
import szczepanski.gerard.cargo.simulator.common.util.StringUtils;
import szczepanski.gerard.cargo.simulator.console.service.command.token.ConsoleCommandToken;
import szczepanski.gerard.cargo.simulator.engine.time.TimeService;

import java.time.LocalDateTime;
import java.util.List;

@Component("dateCommandRunner")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class DateCommandRunner implements ConsoleCommandRunner {

    private static final String DATE_SCHEMA = "DATE proper schema: DATE futureDate(dd.mm.yyyy) futureTime(HH:MM)";
    private static final String DATE_CHANGE_SUCCESS = "DATE successfully changed";

    private final TimeService timeService;

    @Override
    public String run(ConsoleCommandToken commandToken, List<String> arguments) {
        ParamAssertion.isNotNull(commandToken, arguments);
        if (arguments.size() != 2) {
            return DATE_SCHEMA;
        }

        String newDate = arguments.get(0);
        String newTime = arguments.get(1);
        String newDateTimeString = newDate + StringUtils.SPACE + newTime;
        LocalDateTime newStartDate = DateTimeUtils.parseDateTime(newDateTimeString);

        timeService.skipDateTo(newStartDate);
        return DATE_CHANGE_SUCCESS;
    }

}
