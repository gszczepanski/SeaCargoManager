package szczepanski.gerard.cargo.simulator.console.service.command.runner;

import java.util.List;

import org.springframework.stereotype.Component;

import szczepanski.gerard.cargo.simulator.common.config.DevelopmentVariables;
import szczepanski.gerard.cargo.simulator.console.service.command.token.ConsoleCommandToken;

@Component("locationPointCommandRunner")
class LocationPointCommandRunner implements ConsoleCommandRunner {
	
	private static final String LOCATION_POINT_ON_OFF_SCHEMA = "LOCATIONPOINT [ON/OFF]";
	
	@Override
	public String run(ConsoleCommandToken commandToken, List<String> arguments) {
		if (arguments.size() != 1) {
			return LOCATION_POINT_ON_OFF_SCHEMA;
		}
		
		Boolean turnOn = arguments.get(0).equalsIgnoreCase("ON");
		DevelopmentVariables.SHOW_ROUTES_GRAPH_FOR_DEV = turnOn;
		
		return "Show Location Points: " + turnOn;
	}

}
