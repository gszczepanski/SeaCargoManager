package szczepanski.gerard.cargo.simulator.console.service.command.resolver;

import szczepanski.gerard.cargo.simulator.console.service.command.token.ConsoleCommandToken;
import szczepanski.gerard.cargo.simulator.console.service.command.token.ConsoleDispatchingToken;

class TokenParser {
	
	public ConsoleCommandToken parseCommandToken(String token) throws TokenIsNotValidException {
		try {
			return ConsoleCommandToken.valueOf(token.toUpperCase());
		} catch(IllegalArgumentException ex) {
			throw new TokenIsNotValidException();
		}
	}
	
	public ConsoleDispatchingToken parseDispatchingToken(String token) throws TokenIsNotValidException {
		try {
			return ConsoleDispatchingToken.valueOf(token.toUpperCase());
		} catch(IllegalArgumentException ex) {
			throw new TokenIsNotValidException();
		}
	}
	
}
