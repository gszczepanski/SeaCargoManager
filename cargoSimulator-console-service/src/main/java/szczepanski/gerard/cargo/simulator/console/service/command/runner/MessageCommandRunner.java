package szczepanski.gerard.cargo.simulator.console.service.command.runner;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.util.ParamAssertion;
import szczepanski.gerard.cargo.simulator.console.service.command.token.ConsoleCommandToken;
import szczepanski.gerard.cargo.simulator.simulator.domain.message.Message;
import szczepanski.gerard.cargo.simulator.simulator.domain.message.MessageRepository;

@Component("messageCommandRunner")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class MessageCommandRunner implements ConsoleCommandRunner {
	
	private static final String NOT_SUPPORTED_OPERATION = "Not supported operation for Message";
	private static final Comparator<Message> ASC_SORT = (s1, s2) -> {return s1.getUuid().compareTo(s2.getUuid());}; 
	private static final Comparator<Message> DESC_SORT = (s1, s2) -> {return s2.getUuid().compareTo(s1.getUuid());}; 
	
	private final MessageRepository messageRepository;
	
	@Override
	public String run(ConsoleCommandToken commandToken, List<String> arguments) {
		ParamAssertion.isNotNull(commandToken, arguments);
		String executionMessage = NOT_SUPPORTED_OPERATION;
		
		switch (commandToken) {
		case LIST:
			executionMessage = show(arguments);
			break;
		default:
			break;
		}
		
		return executionMessage;
	}
	
	private String show(List<String> arguments) {
		Comparator<Message> order = ASC_SORT;
		if (!arguments.isEmpty() && arguments.get(0).toUpperCase().equals("DESC")) {
			order = DESC_SORT;
		}
		
		return "\n" + messageRepository.getAll().stream()
				.sorted(order)
				.map(Message::toString)
				.collect(Collectors.joining("\n"));
	}
	
}
	
	
	
