package szczepanski.gerard.cargo.simulator.console.service.command.dispatcher;

import java.util.List;

import szczepanski.gerard.cargo.simulator.console.service.command.token.ConsoleCommandToken;
import szczepanski.gerard.cargo.simulator.console.service.command.token.ConsoleDispatchingToken;

public interface ConsoleCommandDispatcherFacade {
	
	String dispatch(ConsoleCommandToken commandToken, List<String> arguments);
	
	String dispatch(ConsoleCommandToken commandToken, ConsoleDispatchingToken dispatchingToken, List<String> arguments);
	
}
