package szczepanski.gerard.cargo.simulator.console.service.command.runner;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.util.ParamAssertion;
import szczepanski.gerard.cargo.simulator.console.service.command.token.ConsoleCommandToken;
import szczepanski.gerard.cargo.simulator.simulator.domain.company.Company;
import szczepanski.gerard.cargo.simulator.simulator.domain.company.SeaTransportCompanyRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.Money;

@Component("moneyCommandRunner")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class MoneyCommandRunner implements ConsoleCommandRunner {
	
	private static final String MONEY_SCHEMA = "ADD_MONEY proper schema: ADD_MONEY seaTransportCompanyUuid value";
	private static final String MONEY_CHANGE_SUCCESS = "MONEY successfully updated to ";
	private static final String COMPANY_NOT_FOUND = "Sea Transport Company with given UUID not found";
	
	private final SeaTransportCompanyRepository companyRepository;
	
	@Override
	public String run(ConsoleCommandToken commandToken, List<String> arguments) {
		ParamAssertion.isNotNull(commandToken, arguments);
		
		if (arguments.size() != 2) {
			return MONEY_SCHEMA;
		}
		
		String companyUuid = arguments.get(0);
		String value = arguments.get(1);
		
		Company company = companyRepository.findOne(companyUuid);
		
		if (company == null) {
			return COMPANY_NOT_FOUND;
		}
		
		Money balance = company.getCompanyFinancesInfo().getCurrentBalance();
		Money moneyToAdd = Money.of(value, balance.getCurrency());
		company.getCompanyFinancesInfo().addMoney(moneyToAdd);
		
		return MONEY_CHANGE_SUCCESS + company.getCompanyFinancesInfo().getCurrentBalance();
	}

}
