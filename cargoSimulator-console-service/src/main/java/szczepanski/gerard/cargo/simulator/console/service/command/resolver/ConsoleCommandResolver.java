package szczepanski.gerard.cargo.simulator.console.service.command.resolver;

import java.util.List;

@FunctionalInterface
public interface ConsoleCommandResolver {
	
	String resolveTokens(List<String> tokens);
	
}
