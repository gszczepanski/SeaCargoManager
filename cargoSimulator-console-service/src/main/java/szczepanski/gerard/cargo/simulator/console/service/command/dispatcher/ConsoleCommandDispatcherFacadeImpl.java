package szczepanski.gerard.cargo.simulator.console.service.command.dispatcher;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import szczepanski.gerard.cargo.simulator.common.util.ParamAssertion;
import szczepanski.gerard.cargo.simulator.console.service.command.runner.ConsoleCommandRunner;
import szczepanski.gerard.cargo.simulator.console.service.command.token.ConsoleCommandToken;
import szczepanski.gerard.cargo.simulator.console.service.command.token.ConsoleDispatchingToken;

@Component
class ConsoleCommandDispatcherFacadeImpl implements ConsoleCommandDispatcherFacade {

	private static final String DEFAULT_DISPATCH_MESSAGE = "Unable to dispatch token";
	
	@Autowired
	@Qualifier("portCommandRunner")
	private ConsoleCommandRunner portCommandRunner;
	@Autowired
	@Qualifier("shipCommandRunner")
	private ConsoleCommandRunner shipCommandRunner;
	@Autowired
	@Qualifier("routeCommandRunner")
	private ConsoleCommandRunner routeCommandRunner;
	@Autowired
	@Qualifier("saveCommandRunner")
	private ConsoleCommandRunner saveCommandRunner;
	@Autowired
	@Qualifier("dateCommandRunner")
	private ConsoleCommandRunner dateCommandRunner;
	@Autowired
	@Qualifier("moneyCommandRunner")
	private ConsoleCommandRunner moneyCommandRunner;
	@Autowired
	@Qualifier("bankCommandRunner")
	private ConsoleCommandRunner bankCommandRunner;
	@Autowired
	@Qualifier("messageCommandRunner")
	private ConsoleCommandRunner messageCommandRunner;
	@Autowired
	@Qualifier("locationPointCommandRunner")
	private ConsoleCommandRunner locationPointCommandRunner;

	@Override
	public String dispatch(ConsoleCommandToken commandToken, ConsoleDispatchingToken dispatchingToken, List<String> arguments) {
		ParamAssertion.isNotNull(commandToken, dispatchingToken);
		String returningMessage = DEFAULT_DISPATCH_MESSAGE;
		
		switch (dispatchingToken) {
		case ROUTE:
			returningMessage = routeCommandRunner.run(commandToken, arguments);
			break;
		case SHIP:
			returningMessage = shipCommandRunner.run(commandToken, arguments);
			break;
		case PORT:
			returningMessage = portCommandRunner.run(commandToken, arguments);
			break;
		case MESSAGE:
			returningMessage = messageCommandRunner.run(commandToken, arguments);
			break;
		default:
			break;
		}

		return returningMessage;
	}

	@Override
	public String dispatch(ConsoleCommandToken commandToken, List<String> arguments) {
		ParamAssertion.isNotNull(commandToken);
		String returningMessage = DEFAULT_DISPATCH_MESSAGE;

		switch (commandToken) {
		case SAVE:
			returningMessage = saveCommandRunner.run(commandToken, arguments);
			break;
		case DATE:
			returningMessage = dateCommandRunner.run(commandToken, arguments);
			break;
		case ADDMONEY:
			returningMessage = moneyCommandRunner.run(commandToken, arguments);
			break;
		case BANK:
			returningMessage = bankCommandRunner.run(commandToken, arguments);
			break;
		case LOCATIONPOINTS:
			returningMessage = locationPointCommandRunner.run(commandToken, arguments);
			break;
		default:
			break;
		}

		return returningMessage;
	}

}
