package szczepanski.gerard.cargo.simulator.console.service.command.runner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.util.DateTimeUtils;
import szczepanski.gerard.cargo.simulator.common.util.ParamAssertion;
import szczepanski.gerard.cargo.simulator.common.util.StringUtils;
import szczepanski.gerard.cargo.simulator.console.service.command.token.ConsoleCommandToken;
import szczepanski.gerard.cargo.simulator.simulator.domain.port.PortRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.route.Route;
import szczepanski.gerard.cargo.simulator.simulator.domain.route.RouteRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.route.RouteState;
import szczepanski.gerard.cargo.simulator.simulator.domain.ship.ShipRepository;
import szczepanski.gerard.cargo.simulator.simulator.service.ship.businesslogic.ShipService;
import szczepanski.gerard.cargo.simulator.simulator.service.transport.businesslogic.RouteService;

@Component("routeCommandRunner")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class RouteCommandRunner implements ConsoleCommandRunner {
	
	private static final String NOT_SUPPORTED_OPERATION = "Not supported operation for Ship";
	private static final String CREATE_ROUTE_SCHEMA = "Create Route proper schema: CREATE ROUTE startDate(dd.mm.yyyy) startTime(HH:MM) startPortUuid destinationPortUuid";
	private static final String SET_ROUTE_SCHEMA = "Set Route proper schema: SET ROUTE routeUuid shipUuid";
	private static final Comparator<Route> ASC_SORT = (s1, s2) -> {return s1.getUuid().compareTo(s2.getUuid());}; 
	private static final Comparator<Route> DESC_SORT = (s1, s2) -> {return s2.getUuid().compareTo(s1.getUuid());}; 
	
	private final PortRepository portRepository;
	private final RouteRepository routeRepository;
	private final RouteService routeService;
	private final ShipService shipService;
	private final ShipRepository shipRepository;
	
	@Override
	public String run(ConsoleCommandToken commandToken, List<String> arguments) {
		ParamAssertion.isNotNull(commandToken, arguments);
		String executionMessage = NOT_SUPPORTED_OPERATION;
		
		switch (commandToken) {
		case CREATE:
			executionMessage = create(arguments);
			break;
		case SET:
			executionMessage = set(arguments);
			break;
		case LIST:
			executionMessage = show(arguments);
			break;
		case FIND:
			executionMessage = find(arguments);
			break;
		default:
			break;
		}
		
		return executionMessage;
	}
	
	private String create(List<String> arguments) {
		if (arguments.size() < 4) {
			return CREATE_ROUTE_SCHEMA;
		}
		
		String routeDate = arguments.get(0);
		String routeTime = arguments.get(1);
		String routeDateTimeString = routeDate + StringUtils.SPACE + routeTime;
		LocalDateTime routeStartDate = DateTimeUtils.parseDateTime(routeDateTimeString);
		
		String startPortUuid = arguments.get(2);
		if (!portRepository.isInRepository(startPortUuid)) {
			return "Could not find port with uuid " + startPortUuid;
		}
		
		String endPortUuid = arguments.get(3);
		if (!portRepository.isInRepository(endPortUuid)) {
			return "Could not find port with uuid " + endPortUuid;
		}
		
		List<String> routePortsUuids = new ArrayList<>();
		routePortsUuids.add(startPortUuid);
		routePortsUuids.add(endPortUuid);
		
		String newRouteUuid = routeService.createRoute(routeStartDate, routePortsUuids);
		return String.format("Route with uuid %s has been created", newRouteUuid);
	}
	
	private String set(List<String> arguments) {
		if (arguments.size() < 2) {
			return SET_ROUTE_SCHEMA;
		}
		
		String routeUuid = arguments.get(0);
		if (!routeRepository.isInRepository(routeUuid)) {
			return "Could not find Route with uuid " + routeUuid;
		}
		
		String shipUuid = arguments.get(1);
		if (!shipRepository.isInRepository(shipUuid)) {
			return "Could not find Ship with uuid " + shipUuid;
		}
		
		if (shipRepository.isRouteInAnyShip(routeUuid)) {
			return "Route is already assigned to ship";
		}
		
		Route route = routeRepository.findOne(routeUuid);
		if (route.getRouteState() != RouteState.PLANNED) {
			return "Route cannot be assigned because is not in PLANNED state";
		}
		
		shipService.setRouteToShip(shipUuid, routeUuid);
		return String.format("Route %s successfully assigned to Ship %s", routeUuid, shipUuid);
	}
	
	private String show(List<String> arguments) {
		Comparator<Route> order = ASC_SORT;
		if (!arguments.isEmpty() && arguments.get(0).toUpperCase().equals("DESC")) {
			order = DESC_SORT;
		}
		
		return "\n" + routeRepository.getAll().stream()
				.sorted(order)
				.map(Route::toString)
				.collect(Collectors.joining("\n"));
	}
	
	private String find(List<String> arguments) {
		if (arguments.isEmpty()) {
			return "You must specify Uuid of Route";
		}
		String uuid = arguments.get(0);
		Route route = routeRepository.findOne(uuid);
		
		if (route == null) {
			return String.format("Route with uuid %s not found", uuid);
		}
		
		return route.toString();
	}
	
}
	
	
	
