package szczepanski.gerard.cargo.simulator.console.service.command.runner;

import java.util.List;

import org.springframework.stereotype.Component;

import szczepanski.gerard.cargo.simulator.common.util.StringUtils;
import szczepanski.gerard.cargo.simulator.console.service.command.token.ConsoleCommandToken;
import szczepanski.gerard.cargo.simulator.simulator.domain.bank.BankProvider;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.Money;

@Component("bankCommandRunner")
class BankCommandRunner implements ConsoleCommandRunner {
	
	@Override
	public String run(ConsoleCommandToken commandToken, List<String> arguments) {
		Money bankMoney = BankProvider.get().getCurrentBalance();
		return StringUtils.formatNumeric(bankMoney.getValue().doubleValue()) + StringUtils.SPACE + bankMoney.getCurrency().getCode();
	}

}
