package szczepanski.gerard.cargo.simulator.console.service.command.token;

public enum ConsoleDispatchingToken {
	
	PORT,
	SHIP,
	ROUTE,
	CITY,
	MESSAGE;
	
}
