/**
 * 
 */
package szczepanski.gerard.cargo.simulator.ui.view.common;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;


public abstract class FactoryProvider<T> {
	private static final Logger LOG = Logger.getLogger(FactoryProvider.class);
	
	private final Map<String, FxmlComponentFactory<T>> factoryContainer = new HashMap<>();
	
	public void registerFactory(String key, FxmlComponentFactory<T> factory) {
		LOG.debug(String.format("Registering factory: [%s] under key: [%s]", factory.getClass().getName(), key));
		factoryContainer.put(key, factory);
	}
	
	public FxmlComponentFactory<T> getFactory(String factoryKey) {
		return factoryContainer.get(factoryKey);
	}

}
