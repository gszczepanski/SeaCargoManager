package szczepanski.gerard.cargo.simulator.ui.view.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FactoryKeys {
	
	public static final String GAME_SCENE = "gameScene";
	public static final String GAME_TOP_TOOLBAR_PANE = "gameToolbarPane";
	public static final String GAME_WORLD_MAP_PANE = "gameWorldMapPane";
	public static final String GAME_FOOTER_PANE = "gameFooterPane";
	
	public static final String SHIP_INFO_PANE = "shipInfoPane";
}
