package szczepanski.gerard.cargo.simulator.ui.view.game.world.map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javafx.scene.control.DialogPane;
import javafx.scene.layout.Pane;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.config.UuidPrefix;
import szczepanski.gerard.cargo.simulator.ui.view.common.FactoryKeys;
import szczepanski.gerard.cargo.simulator.ui.view.common.PaneFactoryProvider;
import szczepanski.gerard.cargo.simulator.ui.view.utils.FxmlUtils;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class WorldMapEventResolver {
	
	private final PaneFactoryProvider paneFactoryProvider;
	
	public void showInfoPanel(String uuid) {
		if (uuid.startsWith(UuidPrefix.SHIP_UUID_PREFIX)) {
			displayInfoPane(FactoryKeys.SHIP_INFO_PANE, uuid);
		}
		//TODO other panes
	}
	
	private void displayInfoPane(String factoryKey, String uuid) {
		Pane pane = paneFactoryProvider.getFactory(factoryKey).createComponentWithParams(uuid);
		FxmlUtils.showDialogPane((DialogPane) pane);
	}
	

}
