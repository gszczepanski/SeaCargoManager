package szczepanski.gerard.cargo.simulator.ui.view.game.world.map;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;
import szczepanski.gerard.cargo.simulator.common.exception.CargoSimulatorRuntimeException;
import szczepanski.gerard.cargo.simulator.simulator.service.ship.businesslogic.ShipService;
import szczepanski.gerard.cargo.simulator.ui.view.common.FxmlComponentFactory;

@Component
public class ShipInfoPaneFactory extends FxmlComponentFactory<Pane> {
	
	private static final String PATH = "/templates/game/pane/dialogPane/ShipInfoDialogPane.fxml";
	
	private final ShipService shipService;
	
	@Autowired
	public ShipInfoPaneFactory(ShipService shipService) {
		super(PATH);
		this.shipService = shipService;
	}

	@Override
	protected Pane generateFxmlComponent(FXMLLoader loader, String... params) {
		String shipUuid = params[0];
		loader.setController(new ShipInfoPaneController(shipUuid, shipService));
		
		try {
			return (Pane) loader.load();
		} catch (IOException e) {
			throw new CargoSimulatorRuntimeException(e);
		}
	}

}
