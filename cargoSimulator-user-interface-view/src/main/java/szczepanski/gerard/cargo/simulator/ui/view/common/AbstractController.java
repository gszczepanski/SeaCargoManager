/**
 * 
 */
package szczepanski.gerard.cargo.simulator.ui.view.common;

import org.springframework.beans.factory.annotation.Autowired;

import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DialogPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import lombok.Getter;
import szczepanski.gerard.cargo.simulator.ui.view.utils.FxmlUtils;

@Getter
public abstract class AbstractController {
	
	@Autowired
	private SceneFactoryProvider sceneFactoryProvider;
	
	@Autowired
	private PaneFactoryProvider paneFactoryProvider;

	protected final void changeScene(ActionEvent event, String sceneKey) {
		Stage programStage = FxmlUtils.getStageFromActionEvent(event);
		Scene nextScene = sceneFactoryProvider.getFactory(sceneKey).createComponent();
		programStage.setScene(nextScene);
	}
	
	protected final Pane getPaneFromFactory(String paneFactoryKey) {
		return paneFactoryProvider.getFactory(paneFactoryKey).createComponent();
	}
	
	protected final void showFormDialog(DialogPane dialogPane) {
		FxmlUtils.showDialogPane(dialogPane);
	}
	
	protected final void setAnchorPane(AnchorPane anchorPane, String paneFactoryKey) {
		double injectedPanePosition = 0.0;
		Pane paneToChange = getPaneFromFactory(paneFactoryKey);
		anchorPane.getChildren().clear();
		AnchorPane.setTopAnchor(paneToChange, injectedPanePosition);
		anchorPane.getChildren().add(paneToChange);
	}

	protected final void closeWindow(Button invokerButton) {
		FxmlUtils.closeSceneFromCancelButton(invokerButton);
	}
	
}
