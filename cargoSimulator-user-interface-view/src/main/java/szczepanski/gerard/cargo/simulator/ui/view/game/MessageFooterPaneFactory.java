package szczepanski.gerard.cargo.simulator.ui.view.game;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;
import szczepanski.gerard.cargo.simulator.common.exception.CargoSimulatorRuntimeException;
import szczepanski.gerard.cargo.simulator.ui.view.common.FxmlComponentFactory;

@Component
public class MessageFooterPaneFactory extends FxmlComponentFactory<Pane> {
	
	private static final String PATH = "/templates/game/pane/MessageFooterPane.fxml";
	
	MessageFooterPaneController messageFooterPaneController;
	
	@Autowired
	public MessageFooterPaneFactory(MessageFooterPaneController messageFooterPaneController) {
		super(PATH);
		this.messageFooterPaneController = messageFooterPaneController;
	}

	@Override
	protected Pane generateFxmlComponent(FXMLLoader loader) {
		loader.setController(messageFooterPaneController);
		try {
			return (Pane) loader.load();
		} catch (IOException e) {
			throw new CargoSimulatorRuntimeException(e);
		}
	}

}
