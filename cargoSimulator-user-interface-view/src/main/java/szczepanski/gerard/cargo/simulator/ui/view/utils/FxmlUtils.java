/**
 * 
 */
package szczepanski.gerard.cargo.simulator.ui.view.utils;

import java.util.Optional;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogPane;
import javafx.stage.Stage;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.config.GlobalImages;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class FxmlUtils {
	
	public static final Stage getStageFromActionEvent(ActionEvent event) {
		 return (Stage) ((Node) event.getSource()).getScene().getWindow();
	}
	
	public static final FXMLLoader getFxmlLoader(String fxmlPath) {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(FxmlUtils.class.getResource(fxmlPath));
		return loader;
	}
	
	public static final Optional<ButtonType> showDialogPane(DialogPane dialogPane) {
		Alert alert = new Alert(AlertType.NONE);
		alert.setDialogPane(dialogPane);
		Stage stage = (Stage) dialogPane.getScene().getWindow();
		stage.getIcons().add(GlobalImages.PROGRAM_ICON);
		return alert.showAndWait();
	}
	
	public static final void closeSceneFromCancelButton(Button button) {
		Stage stage = (Stage) button.getScene().getWindow();
		stage.close();
	}
}
