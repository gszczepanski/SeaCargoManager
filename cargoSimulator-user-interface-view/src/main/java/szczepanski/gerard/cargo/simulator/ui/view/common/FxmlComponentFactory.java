package szczepanski.gerard.cargo.simulator.ui.view.common;

import org.apache.log4j.Logger;

import javafx.fxml.FXMLLoader;

public abstract class FxmlComponentFactory<T> {
	private static final Logger LOG = Logger.getLogger(FxmlComponentFactory.class);

	private final String fxmlPath;

	public FxmlComponentFactory(String fxmlPath) {
		this.fxmlPath = fxmlPath;
	}

	public final T createComponent() {
		LOG.debug("Create FxmlComponent");
		FXMLLoader loader = getLoader();
		return generateFxmlComponent(loader);
	}
	
	public final T createComponentWithParams(String... params) {
		LOG.debug("Create FxmlComponent with params " + params);
		FXMLLoader loader = getLoader();
		return generateFxmlComponent(loader, params);
	}

	private final FXMLLoader getLoader() {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(FxmlComponentFactory.class.getResource(fxmlPath));
		return loader;
	}

	protected T generateFxmlComponent(FXMLLoader loader) {
		// Hook method
		return null;
	}
	
	protected T generateFxmlComponent(FXMLLoader loader, String... params) {
		// Hook method
		return null;
	}

}