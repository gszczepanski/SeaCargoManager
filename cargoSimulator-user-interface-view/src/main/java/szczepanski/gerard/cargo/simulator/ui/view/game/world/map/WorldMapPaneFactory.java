package szczepanski.gerard.cargo.simulator.ui.view.game.world.map;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;
import szczepanski.gerard.cargo.simulator.common.exception.CargoSimulatorRuntimeException;
import szczepanski.gerard.cargo.simulator.ui.component.world.map.WorldMap;
import szczepanski.gerard.cargo.simulator.ui.view.common.FxmlComponentFactory;

@Component
public class WorldMapPaneFactory extends FxmlComponentFactory<Pane> {
	
	private static final String PATH = "/templates/game/pane/EmptyPane.fxml";
	
	private final WorldMapPaneController worldMapPaneController;
	
	@Autowired
	public WorldMapPaneFactory(WorldMapPaneController worldMapPaneController) {
		super(PATH);
		this.worldMapPaneController = worldMapPaneController;
	}

	@Override
	protected Pane generateFxmlComponent(FXMLLoader loader) {
		loader.setController(worldMapPaneController);
		
		try {
			Pane pane = loader.load();
			
			WorldMap worldMap = WorldMap.initialize();
			worldMapPaneController.setWorldMap(worldMap);
			
			pane.getChildren().addAll(worldMap.getWebView());
			return pane;
		} catch (IOException e) {
			throw new CargoSimulatorRuntimeException(e);
		}
	}

}
