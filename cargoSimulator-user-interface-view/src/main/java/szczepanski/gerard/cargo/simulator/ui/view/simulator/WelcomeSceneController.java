package szczepanski.gerard.cargo.simulator.ui.view.simulator;

import java.time.LocalDate;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.initializer.dto.StartGameData;
import szczepanski.gerard.cargo.simulator.initializer.initializer.GameInitializer;
import szczepanski.gerard.cargo.simulator.ui.view.common.AbstractController;
import szczepanski.gerard.cargo.simulator.ui.view.common.FactoryKeys;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class WelcomeSceneController extends AbstractController {
	private static final Logger LOG = Logger.getLogger(WelcomeSceneController.class);
	
	private final GameInitializer worldInitializer;
	
	@FXML
	public void handleStartQuickNewGameBtn(ActionEvent event) {
		LOG.info("handleStartQuickNewGameBtn");
		worldInitializer.initialize(mockStartData());
		changeScene(event, FactoryKeys.GAME_SCENE);
	}
	
	private StartGameData mockStartData() {
		return StartGameData.builder()
				.playerFirstName("Andy")
				.playerLastName("Latimer")
				.playerBirthDate(LocalDate.of(1992, 06, 14))
				.playerCompanyUuid("company0001")
				.playerNationalityUuid("nationality006")
				.numberOfAiLandTransportCompanies(20)
				.numberOfAiSeaTransportCompanies(10)
				.numberOfAiTradingCompanies(70)
				.build();
	}
}
