package szczepanski.gerard.cargo.simulator.ui.view.game.world.map;

import org.apache.log4j.Logger;

import com.google.common.eventbus.Subscribe;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import szczepanski.gerard.cargo.simulator.common.eventbus.EventBusProvider;
import szczepanski.gerard.cargo.simulator.simulator.service.ship.businesslogic.ShipService;
import szczepanski.gerard.cargo.simulator.simulator.service.ship.dto.ShipInfoViewDto;
import szczepanski.gerard.cargo.simulator.simulator.service.time.event.SimulatorDomainUpdatedEvent;
import szczepanski.gerard.cargo.simulator.ui.view.common.AbstractController;

class ShipInfoPaneController extends AbstractController {
	private static final Logger LOG = Logger.getLogger(ShipInfoPaneController.class);
	
	private final String shipUuid;
	private final ShipService shipService;
	
	@FXML
	private Button closeBtn;
	@FXML
	private Label uuidLbl;
	@FXML
	private Label companyLbl;
	@FXML
	private Label routeStateLbl;
	@FXML
	private Label positionLbl;
	@FXML
	private Label startDateLbl;
	@FXML
	private Label startPortLbl;
	@FXML
	private Label destinationPortLbl;
	@FXML
	private Label distanceLbl;
	
	@FXML
	public void initialize() {
		updatePane();
	}
	
	public ShipInfoPaneController(String shipUuid, ShipService shipService) {
		this.shipUuid = shipUuid;
		this.shipService = shipService;
		EventBusProvider.get().register(this);
	}
	
	@FXML
	public void handleCloseBtn() {
		EventBusProvider.get().unregister(this);
		closeWindow(closeBtn);
	}
	
	@Subscribe
	public void updateOnSimulatorDomainUpdatedEvent(SimulatorDomainUpdatedEvent e) {
		Platform.runLater(() -> { 
			updatePane();
		});
	}
	
	private void updatePane() {
		ShipInfoViewDto dto = shipService.getShipInfoDto(shipUuid);
		uuidLbl.setText(dto.getUuid());
		companyLbl.setText(dto.getCompany());
		routeStateLbl.setText(dto.getRouteState());
		positionLbl.setText(dto.getCurrentPosition());
		startDateLbl.setText(dto.getStartRouteDate());
		startPortLbl.setText(dto.getStartPortName());
		destinationPortLbl.setText(dto.getEndPortName());
		distanceLbl.setText(dto.getDistanceTravelled());
	}
	
}
