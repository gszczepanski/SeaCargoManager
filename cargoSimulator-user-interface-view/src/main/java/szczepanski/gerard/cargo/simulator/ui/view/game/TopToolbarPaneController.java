package szczepanski.gerard.cargo.simulator.ui.view.game;

import java.time.LocalDateTime;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.eventbus.Subscribe;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import szczepanski.gerard.cargo.simulator.common.eventbus.EventBusProvider;
import szczepanski.gerard.cargo.simulator.common.util.DateTimeUtils;
import szczepanski.gerard.cargo.simulator.engine.player.HumanPlayerContext;
import szczepanski.gerard.cargo.simulator.engine.time.MinutePassedTimeEvent;
import szczepanski.gerard.cargo.simulator.engine.time.TimeService;
import szczepanski.gerard.cargo.simulator.simulator.service.company.businesslogic.SeaTransportCompanyService;
import szczepanski.gerard.cargo.simulator.simulator.service.company.dto.CompanyInfoDto;
import szczepanski.gerard.cargo.simulator.simulator.service.time.event.SimulatorDomainUpdatedEvent;
import szczepanski.gerard.cargo.simulator.ui.view.common.AbstractController;

@Component
class TopToolbarPaneController extends AbstractController {
	private static final Logger LOG = Logger.getLogger(TopToolbarPaneController.class);

	private final TimeService timeService;
	private final SeaTransportCompanyService seaTransportCompanyService;

	@FXML
	private Label dateLbl;
	@FXML
	private Label timeLbl;
	@FXML
	private Label dayOfWeekLbl;
	@FXML
	private Label companyBalanceLbl;
	@FXML
	private Label reputationLbl;
	
	@Autowired
	public TopToolbarPaneController(TimeService timeService, SeaTransportCompanyService seaTransportCompanyService) {
		this.timeService = timeService;
		this.seaTransportCompanyService = seaTransportCompanyService;
		EventBusProvider.get().register(this);
	}

	@FXML
	public void initialize() {
		initDateTime();
		updatePlayerCompanyInfo();
	}

	@FXML
	public void handlePauseGameTimeBtn() {
		LOG.info("handlePauseGameTimeBtn");
		timeService.pause();
	}

	@FXML
	public void handleRunGameTimeBtn() {
		LOG.info("handleRunGameTimeBtn");
		timeService.start();
	}

	@FXML
	public void handleSpeedUpGameTimeBtn() {
		LOG.info("handleSpeedUpGameTimeBtn");
		timeService.speedUp();
	}
	
	@FXML
	public void handleHourTickGameTimeBtn() {
		LOG.info("handleHourTickGameTimeBtn");
		timeService.hourTick();
	}

	private void initDateTime() {
		LocalDateTime gameTime = timeService.getGameTime();
		dateLbl.setText(DateTimeUtils.getDate(gameTime));
		timeLbl.setText(DateTimeUtils.getTime(gameTime));
		dayOfWeekLbl.setText(DateTimeUtils.getDayOfWeek(gameTime));
	}
	
	@Subscribe
	void handleSimulatorDomainEventUpdated(SimulatorDomainUpdatedEvent e) {
		Platform.runLater(() -> {
			updatePlayerCompanyInfo();
		});
	}
	
	private void updatePlayerCompanyInfo() {
		String playerCompanyUuid = HumanPlayerContext.get().getPlayer().getCompany().getUuid();
		CompanyInfoDto companyInfo = seaTransportCompanyService.fetchCompanyInfo(playerCompanyUuid);
		companyBalanceLbl.setText(companyInfo.getCompanyBalance());
		reputationLbl.setText(companyInfo.getReputation());
	}

	@Component
	static class ToolbarClockUpdater {

		private final TimeService timeService;
		private final TopToolbarPaneController parent;

		@Autowired
		ToolbarClockUpdater(TimeService timeService, TopToolbarPaneController parent) {
			this.timeService = timeService;
			this.parent = parent;
			EventBusProvider.get().register(this);
		}

		@Subscribe
		private void updateOnMinuePassedEvent(MinutePassedTimeEvent e) {
			Platform.runLater(() -> {
				LocalDateTime gameTime = timeService.getGameTime();
				parent.dateLbl.setText(DateTimeUtils.getDate(gameTime));
				parent.timeLbl.setText(DateTimeUtils.getTime(gameTime));
				parent.dayOfWeekLbl.setText(DateTimeUtils.getDayOfWeek(gameTime));
			});
		}

	}

}
