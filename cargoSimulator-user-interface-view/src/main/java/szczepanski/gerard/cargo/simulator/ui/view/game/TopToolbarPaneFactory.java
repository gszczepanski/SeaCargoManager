package szczepanski.gerard.cargo.simulator.ui.view.game;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;
import szczepanski.gerard.cargo.simulator.common.exception.CargoSimulatorRuntimeException;
import szczepanski.gerard.cargo.simulator.ui.view.common.FxmlComponentFactory;

@Component
public class TopToolbarPaneFactory extends FxmlComponentFactory<Pane> {
	
	private static final String PATH = "/templates/game/pane/TopToolbarPane.fxml";
	
	private final TopToolbarPaneController topToolbarPaneController;
	
	@Autowired
	public TopToolbarPaneFactory(TopToolbarPaneController topToolbarPaneController) {
		super(PATH);
		this.topToolbarPaneController = topToolbarPaneController;
	}

	@Override
	protected Pane generateFxmlComponent(FXMLLoader loader) {
		loader.setController(topToolbarPaneController);
		try {
			return (Pane) loader.load();
		} catch (IOException e) {
			throw new CargoSimulatorRuntimeException(e);
		}
	}

}
