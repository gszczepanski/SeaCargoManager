package szczepanski.gerard.cargo.simulator.ui.view.utils;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Properties;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.config.GlobalVariables;
import szczepanski.gerard.cargo.simulator.common.exception.CargoSimulatorRuntimeException;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MessageResolver {
	
	private static final Properties MESSAGES = new Properties();
	
	static {
		try {
			MESSAGES.load(GlobalVariables.class.getResourceAsStream("/messages/messages.properties"));
		} catch (IOException e) {
			throw new CargoSimulatorRuntimeException(e);
		}
	}
	
	public static String resolveMessage(String key, Object... args) {
		return MessageFormat.format(MESSAGES.getProperty(key), args);
	}
	
	public static String resolveMessage(String key) {
		return resolveMessage(key, new Object[]{});
	}
	
}
