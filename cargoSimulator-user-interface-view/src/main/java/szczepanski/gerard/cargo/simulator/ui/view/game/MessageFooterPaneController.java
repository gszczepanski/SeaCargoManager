package szczepanski.gerard.cargo.simulator.ui.view.game;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.eventbus.Subscribe;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import szczepanski.gerard.cargo.simulator.common.eventbus.EventBusProvider;
import szczepanski.gerard.cargo.simulator.simulator.domain.message.Message;
import szczepanski.gerard.cargo.simulator.simulator.service.message.businesslogic.MessageService;
import szczepanski.gerard.cargo.simulator.simulator.service.time.event.SimulatorDomainUpdatedEvent;
import szczepanski.gerard.cargo.simulator.ui.view.common.AbstractController;
import szczepanski.gerard.cargo.simulator.ui.view.utils.MessageResolver;

@Component
class MessageFooterPaneController extends AbstractController {
	private static final Logger LOG = Logger.getLogger(MessageFooterPaneController.class);

	private final MessageService messageService;

	@FXML
	private TextField messagePromptTextField;
	@FXML
	private Label unreadedMessagesLbl;

	@Autowired
	public MessageFooterPaneController(MessageService messageService) {
		this.messageService = messageService;
		EventBusProvider.get().register(this);
	}

	@Subscribe
	private void updateOnSimulatorDomainUpdatedEvent(SimulatorDomainUpdatedEvent e) {
		Platform.runLater(() -> {
			updateUnreadedMessagesLabel();
			updateMessagesPrompt();
		});
	}

	private void updateUnreadedMessagesLabel() {
		int numberOfUnreadedMessages = messageService.getNumberOfUnreadedMessages();
		unreadedMessagesLbl.setText(String.valueOf(numberOfUnreadedMessages));
	}

	private void updateMessagesPrompt() {
		List<Message> messages = messageService.getUnreadMessagesFromPastLatestHours(6);

		if (!messages.isEmpty()) {
			Message message = messages.get(0);
			messagePromptTextField.setText(MessageResolver.resolveMessage(message.getMessageCode(), message.getArgs()));
		}
	}

}
