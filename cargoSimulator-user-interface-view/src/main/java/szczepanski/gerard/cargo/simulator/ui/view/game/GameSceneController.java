package szczepanski.gerard.cargo.simulator.ui.view.game;

import org.springframework.stereotype.Component;

import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import szczepanski.gerard.cargo.simulator.ui.view.common.AbstractController;
import szczepanski.gerard.cargo.simulator.ui.view.common.FactoryKeys;

@Component
class GameSceneController extends AbstractController {
	
	@FXML 
	private AnchorPane topToolbarAnchorPane;
	
	@FXML 
	private AnchorPane gameAnchorPane;
	
	@FXML 
	private AnchorPane footerAnchorPane;
	
	@FXML
	public void initialize() {
		setAnchorPane(topToolbarAnchorPane, FactoryKeys.GAME_TOP_TOOLBAR_PANE);
		setAnchorPane(gameAnchorPane, FactoryKeys.GAME_WORLD_MAP_PANE);
		setAnchorPane(footerAnchorPane, FactoryKeys.GAME_FOOTER_PANE);
	}
	
}
