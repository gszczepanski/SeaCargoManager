package szczepanski.gerard.cargo.simulator.ui.view.game;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import szczepanski.gerard.cargo.simulator.common.exception.CargoSimulatorRuntimeException;
import szczepanski.gerard.cargo.simulator.ui.view.common.FxmlComponentFactory;

@Component
public class GameSceneFactory extends FxmlComponentFactory<Scene> {
	
	private static final String PATH = "/templates/game/scene/GameScene.fxml";
	
	private final GameSceneController gameSceneController;
	
	@Autowired
	public GameSceneFactory(GameSceneController gameSceneController) {
		super(PATH);
		this.gameSceneController = gameSceneController;
	}

	@Override
	protected Scene generateFxmlComponent(FXMLLoader loader) {
		loader.setController(gameSceneController);
		Pane rootLayout;
		try {
			rootLayout = (Pane) loader.load();
			return new Scene(rootLayout);
		} catch (IOException e) {
			throw new CargoSimulatorRuntimeException(e);
		}
	}

}
