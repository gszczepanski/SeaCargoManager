package szczepanski.gerard.cargo.simulator.ui.view.simulator;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import szczepanski.gerard.cargo.simulator.common.exception.CargoSimulatorRuntimeException;
import szczepanski.gerard.cargo.simulator.ui.view.common.FxmlComponentFactory;

@Component
public class WelcomeSceneFactory extends FxmlComponentFactory<Scene> {
	
	private static final String PATH = "/templates/simulator/scene/WelcomeScene.fxml";
	
	private final WelcomeSceneController welcomeSceneController;
	
	@Autowired
	public WelcomeSceneFactory(WelcomeSceneController welcomeSceneController) {
		super(PATH);
		this.welcomeSceneController = welcomeSceneController;
	}

	@Override
	protected Scene generateFxmlComponent(FXMLLoader loader) {
		loader.setController(welcomeSceneController);
		Pane rootLayout;
		try {
			rootLayout = (Pane) loader.load();
			return new Scene(rootLayout);
		} catch (IOException e) {
			throw new CargoSimulatorRuntimeException(e);
		}
	}

}
