package szczepanski.gerard.cargo.simulator.ui.view.game.world.map;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.eventbus.Subscribe;

import javafx.application.Platform;
import lombok.Setter;
import szczepanski.gerard.cargo.simulator.common.eventbus.EventBusProvider;
import szczepanski.gerard.cargo.simulator.simulator.service.map.businesslogic.MapService;
import szczepanski.gerard.cargo.simulator.simulator.service.map.dto.MapPointDto;
import szczepanski.gerard.cargo.simulator.simulator.service.time.event.SimulatorDomainUpdatedEvent;
import szczepanski.gerard.cargo.simulator.ui.component.world.map.WorldMap;
import szczepanski.gerard.cargo.simulator.ui.component.world.map.WorldMapElementClickedEvent;
import szczepanski.gerard.cargo.simulator.ui.view.common.AbstractController;

@Component
class WorldMapPaneController extends AbstractController {
	private static final Logger LOG = Logger.getLogger(WorldMapPaneController.class);
	
	private final WorldMapEventResolver worldMapEventResolver;
	private final MapService mapService;
	
	@Autowired
	public WorldMapPaneController(WorldMapEventResolver worldMapEventResolver, MapService mapService) {
		this.worldMapEventResolver = worldMapEventResolver;
		this.mapService = mapService;
		EventBusProvider.get().register(this);
	}
	
	@Setter
	private WorldMap worldMap;

	@Subscribe
	private void updateOnSimulatorDomainUpdatedEvent(SimulatorDomainUpdatedEvent e) {
		reloadMap();
	}
	
	private void reloadMap() {
		Platform.runLater(() -> { 
			List<MapPointDto> mapPoints = mapService.getAllMapPoints();
			worldMap.updateMap(mapPoints);
		});
	}

	@Subscribe
	private void handleWorldMapElementClickedEvent(WorldMapElementClickedEvent e) {
		String clickedElementUuid = e.getClickedElementUuid();
		LOG.info("Clicked uuid on map: " + clickedElementUuid);
		worldMapEventResolver.showInfoPanel(clickedElementUuid);
	}
	
}
