package szczepanski.gerard.cargo.simulator.ui.view.common;

import org.springframework.stereotype.Component;

import javafx.scene.layout.Pane;

@Component
public class PaneFactoryProvider extends FactoryProvider<Pane> {

}
