package szczepanski.gerard.cargo.simulator.ui.component.world.map;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public final class WorldMapElementClickedEvent {
		
	private final String clickedElementUuid;
	
}
