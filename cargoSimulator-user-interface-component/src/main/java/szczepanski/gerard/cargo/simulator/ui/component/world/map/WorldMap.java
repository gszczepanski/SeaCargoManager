package szczepanski.gerard.cargo.simulator.ui.component.world.map;

import java.util.List;

import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import lombok.Getter;
import szczepanski.gerard.cargo.simulator.simulator.service.map.dto.MapPointDto;

public class WorldMap {

	private static final String WORLD_MAP_VIEW_PATH = WorldMap.class.getResource("/component/worldMap/worldMap.html").toString();

	@Getter
	private final WebView webView;
	private final WorldMapPointGenerator worldMapPointGenerator;
	final WebEngine webEngine;

	private WorldMap() {
		this.webView = new WebView();
		this.webEngine = webView.getEngine();
		this.webEngine.setJavaScriptEnabled(true);
		this.worldMapPointGenerator = new WorldMapPointGenerator();
	}

	public static WorldMap initialize() {
		WorldMap worldMap = new WorldMap();

		worldMap.webView.setMinWidth(1600);
		worldMap.webView.setMinHeight(850);
		worldMap.webView.setContextMenuEnabled(false);
		worldMap.webEngine.load(WORLD_MAP_VIEW_PATH);
		WorldMapJavaCaller.installCaller("javaCaller", worldMap);
		
		return worldMap;
	}

	public void updateMap(List<MapPointDto> mapPoints) {
		String mapPointsInJs = worldMapPointGenerator.convertToJsPointsString(mapPoints);
		String script = mapPointsInJs + "updateMarkers(markers);";
		webEngine.executeScript(script);
	}
	
}
