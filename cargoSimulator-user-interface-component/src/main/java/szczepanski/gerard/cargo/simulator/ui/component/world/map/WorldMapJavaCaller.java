package szczepanski.gerard.cargo.simulator.ui.component.world.map;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker.State;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import netscape.javascript.JSObject;
import szczepanski.gerard.cargo.simulator.common.eventbus.EventBusProvider;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class WorldMapJavaCaller {

	static void installCaller(String callerName, WorldMap source) {
		WorldMapJavaCaller caller = new WorldMapJavaCaller();

		source.webEngine.getLoadWorker().stateProperty().addListener(new ChangeListener<State>() {

			@Override
			public void changed(ObservableValue<? extends State> ov, State oldState, State newState) {
				if (newState == State.SUCCEEDED) {
					JSObject window = (JSObject) source.webEngine.executeScript("window");
					window.setMember(callerName, caller);
				}
			}
		});
		
		JSObject window = (JSObject) source.webEngine.executeScript("window");
        window.setMember(callerName, caller);
	}
	
	public void callFromMap(String uuid) {
		EventBusProvider.get().post(new WorldMapElementClickedEvent(uuid));
	}
	
}
