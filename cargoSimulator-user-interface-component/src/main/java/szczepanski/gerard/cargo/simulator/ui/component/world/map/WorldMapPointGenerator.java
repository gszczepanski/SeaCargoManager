package szczepanski.gerard.cargo.simulator.ui.component.world.map;

import java.util.List;

import szczepanski.gerard.cargo.simulator.simulator.service.map.dto.MapPointDto;

final class WorldMapPointGenerator {

	private static final String POINT_TEMPLATE = "{latLng: [%s, %s], uuid: '%s', name: '%s', style: {r: 5, fill:'%s'}}";

	public String generatePoint(MapPointDto dto) {
		return String.format(POINT_TEMPLATE, dto.getLatitude(), dto.getLongitude(), dto.getUuid(), dto.getName(), dto.getColor());
	}
	
	public String convertToJsPointsString(List<MapPointDto> dtos) {
		StringBuilder builder = new StringBuilder("var markers = [");
		
		dtos.forEach(d -> {
			builder.append(generatePoint(d));
			builder.append(",");
		});
		
		String generatedString = builder.toString();
		String updatedString = generatedString.substring(0, generatedString.length() - 1) + "];";
		return updatedString;
	}

}
