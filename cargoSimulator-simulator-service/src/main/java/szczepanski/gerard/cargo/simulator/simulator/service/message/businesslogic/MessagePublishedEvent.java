package szczepanski.gerard.cargo.simulator.simulator.service.message.businesslogic;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class MessagePublishedEvent {

	private static final MessagePublishedEvent INSTANCE = new MessagePublishedEvent();

	public static MessagePublishedEvent instance() {
		return INSTANCE;
	}

}
