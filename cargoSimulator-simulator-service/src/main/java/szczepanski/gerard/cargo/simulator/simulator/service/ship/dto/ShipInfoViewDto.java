package szczepanski.gerard.cargo.simulator.simulator.service.ship.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShipInfoViewDto {
	
	private String uuid;
	private String company;
	private String routeState;
	private String currentPosition;
	private String startPortName;
	private String endPortName;
	private String distanceTravelled;
	private String startRouteDate;
	
}
