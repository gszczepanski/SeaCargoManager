package szczepanski.gerard.cargo.simulator.simulator.service.time.businesslogic;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.eventbus.Subscribe;

import szczepanski.gerard.cargo.simulator.common.eventbus.EventBusProvider;
import szczepanski.gerard.cargo.simulator.simulator.service.common.businesslogic.UpdatableOnHourTickSimulatorDomainService;
import szczepanski.gerard.cargo.simulator.simulator.service.time.event.SimulatorDomainUpdatedEvent;
import szczepanski.gerard.cargo.simulator.simulator.service.time.event.SimulatorHourPassedTimeEvent;

@Component
class SimulatorHourPassedEventListener {
	
	private final List<UpdatableOnHourTickSimulatorDomainService> simulatorOnHourTickDomainUpdaters;
	
	@Autowired
	public SimulatorHourPassedEventListener(List<UpdatableOnHourTickSimulatorDomainService> simulatorOnHourTickDomainUpdaters) {
		this.simulatorOnHourTickDomainUpdaters = simulatorOnHourTickDomainUpdaters;
		EventBusProvider.get().register(this);
	}
	
	@Subscribe
	void triggerSimulatorActionsOnHourTick(SimulatorHourPassedTimeEvent e) {
		simulatorOnHourTickDomainUpdaters.forEach(UpdatableOnHourTickSimulatorDomainService::updateOnHourTick);
		EventBusProvider.get().post(SimulatorDomainUpdatedEvent.instance());
	}
	
}
