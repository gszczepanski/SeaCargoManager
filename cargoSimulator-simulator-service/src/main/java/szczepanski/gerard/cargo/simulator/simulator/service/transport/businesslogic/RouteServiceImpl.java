package szczepanski.gerard.cargo.simulator.simulator.service.transport.businesslogic;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.util.ParamAssertion;
import szczepanski.gerard.cargo.simulator.simulator.domain.port.Port;
import szczepanski.gerard.cargo.simulator.simulator.domain.port.PortRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.route.Route;
import szczepanski.gerard.cargo.simulator.simulator.domain.route.RouteFactory;
import szczepanski.gerard.cargo.simulator.simulator.domain.route.RouteRepository;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class RouteServiceImpl implements RouteService {
	
	private final RouteFactory routeFactory;
	private final RouteRepository routeRepository;
	private final PortRepository portRepository;
	
	@Override
	public String createRoute(LocalDateTime routeStartDate, List<String> routePortsUuids) {
		ParamAssertion.isNotNull(routeStartDate);
		ParamAssertion.isNotNull(routePortsUuids);
		
		List<Port> routePorts = portRepository.getByIds(routePortsUuids);
		Route route = routeFactory.generateRoute(routeStartDate, routePorts);
		routeRepository.save(route);
		return route.getUuid();
	}
	

}
