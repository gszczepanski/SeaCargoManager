package szczepanski.gerard.cargo.simulator.simulator.service.transport.businesslogic;

import java.time.LocalDateTime;
import java.util.List;

public interface RouteService {
	
	String createRoute(LocalDateTime routeStartDate, List<String> routePortsUuids);
	
}
