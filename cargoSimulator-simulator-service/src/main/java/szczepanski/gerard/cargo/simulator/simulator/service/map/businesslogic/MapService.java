package szczepanski.gerard.cargo.simulator.simulator.service.map.businesslogic;

import java.util.List;

import szczepanski.gerard.cargo.simulator.simulator.service.map.dto.MapPointDto;

public interface MapService {
	
	List<MapPointDto> getAllMapPoints();
	
}
