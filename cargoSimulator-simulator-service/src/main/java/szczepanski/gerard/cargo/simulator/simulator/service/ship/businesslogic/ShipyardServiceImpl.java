package szczepanski.gerard.cargo.simulator.simulator.service.ship.businesslogic;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.eventbus.Subscribe;

import szczepanski.gerard.cargo.simulator.common.eventbus.EventBusProvider;
import szczepanski.gerard.cargo.simulator.common.util.ParamAssertion;
import szczepanski.gerard.cargo.simulator.simulator.domain.company.SeaTransportCompany;
import szczepanski.gerard.cargo.simulator.simulator.domain.company.SeaTransportCompanyRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.port.Port;
import szczepanski.gerard.cargo.simulator.simulator.domain.port.PortRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.ship.Ship;
import szczepanski.gerard.cargo.simulator.simulator.domain.ship.ShipModel;
import szczepanski.gerard.cargo.simulator.simulator.domain.ship.ShipModelRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.ship.ShipRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.ship.ShipYard;
import szczepanski.gerard.cargo.simulator.simulator.domain.ship.ShipYardProvider;
import szczepanski.gerard.cargo.simulator.simulator.domain.ship.ShipYardShipCreatedEvent;

@Component
class ShipyardServiceImpl implements ShipyardService {
	
	private final SeaTransportCompanyRepository seaTransportCompanyRepository; 
	private final PortRepository portRepository;
	private final ShipYardProvider shipYardProvider;
	private final ShipModelRepository shipModelRepository;
	private final ShipRepository shipRepository;
	
	@Autowired
	public ShipyardServiceImpl(SeaTransportCompanyRepository seaTransportCompanyRepository, PortRepository portRepository, ShipYardProvider shipYardProvider,
			ShipModelRepository shipModelRepository, ShipRepository shipRepository) {
		this.seaTransportCompanyRepository = seaTransportCompanyRepository;
		this.portRepository = portRepository;
		this.shipYardProvider = shipYardProvider;
		this.shipModelRepository = shipModelRepository;
		this.shipRepository = shipRepository;
		
		EventBusProvider.get().register(this);
	}

	@Override
	public LocalDateTime buyShip(String shipModelUuid, String companyUuid, String dockPortUuid) {
		ParamAssertion.isNotBlank(shipModelUuid, companyUuid, dockPortUuid);
		
		ShipModel shipModel = shipModelRepository.findOne(shipModelUuid);
		SeaTransportCompany company = seaTransportCompanyRepository.findOne(companyUuid);
		Port dockPort = portRepository.findOne(dockPortUuid);
		
		ShipYard shipyard = shipYardProvider.getShipYard();
		return shipyard.buyShip(shipModel, company, dockPort);
	}
	
	@Subscribe
	private void registerCreatedShip(ShipYardShipCreatedEvent e) {
		Ship createdShip = e.getCreatedShip();
		shipRepository.save(createdShip);
	}

	@Override
	public void updateOnHourTick() {
		ShipYard shipyard = shipYardProvider.getShipYard();
		shipyard.processBuilding();
	}
}
