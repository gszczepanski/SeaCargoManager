package szczepanski.gerard.cargo.simulator.simulator.service.common.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public final class SearchPageDto {
	
	private final int numberOfPage;
	private final int elementsOnPage;
	
}
