package szczepanski.gerard.cargo.simulator.simulator.service.time.event;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Designed as singleton. This event notifies UI layer, that Simulator domain was updated on hour passed. 
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class SimulatorHourPassedTimeEvent {
	
	private static final SimulatorHourPassedTimeEvent INSTANCE = new SimulatorHourPassedTimeEvent();
	
	public static SimulatorHourPassedTimeEvent instance() {
		return INSTANCE;
	}
	
}
