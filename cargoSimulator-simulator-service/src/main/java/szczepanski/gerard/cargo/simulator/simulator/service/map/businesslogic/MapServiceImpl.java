package szczepanski.gerard.cargo.simulator.simulator.service.map.businesslogic;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.collections.CollectionFactory;
import szczepanski.gerard.cargo.simulator.common.config.DevelopmentVariables;
import szczepanski.gerard.cargo.simulator.simulator.domain.location.LocationPoint;
import szczepanski.gerard.cargo.simulator.simulator.domain.port.Port;
import szczepanski.gerard.cargo.simulator.simulator.domain.port.PortRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.route.RouteFactory;
import szczepanski.gerard.cargo.simulator.simulator.domain.ship.Ship;
import szczepanski.gerard.cargo.simulator.simulator.domain.ship.ShipRepository;
import szczepanski.gerard.cargo.simulator.simulator.service.map.dto.MapPointDto;
import szczepanski.gerard.cargo.simulator.simulator.service.port.businesslogic.PortAssembler;
import szczepanski.gerard.cargo.simulator.simulator.service.ship.businesslogic.ShipAssembler;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class MapServiceImpl implements MapService {

	private final PortRepository portRepository;
	private final ShipRepository shipRepository;
	private final RouteFactory routeFactory;
	
	private List<MapPointDto> staticPoints = CollectionFactory.list();
	private List<MapPointDto> staticDevPoints = CollectionFactory.list();

	@Override
	public List<MapPointDto> getAllMapPoints() {
		if (DevelopmentVariables.SHOW_ROUTES_GRAPH_FOR_DEV) {
			return getMapPointsForDev();
		}
		
		return getMapPoints();
	}

	private List<MapPointDto> getMapPoints() {
		if (staticPoints.isEmpty()) {
			staticPoints.addAll(getAllPorts());
		}
		
		List<MapPointDto> points = getDynamicPoints();
		points.addAll(staticPoints);
		return points;
	}
	
	private List<MapPointDto> getDynamicPoints() {
		List<MapPointDto> points = CollectionFactory.list();
		points.addAll(getAllSailingShips());
		return points;
	}
	
	private List<MapPointDto> getMapPointsForDev() {
		if (staticDevPoints.isEmpty()) {
			Set<LocationPoint> locationPoints = routeFactory.getRoutesGraph().getAllLocationPoints();
			List<MapPointDto> mapPoints = LocationPointAssembler.createMapPointDtos(locationPoints);
			staticDevPoints.addAll(mapPoints);
		}
		
		return staticDevPoints;
	}

	private List<MapPointDto> getAllPorts() {
		Collection<Port> ports = portRepository.getAll();
		return PortAssembler.toMapPointsDto(ports);
	}

	private List<MapPointDto> getAllSailingShips() {
		Collection<Ship> ships = shipRepository.findAllSaillingShips();
		return ShipAssembler.toMapPointsDto(ships);
	}

}
