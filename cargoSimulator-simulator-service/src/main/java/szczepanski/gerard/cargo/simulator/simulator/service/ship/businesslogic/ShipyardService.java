package szczepanski.gerard.cargo.simulator.simulator.service.ship.businesslogic;

import java.time.LocalDateTime;

import szczepanski.gerard.cargo.simulator.simulator.service.common.businesslogic.UpdatableOnHourTickSimulatorDomainService;

public interface ShipyardService extends UpdatableOnHourTickSimulatorDomainService {
	
	LocalDateTime buyShip(String shipModelUuid, String companyUuid, String dockPortUuid);
	
}
