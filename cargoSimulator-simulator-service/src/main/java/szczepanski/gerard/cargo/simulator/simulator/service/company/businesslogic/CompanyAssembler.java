package szczepanski.gerard.cargo.simulator.simulator.service.company.businesslogic;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.util.StringUtils;
import szczepanski.gerard.cargo.simulator.simulator.domain.company.Company;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.Money;
import szczepanski.gerard.cargo.simulator.simulator.service.company.dto.CompanyInfoDto;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
class CompanyAssembler {

	static CompanyInfoDto toCompanyInfoDto(Company company) {
		CompanyInfoDto companyInfoDto = new CompanyInfoDto();

		Money balance = company.getCompanyFinancesInfo().getCurrentBalance();
		companyInfoDto.setCompanyBalance(StringUtils.formatNumeric(balance.getValue().doubleValue()) + StringUtils.SPACE + balance.getCurrency().getCode());
		
		companyInfoDto.setReputation(String.valueOf(company.getCompanyInfo().getReputationPoints()));
		
		return companyInfoDto;
	}

}
