package szczepanski.gerard.cargo.simulator.simulator.service.message.businesslogic;

import java.util.List;

import szczepanski.gerard.cargo.simulator.simulator.domain.message.Message;
import szczepanski.gerard.cargo.simulator.simulator.service.common.dto.PageResultDto;
import szczepanski.gerard.cargo.simulator.simulator.service.common.dto.SearchPageDto;

public interface MessageService {
	
	PageResultDto<Message> find(SearchPageDto searchPageDto);
	
	List<Message> getUnreadMessagesFromPastLatestHours(int latestHoursNumber);
	
	int getNumberOfUnreadedMessages();
}
