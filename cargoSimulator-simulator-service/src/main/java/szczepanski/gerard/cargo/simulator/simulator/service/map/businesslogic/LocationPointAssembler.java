package szczepanski.gerard.cargo.simulator.simulator.service.map.businesslogic;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.config.DevelopmentVariables;
import szczepanski.gerard.cargo.simulator.simulator.domain.location.LocationPoint;
import szczepanski.gerard.cargo.simulator.simulator.service.map.dto.MapPointDto;
import szczepanski.gerard.cargo.simulator.simulator.service.map.dto.MapPointDto.MapPointType;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
class LocationPointAssembler {
	
	static MapPointDto createMapPointDto(LocationPoint locationPoint) {
		MapPointDto dto = new MapPointDto();

		dto.setMapPointType(MapPointType.LOCATION_POINT);
		dto.setUuid("");
		dto.setName("Lat: " + locationPoint.getLatitude() + " Lon: " + locationPoint.getLongitude());
		dto.setLatitude(Double.toString(locationPoint.getLatitude()));
		dto.setLongitude(Double.toString(locationPoint.getLongitude()));
		dto.setColor(DevelopmentVariables.LOCATION_POINT_MAP_COLOR);

		return dto;
	}
	
	static List<MapPointDto> createMapPointDtos(Set<LocationPoint> locationPoints) {
		return locationPoints.stream().map(LocationPointAssembler::createMapPointDto).collect(Collectors.toList());
	}
	
}
