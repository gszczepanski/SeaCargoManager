package szczepanski.gerard.cargo.simulator.simulator.service.common.dto;

import java.util.List;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public final class PageResultDto<T> {

	private final List<T> results;
	private final int pageNumber;
	private final int pageSize;
	private final int totalPages;

	public static <T> PageResultDtoBuilder<T> builder() {
		return new PageResultDtoBuilder<T>();
	}

	@NoArgsConstructor(access = AccessLevel.PRIVATE)
	public static final class PageResultDtoBuilder<T> {

		private List<T> results;
		private int pageNumber;
		private int pageSize;

		public final PageResultDtoBuilder<T> results(List<T> results) {
			this.results = results;
			return this;
		}

		public final PageResultDtoBuilder<T> pageNumber(int pageNumber) {
			this.pageNumber = pageNumber;
			return this;
		}

		public final PageResultDtoBuilder<T> pageSize(int pageSize) {
			this.pageSize = pageSize;
			return this;
		}

		public PageResultDto<T> buildPage() {
			int totalPages = (results.size() / pageNumber) + 1;
			int startPageElementIndex;
			int endPageElementIndex;
			
			if (pageNumber == 1) {
				startPageElementIndex = 0;
				endPageElementIndex = pageSize;
			} else {
				startPageElementIndex = (pageSize - 1) * pageNumber;
				endPageElementIndex = pageSize * pageNumber;
			}
			
			List<T> pageResults = results.subList(startPageElementIndex, endPageElementIndex);
			return new PageResultDto<>(pageResults, pageNumber, pageSize, totalPages);
		}

	}

}
