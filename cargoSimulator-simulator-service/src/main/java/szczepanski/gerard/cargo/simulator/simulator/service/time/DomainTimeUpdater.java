package szczepanski.gerard.cargo.simulator.simulator.service.time;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.time.TimeProxy;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DomainTimeUpdater {
	
	private final TimeProxy timeProxy;
	
	public final void updateDomainTime(LocalDateTime time) {
		timeProxy.updateTime(time);
	}
	
}
