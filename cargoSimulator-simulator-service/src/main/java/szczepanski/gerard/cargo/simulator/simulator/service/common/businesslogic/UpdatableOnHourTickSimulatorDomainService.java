package szczepanski.gerard.cargo.simulator.simulator.service.common.businesslogic;

@FunctionalInterface
public interface UpdatableOnHourTickSimulatorDomainService {
	
	void updateOnHourTick();
	
}
