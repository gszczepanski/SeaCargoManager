package szczepanski.gerard.cargo.simulator.simulator.service.port.businesslogic;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.config.GlobalVariables;
import szczepanski.gerard.cargo.simulator.simulator.domain.port.Port;
import szczepanski.gerard.cargo.simulator.simulator.service.map.dto.MapPointDto;
import szczepanski.gerard.cargo.simulator.simulator.service.map.dto.MapPointDto.MapPointType;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PortAssembler {
		
	public static MapPointDto toMapPointDto(Port port) {
		MapPointDto dto = new MapPointDto();
		
		dto.setMapPointType(MapPointType.PORT);
		dto.setUuid(port.getUuid());
		dto.setName(port.getName());
		dto.setLatitude(Double.toString(port.getCity().getLocationPoint().getLatitude()));
		dto.setLongitude(Double.toString(port.getCity().getLocationPoint().getLongitude()));
		dto.setColor(GlobalVariables.MAP_PORT_COLOR);
		
		return dto;
	}
	
	public static List<MapPointDto> toMapPointsDto(Collection<Port> ports) {
		return ports.stream().map(PortAssembler::toMapPointDto).collect(Collectors.toList());
	}
	
}
