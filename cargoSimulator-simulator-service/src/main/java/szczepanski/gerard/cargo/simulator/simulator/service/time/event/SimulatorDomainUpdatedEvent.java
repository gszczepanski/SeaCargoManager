package szczepanski.gerard.cargo.simulator.simulator.service.time.event;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Designed as singleton. This event notifies subscribers, that Simulator hour passed. 
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class SimulatorDomainUpdatedEvent {
	
	private static final SimulatorDomainUpdatedEvent INSTANCE = new SimulatorDomainUpdatedEvent();
	
	public static SimulatorDomainUpdatedEvent instance() {
		return INSTANCE;
	}
	
}
