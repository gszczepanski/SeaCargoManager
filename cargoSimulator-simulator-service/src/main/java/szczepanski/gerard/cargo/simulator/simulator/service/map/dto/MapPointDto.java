package szczepanski.gerard.cargo.simulator.simulator.service.map.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MapPointDto {
	
	private MapPointType mapPointType;
	
	private String uuid;
	private String name;
	private String color;
	
	private String latitude;
	private String longitude;
	
	public static enum MapPointType {
		
		PORT,
		SHIP,
		LOCATION_POINT,
		CITY;
		
	}
	
}
