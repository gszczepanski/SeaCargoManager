package szczepanski.gerard.cargo.simulator.simulator.service.company.businesslogic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.util.ParamAssertion;
import szczepanski.gerard.cargo.simulator.simulator.domain.company.Company;
import szczepanski.gerard.cargo.simulator.simulator.domain.company.SeaTransportCompanyRepository;
import szczepanski.gerard.cargo.simulator.simulator.service.company.dto.CompanyInfoDto;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class SeaTransportCompanyServiceImpl implements SeaTransportCompanyService {
	
	private final SeaTransportCompanyRepository seaTransportCompanyRepository;

	@Override
	public CompanyInfoDto fetchCompanyInfo(String companyUuid) {
		ParamAssertion.isNotBlank(companyUuid);
		Company company = seaTransportCompanyRepository.findOne(companyUuid);
		
		return CompanyAssembler.toCompanyInfoDto(company);
	}
}
