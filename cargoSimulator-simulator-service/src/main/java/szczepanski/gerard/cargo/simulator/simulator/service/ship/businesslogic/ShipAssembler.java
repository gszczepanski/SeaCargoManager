package szczepanski.gerard.cargo.simulator.simulator.service.ship.businesslogic;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.util.DateTimeUtils;
import szczepanski.gerard.cargo.simulator.simulator.domain.location.LocationPoint;
import szczepanski.gerard.cargo.simulator.simulator.domain.route.RouteInformationObject;
import szczepanski.gerard.cargo.simulator.simulator.domain.ship.Ship;
import szczepanski.gerard.cargo.simulator.simulator.domain.ship.ShipInformationObject;
import szczepanski.gerard.cargo.simulator.simulator.service.map.dto.MapPointDto;
import szczepanski.gerard.cargo.simulator.simulator.service.map.dto.MapPointDto.MapPointType;
import szczepanski.gerard.cargo.simulator.simulator.service.ship.dto.ShipInfoViewDto;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ShipAssembler {

	public static MapPointDto toMapPointDto(Ship ship) {
		MapPointDto dto = new MapPointDto();

		LocationPoint currentPoint = ship.currentPosition();
		
		dto.setMapPointType(MapPointType.SHIP);
		dto.setUuid(ship.getUuid());
		dto.setName(prepareMapName(ship));
		dto.setLatitude(Double.toString(currentPoint.getLatitude()));
		dto.setLongitude(Double.toString(currentPoint.getLongitude()));
		dto.setColor(ship.getOwnerCompany().getCompanyInfo().getCompanyColor());

		return dto;
	}
	
	private static String prepareMapName(Ship ship) {
		return "<b>Id:</b> " + ship.getUuid() + "<br/><b>Model:</b> " + ship.getShipModel().getName() + "<br/><b>Owner:</b> " + ship.getOwnerCompany().getCompanyInfo().getName();
	}

	public static List<MapPointDto> toMapPointsDto(Collection<Ship> ships) {
		return ships.stream().map(ShipAssembler::toMapPointDto).collect(Collectors.toList());
	}

	public static ShipInfoViewDto toShipInfoViewDto(Ship ship) {
		ShipInformationObject shipInformationObject = ship.informationObject();
		ShipInfoViewDto dto = new ShipInfoViewDto();

		dto.setUuid(shipInformationObject.getUuid());
		dto.setCompany("Latimer Co.");

		Optional<RouteInformationObject> optionalRouteInformationObject = shipInformationObject.getRouteInfoObj();

		if (optionalRouteInformationObject.isPresent()) {
			RouteInformationObject routeInformationObject = optionalRouteInformationObject.get();

			dto.setRouteState(StringUtils.capitalize(routeInformationObject.getRouteState()));
			dto.setStartPortName(routeInformationObject.getStartPortName());
			dto.setEndPortName(routeInformationObject.getEndPortName());
			
			String distanceTravelledString = (routeInformationObject.getTravelledDistance() / 1000) + " / " + (routeInformationObject.getTotalDistance() / 1000) + " km";
			dto.setDistanceTravelled(distanceTravelledString);
			String currentPosition = "(" + String.valueOf(routeInformationObject.getCurrentLocation().getLatitude()) + ", "
					+ String.valueOf(routeInformationObject.getCurrentLocation().getLongitude()) + ")";
			dto.setCurrentPosition(currentPosition);
			dto.setStartRouteDate(DateTimeUtils.getDateTime(routeInformationObject.getStartDate()));
		}

		return dto;
	}

}
