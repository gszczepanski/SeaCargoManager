package szczepanski.gerard.cargo.simulator.simulator.service.company.businesslogic;

import szczepanski.gerard.cargo.simulator.simulator.service.company.dto.CompanyInfoDto;

public interface SeaTransportCompanyService {
	
	CompanyInfoDto fetchCompanyInfo(String companyUuid);
	
}
