package szczepanski.gerard.cargo.simulator.simulator.service.company.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CompanyInfoDto {
	
	private String companyBalance;
	private String reputation;
	
}
