package szczepanski.gerard.cargo.simulator.simulator.service.message.businesslogic;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.eventbus.Subscribe;

import szczepanski.gerard.cargo.simulator.common.eventbus.EventBusProvider;
import szczepanski.gerard.cargo.simulator.common.util.ParamAssertion;
import szczepanski.gerard.cargo.simulator.simulator.domain.message.Message;
import szczepanski.gerard.cargo.simulator.simulator.domain.message.MessageEvent;
import szczepanski.gerard.cargo.simulator.simulator.domain.message.MessageRepository;
import szczepanski.gerard.cargo.simulator.simulator.service.common.dto.PageResultDto;
import szczepanski.gerard.cargo.simulator.simulator.service.common.dto.PageResultDto.PageResultDtoBuilder;
import szczepanski.gerard.cargo.simulator.simulator.service.common.dto.SearchPageDto;

@Component
class MessageServiceImpl implements MessageService {
	
	private final MessageRepository messageRepository;
	
	@Autowired
	public MessageServiceImpl(MessageRepository messageRepository) {
		this.messageRepository = messageRepository;
		EventBusProvider.get().register(this);
	}

	@Override
	public PageResultDto<Message> find(SearchPageDto searchPageDto) {
		ParamAssertion.isNotNull(searchPageDto);
		List<Message> messages = messageRepository.getAll((m1, m2) -> m2.getTime().compareTo(m1.getTime()));
		
		PageResultDtoBuilder<Message> builder = PageResultDto.builder();
		return builder
				.pageNumber(searchPageDto.getNumberOfPage())
				.pageSize(searchPageDto.getElementsOnPage())
				.results(messages)
				.buildPage();
	}
	
	@Override
	public List<Message> getUnreadMessagesFromPastLatestHours(int latestHoursNumber) {
		return messageRepository.getUnreadMessagesFromLatestHours(latestHoursNumber);
	}
	
	@Override
	public int getNumberOfUnreadedMessages() {
		return messageRepository.getNumberOfUnreadedMessages();
	}
	
	@Subscribe
	private void handleMessageEvent(MessageEvent e) {
		Message message = e.getMessage();
		ParamAssertion.isNotNull(e);
		messageRepository.save(message);
		EventBusProvider.get().post(MessagePublishedEvent.instance());
	}


	
}
