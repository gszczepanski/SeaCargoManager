package szczepanski.gerard.cargo.simulator.simulator.service.ship.businesslogic;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.util.ParamAssertion;
import szczepanski.gerard.cargo.simulator.simulator.domain.route.Route;
import szczepanski.gerard.cargo.simulator.simulator.domain.route.RouteRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.ship.Ship;
import szczepanski.gerard.cargo.simulator.simulator.domain.ship.ShipRepository;
import szczepanski.gerard.cargo.simulator.simulator.service.ship.dto.ShipInfoViewDto;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class ShipServiceImpl implements ShipService {

	private final ShipRepository shipRepository;
	private final RouteRepository routeRepository;

	@Override
	public void updateOnHourTick() {
		List<Ship> shipsOnSea = shipRepository.findAllSaillingShips();
		shipsOnSea.forEach(Ship::sail);
	}

	@Override
	public void setRouteToShip(String shipUuid, String routeUuid) {
		ParamAssertion.isNotNull(shipUuid, routeUuid);
		Route route = routeRepository.findOne(routeUuid);
		Ship ship = shipRepository.findOne(shipUuid);
		ship.setRoute(route);
	}

	@Override
	public ShipInfoViewDto getShipInfoDto(String uuid) {
		ParamAssertion.isNotNull(uuid);
		Ship ship = shipRepository.findOne(uuid);
		return ShipAssembler.toShipInfoViewDto(ship);
	}

}
