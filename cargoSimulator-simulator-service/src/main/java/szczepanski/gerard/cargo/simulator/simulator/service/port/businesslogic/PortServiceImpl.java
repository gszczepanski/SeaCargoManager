package szczepanski.gerard.cargo.simulator.simulator.service.port.businesslogic;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.simulator.domain.port.Port;
import szczepanski.gerard.cargo.simulator.simulator.domain.port.PortRepository;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class PortServiceImpl implements PortService {
	
	private final PortRepository portProvider;
	
	@Override
	public void updateOnHourTick() {
		Collection<Port> ports = portProvider.getAll();
		ports.forEach(Port::releaseReadyShips);
	}

}
