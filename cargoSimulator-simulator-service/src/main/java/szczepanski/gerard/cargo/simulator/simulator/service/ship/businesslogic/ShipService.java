package szczepanski.gerard.cargo.simulator.simulator.service.ship.businesslogic;

import szczepanski.gerard.cargo.simulator.simulator.service.common.businesslogic.UpdatableOnHourTickSimulatorDomainService;
import szczepanski.gerard.cargo.simulator.simulator.service.ship.dto.ShipInfoViewDto;

public interface ShipService extends UpdatableOnHourTickSimulatorDomainService {
	
	void setRouteToShip(String shipUuid, String routeUuid);
	
	ShipInfoViewDto getShipInfoDto(String uuid);
	
}
