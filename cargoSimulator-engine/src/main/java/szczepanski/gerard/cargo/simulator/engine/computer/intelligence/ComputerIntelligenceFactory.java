package szczepanski.gerard.cargo.simulator.engine.computer.intelligence;

import org.springframework.stereotype.Component;

import szczepanski.gerard.cargo.simulator.engine.player.Player;
import szczepanski.gerard.cargo.simulator.simulator.domain.company.CompanyType;

@Component
public class ComputerIntelligenceFactory {
	
	public ComputerIntelligence createForPlayer(Player player) {
		if (player.getCompany().getCompanyInfo().getCompanyType() == CompanyType.SEA_TRANSPORT) {
			return new DummySeaTransportCompanyIntelligence(player);
		}
		
		// TODO other companies
		return null;
	}
	
}
