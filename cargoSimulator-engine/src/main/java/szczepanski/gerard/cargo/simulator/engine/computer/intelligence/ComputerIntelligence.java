package szczepanski.gerard.cargo.simulator.engine.computer.intelligence;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.context.ApplicationContextProvider;
import szczepanski.gerard.cargo.simulator.engine.player.Player;

@RequiredArgsConstructor
public abstract class ComputerIntelligence {
	
	@Getter
	private final Player player;
	
	public abstract void computeLogic();
	
	public <T> T getBean(Class<T> requiredType) {
		return ApplicationContextProvider.getBean(requiredType);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((player == null) ? 0 : player.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComputerIntelligence other = (ComputerIntelligence) obj;
		if (player == null) {
			if (other.player != null)
				return false;
		} else if (!player.equals(other.player))
			return false;
		return true;
	}
	
	
	
}
