package szczepanski.gerard.cargo.simulator.engine.engine;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.eventbus.Subscribe;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.eventbus.EventBusProvider;
import szczepanski.gerard.cargo.simulator.engine.computer.intelligence.ComputerIntelligence;
import szczepanski.gerard.cargo.simulator.engine.computer.intelligence.ComputerIntelligencePool;
import szczepanski.gerard.cargo.simulator.engine.time.HourPassedTimeEvent;
import szczepanski.gerard.cargo.simulator.simulator.service.time.event.SimulatorHourPassedTimeEvent;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class GameEngineImpl implements GameEngine {
	
	private final ComputerIntelligencePool gameIntelligencePool;

	@Override
	public void startNewGame() {
		EventBusProvider.get().register(this);
	}
	
	@Subscribe
	private void invokeActionsOnGameHourTick(HourPassedTimeEvent e) {
		EventBusProvider.get().post(SimulatorHourPassedTimeEvent.instance());
		updateAI();
	}
	
	private void updateAI() {
		Thread aiUpdateThread = new Thread(() -> {
			gameIntelligencePool.getComputerIntelligenceSet().forEach(ComputerIntelligence::computeLogic);
		});
		aiUpdateThread.start();
	}

}
