package szczepanski.gerard.cargo.simulator.engine.time;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Designed as singleton. This event notifies subscribers, that Simulator hour passed. 
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class HourPassedTimeEvent {
	
	private static final HourPassedTimeEvent INSTANCE = new HourPassedTimeEvent();
	
	public static HourPassedTimeEvent instance() {
		return INSTANCE;
	}
	
}
