package szczepanski.gerard.cargo.simulator.engine.computer.intelligence;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import szczepanski.gerard.cargo.simulator.common.util.RandomUtils;
import szczepanski.gerard.cargo.simulator.engine.player.Player;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.time.SimulatorTime;
import szczepanski.gerard.cargo.simulator.simulator.domain.company.SeaTransportCompany;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.CanNotCreateMoneyTransactionException;
import szczepanski.gerard.cargo.simulator.simulator.domain.port.Port;
import szczepanski.gerard.cargo.simulator.simulator.domain.port.PortRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.ship.Ship;
import szczepanski.gerard.cargo.simulator.simulator.domain.ship.ShipState;
import szczepanski.gerard.cargo.simulator.simulator.service.ship.businesslogic.ShipService;
import szczepanski.gerard.cargo.simulator.simulator.service.ship.businesslogic.ShipyardService;
import szczepanski.gerard.cargo.simulator.simulator.service.transport.businesslogic.RouteService;

class DummySeaTransportCompanyIntelligence extends ComputerIntelligence {
	private static final Logger LOG = Logger.getLogger(ComputerIntelligence.class);

	private final SeaTransportCompany seaTransportCompany;

	public DummySeaTransportCompanyIntelligence(Player player) {
		super(player);
		this.seaTransportCompany = (SeaTransportCompany) player.getCompany();
	}

	@Override
	public void computeLogic() {
		testLogic();
	}

	private void testLogic() {
		int min = 0;
		int max = 61;

		if (RandomUtils.randomFromRange(min, max) == 9) {
			buyShip();
		}
		
		if (RandomUtils.randomFromRange(min, max) > 58) {
			assignRouteToShip();
		}
	}

	private void buyShip() {
		String shipModelUuid = "shipModel01";
		
		ShipyardService shipYardService = getBean(ShipyardService.class);
		PortRepository portRepository = getBean(PortRepository.class);

		String startPortUuid = portRepository.getPortForCity(seaTransportCompany.getCompanyInfo().getCity().getUuid()).get().getUuid();
		try {
			shipYardService.buyShip(shipModelUuid, seaTransportCompany.getUuid(), startPortUuid);
		} catch(CanNotCreateMoneyTransactionException e) {
			LOG.info("Can not buy ship :(");
		}
		
	}
	
	private void assignRouteToShip() {
		PortRepository portRepository = getBean(PortRepository.class);
		RouteService routeService = getBean(RouteService.class);
		ShipService shipService = getBean(ShipService.class);
		
		Optional<Ship> optFreeShip = seaTransportCompany.getSeaTransportCompanyOwnership().getShips().stream().filter(s -> s.getState() == ShipState.DOCKED).findFirst();
		
		if (optFreeShip.isPresent()) {
			Ship ship = optFreeShip.get();
			String shipUuid = ship.getUuid();
			String startPortUuid = ship.getDockedToPort().getUuid();
			
			List<Port> potentialDestinationPorts = portRepository.getAll().stream().filter(p -> !p.getUuid().equals(startPortUuid)).collect(Collectors.toList());
			int destinationPortRandomIndex = RandomUtils.randomFromRange(0, potentialDestinationPorts.size() - 1);
			String destinationPortUuid = potentialDestinationPorts.get(destinationPortRandomIndex).getUuid(); 
			
			String routeUuid = routeService.createRoute(SimulatorTime.getTime().plusDays(1), Arrays.asList(startPortUuid, destinationPortUuid));
			shipService.setRouteToShip(shipUuid, routeUuid);
		}
	}

}
