package szczepanski.gerard.cargo.simulator.engine.player;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.simulator.domain.company.BasicCompanyInfo;
import szczepanski.gerard.cargo.simulator.simulator.domain.company.SeaTransportCompany;
import szczepanski.gerard.cargo.simulator.simulator.domain.company.SeaTransportCompanyRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.person.Person;
import szczepanski.gerard.cargo.simulator.simulator.domain.person.PersonRepository;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PlayerFactory {
	
	private final PersonRepository personRepository; 
	private final SeaTransportCompanyRepository seaTransportCompanyRepository;
	
	public Player createPlayer(String playerPersonUuid, String companyUuid, PlayerType playerType) {
		Person person = personRepository.findOne(playerPersonUuid);
		SeaTransportCompany company = seaTransportCompanyRepository.findOne(companyUuid);
		
		BasicCompanyInfo companyInfo = company.getCompanyInfo();
		companyInfo.setCeo(person);
		
		return Player.builder()
			.playerType(playerType)		
			.person(person)
			.company(company)
			.build();
	}
	
	
}
