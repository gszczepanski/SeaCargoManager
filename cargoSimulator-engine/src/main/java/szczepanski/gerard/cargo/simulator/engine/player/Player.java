package szczepanski.gerard.cargo.simulator.engine.player;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import szczepanski.gerard.cargo.simulator.simulator.domain.company.Company;
import szczepanski.gerard.cargo.simulator.simulator.domain.person.Person;

@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class Player {
	
	private final PlayerType playerType;
	private final Person person;
	private final Company company;
	
}
