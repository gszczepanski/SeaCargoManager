package szczepanski.gerard.cargo.simulator.engine.time;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Designed as singleton. This event notifies subscribers, that Simulator minute passed. 
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class MinutePassedTimeEvent {
	
	private static final MinutePassedTimeEvent INSTANCE = new MinutePassedTimeEvent();
	
	public static MinutePassedTimeEvent instance() {
		return INSTANCE;
	}
	
}
