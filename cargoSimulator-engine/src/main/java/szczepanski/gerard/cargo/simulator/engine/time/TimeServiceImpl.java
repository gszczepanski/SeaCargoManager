package szczepanski.gerard.cargo.simulator.engine.time;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.eventbus.Subscribe;

import szczepanski.gerard.cargo.simulator.common.config.GlobalVariables;
import szczepanski.gerard.cargo.simulator.common.eventbus.EventBusProvider;
import szczepanski.gerard.cargo.simulator.simulator.domain.common.time.TimeProxy;

@Component
class TimeServiceImpl implements TimeService {

	private LocalDateTime gameTime = GlobalVariables.DEFAULT_GAME_DATE_START;
	private final Clock clock;
	private final TimeProxy timeProxy;
	private ClockState previousClockState;
	
	@Autowired
	public TimeServiceImpl(TimeProxy timeProxy) {
		this.timeProxy = timeProxy;
		this.clock = new Clock(ClockState.PAUSED, 0);
		
		EventBusProvider.get().register(this);
		
		timeProxy.updateTime(gameTime);
	}
	
	@Override
	public void pause() {
		previousClockState = clock.getClockState();
		clock.setClockState(ClockState.PAUSED);
	}

	@Override
	public void start() {
		previousClockState = clock.getClockState();
		clock.setClockState(ClockState.RUNNING);
	}

	@Override
	public void speedUp() {
		previousClockState = clock.getClockState();
		clock.setClockState(ClockState.SPEEDED_UP);
	}

	@Override
	public void restorePreviousClockState() {
		clock.setClockState(previousClockState);
	}

	@Override
	public LocalDateTime getGameTime() {
		return gameTime;
	}

	@Override
	public void hourTick() {
		previousClockState = clock.getClockState();
		clock.setClockState(ClockState.HOUR_TICK);
	}

	@Override
	public void skipDateTo(LocalDateTime newGameTime) {
		long hoursPassed = gameTime.until(newGameTime, ChronoUnit.HOURS);
		ClockState clockStateBeforeSkip = clock.getClockState();
		
		clock.setClockState(ClockState.HOUR_TICK);
		for (long i = 0; i <= hoursPassed; i++) {
			EventBusProvider.get().post(HourPassedTimeEvent.instance());
			gameTime = gameTime.plusHours(1);
			timeProxy.updateTime(gameTime);
		}
		
		clock.setClockState(clockStateBeforeSkip);
	}
	
	@Subscribe
	private void handleMinutePassed(MinutePassedTimeEvent e) {
		gameTime = clock.getClockState() == ClockState.HOUR_TICK ? gameTime.plusHours(1) : gameTime.plusMinutes(1);
		timeProxy.updateTime(gameTime);
	}
}
