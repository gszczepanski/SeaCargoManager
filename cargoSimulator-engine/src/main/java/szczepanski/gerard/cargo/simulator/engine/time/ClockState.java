package szczepanski.gerard.cargo.simulator.engine.time;

public enum ClockState {
	
	PAUSED, RUNNING, SPEEDED_UP, HOUR_TICK;
	
}
