package szczepanski.gerard.cargo.simulator.engine.time;

import java.time.LocalDateTime;

public interface TimeService {
	
	void pause();
	
	void start();
	
	void speedUp();
	
	void hourTick();
	
	void restorePreviousClockState();
	
	LocalDateTime getGameTime();
	
	void skipDateTo(LocalDateTime newGameTime);
	
}
