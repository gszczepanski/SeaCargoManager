package szczepanski.gerard.cargo.simulator.engine.time;

import org.apache.log4j.Logger;

import lombok.Getter;
import szczepanski.gerard.cargo.simulator.common.config.GlobalVariables;
import szczepanski.gerard.cargo.simulator.common.eventbus.EventBusProvider;
import szczepanski.gerard.cargo.simulator.common.exception.CargoSimulatorRuntimeException;

class Clock implements Runnable {
	private static final Logger LOG = Logger.getLogger(Clock.class);
	private static final int MINUTES_IN_HOUR = 60;
	
	private Thread clockThread;
	private long minutesPassedFromLastHour;

	@Getter
	private ClockState clockState;

	Clock(ClockState clockState, long minutesPassedFromLastHour) {
		this.clockState = clockState;
		this.minutesPassedFromLastHour = minutesPassedFromLastHour;
	}

	@Override
	public void run() {
		long sleepTime = 0;

		while (clockState != ClockState.PAUSED) {
			sleepTime = getSleepTime();
			sleep(sleepTime);
			tick();
		}
	}

	private long getSleepTime() {
		if (clockState == ClockState.SPEEDED_UP) {
			return GlobalVariables.SPEEDED_UP_GAME_CLOCK_TICK_IN_MS;
		}
		return GlobalVariables.GAME_CLOCK_TICK_MS;
	}

	private void sleep(long sleepTime) {
		try {
			Thread.sleep(sleepTime);
		} catch (InterruptedException e) {
			throw new CargoSimulatorRuntimeException(e);
		}
	}
	
	private void tick() {
		if (clockState == ClockState.HOUR_TICK) {
			minutesPassedFromLastHour = 0;
			EventBusProvider.get().post(MinutePassedTimeEvent.instance());
			EventBusProvider.get().post(HourPassedTimeEvent.instance());
		} else {
			tickGameMinute();
			checkIfHourPassed();
		}
	}

	private void tickGameMinute() {
		EventBusProvider.get().post(MinutePassedTimeEvent.instance());
		minutesPassedFromLastHour++;
	}

	private void checkIfHourPassed() {
		if (minutesPassedFromLastHour >= MINUTES_IN_HOUR) {
			EventBusProvider.get().post(HourPassedTimeEvent.instance());
			minutesPassedFromLastHour = 0;
		}
	}

	public void setClockState(ClockState clockState) {
		LOG.info("Changing clock state: " + clockState);
		this.clockState = clockState;
		startThreadIfNeed();
	}
	
	private void startThreadIfNeed() {
		if (clockThread == null || !clockThread.isAlive()) {
			clockThread = new Thread(this);
			clockThread.start();
		}
	}

}
