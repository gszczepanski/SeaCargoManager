package szczepanski.gerard.cargo.simulator.engine.computer.intelligence;

import java.util.Set;

import org.springframework.stereotype.Component;

import lombok.Getter;
import szczepanski.gerard.cargo.simulator.common.collections.CollectionFactory;

@Component
public class ComputerIntelligencePool {
	
	@Getter
	private final Set<ComputerIntelligence> computerIntelligenceSet = CollectionFactory.set();
	
	public void register(ComputerIntelligence computerIntelligence) {
		computerIntelligenceSet.add(computerIntelligence);
	}
	
}
