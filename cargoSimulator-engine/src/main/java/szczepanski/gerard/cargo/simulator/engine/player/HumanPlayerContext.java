package szczepanski.gerard.cargo.simulator.engine.player;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class HumanPlayerContext { 
	private static HumanPlayerContext HUMAN_PLAYER_CONTEXT;
	
	private final Player player;
	
	public static HumanPlayerContext get() {
		return HUMAN_PLAYER_CONTEXT;
	}
	
	public static void newInstance(Player player) {
		HUMAN_PLAYER_CONTEXT = new HumanPlayerContext(player);
	}
	
}
