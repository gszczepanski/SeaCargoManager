package szczepanski.gerard.cargo.simulator.storage.service.storage;

import java.io.Serializable;
import java.util.Collection;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import szczepanski.gerard.cargo.simulator.simulator.domain.location.City;
import szczepanski.gerard.cargo.simulator.simulator.domain.location.Country;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.Currency;
import szczepanski.gerard.cargo.simulator.simulator.domain.port.Port;
import szczepanski.gerard.cargo.simulator.simulator.domain.route.Route;
import szczepanski.gerard.cargo.simulator.simulator.domain.ship.Ship;

@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
class SavedDomainDataDump implements Serializable {
	private static final long serialVersionUID = -8048455401396061274L;
	
	private final Collection<City> cityCollection;
	private final Collection<Country> countryCollection;
	private final Collection<Currency> currencyCollection;
	private final Collection<Port> portCollection;
	private final Collection<Ship> shipCollection;
	private final Collection<Route> routeCollection;
	
}
