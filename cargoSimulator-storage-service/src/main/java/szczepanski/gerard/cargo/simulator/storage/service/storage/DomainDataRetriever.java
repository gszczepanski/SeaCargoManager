package szczepanski.gerard.cargo.simulator.storage.service.storage;

@FunctionalInterface
interface DomainDataRetriever {
	
	SavedDomainDataDump createSnapshot();
	
}
