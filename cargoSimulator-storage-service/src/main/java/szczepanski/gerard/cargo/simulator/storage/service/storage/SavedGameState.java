package szczepanski.gerard.cargo.simulator.storage.service.storage;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
class SavedGameState implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final String GAME_SAVE_FILE_EXTENSION = ".sim";
	
	private final String name;
	private final String fileName;
	private final LocalDateTime saveDate;
	private final SavedDomainDataDump savedDataDump;
	private final SavedEngineState savedEngineState;
	
	public static String createFileName(String saveName) {
		return saveName + GAME_SAVE_FILE_EXTENSION;
	}
	
}
