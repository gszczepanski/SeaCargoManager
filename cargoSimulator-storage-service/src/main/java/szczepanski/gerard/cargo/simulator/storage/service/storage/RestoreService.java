package szczepanski.gerard.cargo.simulator.storage.service.storage;

@FunctionalInterface
public interface RestoreService {
	
	void restoreGameState(String saveName);
	
}
