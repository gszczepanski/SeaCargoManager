package szczepanski.gerard.cargo.simulator.storage.service.storage;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
class SavedEngineState implements Serializable {
	private static final long serialVersionUID = -1754569593513371766L;
	
	private LocalDateTime gameTime;
	
}
