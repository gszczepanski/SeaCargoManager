package szczepanski.gerard.cargo.simulator.storage.service.storage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.time.LocalDateTime;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.config.GlobalVariables;
import szczepanski.gerard.cargo.simulator.common.util.ParamAssertion;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class StorageServiceImpl implements StorageService {
	private static final Logger LOG = Logger.getLogger(StorageService.class);
	
	private final DomainDataRetriever domainDataRetriever;
	private final EngineDataRetriever engineDataRetriever;

	@Override
	public void saveGameState(String saveName) {
		ParamAssertion.isNotBlank(saveName);
		SavedGameState save = createSave(saveName);
		storeOnDisk(save);
		LOG.info("Game succeessfuly saved as : " + save.getName());
	}
	
	private SavedGameState createSave(String saveName) {
		return SavedGameState.builder()
				.name(saveName)
				.fileName(SavedGameState.createFileName(saveName))
				.saveDate(LocalDateTime.now())
				.savedDataDump(domainDataRetriever.createSnapshot())
				.savedEngineState(engineDataRetriever.createSnapshot())
				.build();
	}
	
	private void storeOnDisk(SavedGameState save) {
		File file = new File(GlobalVariables.SAVE_FOLDER_PATH + save.getFileName());
		
		ObjectOutputStream oos;
		try {
			oos = new ObjectOutputStream(new FileOutputStream(file));
			oos.writeObject(save);
			oos.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
}
