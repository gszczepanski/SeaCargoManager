package szczepanski.gerard.cargo.simulator.storage.service.storage;

@FunctionalInterface
public interface StorageService {
	
	void saveGameState(String saveName);
	
}
