package szczepanski.gerard.cargo.simulator.storage.service.storage;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import szczepanski.gerard.cargo.simulator.simulator.domain.common.time.SimulatorTime;

@Component
class EngineDataRetrieverImpl implements EngineDataRetriever {
	private static final Logger LOG = Logger.getLogger(EngineDataRetriever.class);
	
	@Override
	public SavedEngineState createSnapshot() {
		LOG.info("Creating game engine state snapshot");
		
		return SavedEngineState.builder()
				.gameTime(SimulatorTime.getTime())
				.build();
	}

}
