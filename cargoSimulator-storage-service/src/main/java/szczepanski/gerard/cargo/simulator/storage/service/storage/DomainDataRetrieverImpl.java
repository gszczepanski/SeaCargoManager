package szczepanski.gerard.cargo.simulator.storage.service.storage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.collections.CollectionFactory;
import szczepanski.gerard.cargo.simulator.simulator.domain.location.CityRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.location.CountryRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.payment.CurrencyRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.port.PortRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.route.RouteRepository;
import szczepanski.gerard.cargo.simulator.simulator.domain.ship.ShipRepository;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class DomainDataRetrieverImpl implements DomainDataRetriever {
	private static final Logger LOG = Logger.getLogger(DomainDataRetriever.class);

	private final CityRepository cityRepository;
	private final CountryRepository countryRepository;
	private final CurrencyRepository currencyRepository;
	private final ShipRepository shipRepository;
	private final PortRepository portRepository;
	private final RouteRepository routeRepository;
	
	@Override
	public SavedDomainDataDump createSnapshot() {
		LOG.info("Creating Domain data dump snapshot");
		
		return SavedDomainDataDump.builder()
				.cityCollection(CollectionFactory.serializableSet(cityRepository.getAll()))
				.countryCollection(CollectionFactory.serializableSet(countryRepository.getAll()))
				.currencyCollection(CollectionFactory.serializableSet(currencyRepository.getAll()))
				.shipCollection(CollectionFactory.serializableSet(shipRepository.getAll()))
				.portCollection(CollectionFactory.serializableSet(portRepository.getAll()))
				.routeCollection(CollectionFactory.serializableSet(routeRepository.getAll()))
				.build();
	}
}
