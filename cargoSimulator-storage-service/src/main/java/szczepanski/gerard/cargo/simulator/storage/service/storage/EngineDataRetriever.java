package szczepanski.gerard.cargo.simulator.storage.service.storage;

@FunctionalInterface
interface EngineDataRetriever {
	
	SavedEngineState createSnapshot();
	
}
