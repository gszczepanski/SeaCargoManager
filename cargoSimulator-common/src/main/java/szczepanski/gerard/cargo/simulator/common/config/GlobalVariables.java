package szczepanski.gerard.cargo.simulator.common.config;

import java.time.LocalDateTime;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class GlobalVariables {
	
	public static final String PROGRAM_TITLE = "Sea Cargo Manager";
	public static final String PROGRAM_VERSION = "0.0.3";
	public static final String PROGRAM_ICON_PATH = "/images/simulator/seaCargoManagerIcon.png";
	public static final String DISPLAY_PROGRAM_TITLE = PROGRAM_TITLE + " " + PROGRAM_VERSION;
	
	public static final String ROOT_PACKAGE = "szczepanski.gerard.cargo.simulator";
	public static final String MODEL_FOLDER_PATH = "model/";
	public static final String SAVE_FOLDER_PATH = "save/";
	
	public static final String PROGRAM_SETTINGS_PROPERTIES_PATH = "./settings.properties";
	
	public static final long GAME_CLOCK_TICK_MS = 500;
	public static final long SPEEDED_UP_GAME_CLOCK_TICK_IN_MS = 50;
	
	public static final LocalDateTime DEFAULT_GAME_DATE_START = LocalDateTime.of(2015, 01, 01, 0, 0, 0);
	
	public static final String MAP_PORT_COLOR = "#00AA11";
	public static final String MAP_SHIP_COLOR = "#9a6447";
	
}
