package szczepanski.gerard.cargo.simulator.common.config;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DevelopmentVariables {

	public static final Integer CONSOLE_COMMUNICATION_PORT = 6789;
	public static final String LOCATION_POINT_MAP_COLOR = "#9a2d2d";
	
	public static boolean SHOW_ROUTES_GRAPH_FOR_DEV = false;

}
