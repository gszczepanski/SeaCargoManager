package szczepanski.gerard.cargo.simulator.common.util;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.exception.CargoSimulatorRuntimeException;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class XmlUtils {

	public static Document loadXmlDocument(String xmlPath) {
		File xmlFile = new File(xmlPath);
		DocumentBuilder documentBuilder = getDocumentBuilder();
		try {
			return documentBuilder.parse(xmlFile);
		} catch (SAXException | IOException e) {
			throw new CargoSimulatorRuntimeException(e);
		}
	}

	private static DocumentBuilder getDocumentBuilder() {
		try {
			return DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			throw new CargoSimulatorRuntimeException(e);
		}
	}
	
	public static String getTagValue(String tagName, Element element) {
		return element.getElementsByTagName(tagName).item(0).getTextContent();
	}
	
	public static NodeList loadNodeListFromSpecifiedTag(String xmlPath, String tagName) {
		Document xmlDocument = XmlUtils.loadXmlDocument(xmlPath);
		xmlDocument.getDocumentElement().normalize();
		return xmlDocument.getElementsByTagName(tagName);
	}
	
}
