package szczepanski.gerard.cargo.simulator.common.config;

import javafx.scene.image.Image;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class GlobalImages {
	
	public static final Image PROGRAM_ICON = new Image(GlobalVariables.class.getResourceAsStream(GlobalVariables.PROGRAM_ICON_PATH));
	
}
