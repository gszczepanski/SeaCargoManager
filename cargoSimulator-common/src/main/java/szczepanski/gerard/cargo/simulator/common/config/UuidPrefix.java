package szczepanski.gerard.cargo.simulator.common.config;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UuidPrefix {
	
	public static final String CURRENCY_UUID_PREFIX = "currency";
	public static final String COUNTRY_UUID_PREFIX = "country";
	public static final String CITY_UUID_PREFIX = "city";
	public static final String PORT_UUID_PREFIX = "port";
	public static final String SHIP_UUID_PREFIX = "ship";
	public static final String ROUTE_UUID_PREFIX = "route";
	public static final String MESSAGE_UUID_PREFIX = "msg";

}
