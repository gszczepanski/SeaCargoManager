package szczepanski.gerard.cargo.simulator.common.util;

import java.util.Random;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RandomUtils {

	private static final Random R = new Random();
	
	public static int randomFromRange(int min, int max) {
		return min + R.nextInt(max - min + 1);
	}
	
	public static String randomColor() {
		int nextInt = R.nextInt(256*256*256);
        return String.format("#%06x", nextInt).toUpperCase();
	}
}
