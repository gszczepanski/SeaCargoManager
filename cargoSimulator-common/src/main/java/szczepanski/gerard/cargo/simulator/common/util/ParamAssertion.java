package szczepanski.gerard.cargo.simulator.common.util;

import java.util.Collection;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.exception.CargoSimulatorRuntimeException;
import szczepanski.gerard.cargo.simulator.common.exception.ParamAssertionException;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ParamAssertion {

	public static void isNotNull(Object... object) {
		for (Object o : object) {
			if (o == null) {
				throw new ParamAssertionException("Assertion failed! Object is null!");
			}
		}
	}

	public static void collectionContainsAtLeastElements(Collection<?> collection, int minimumNumberOfElements) {
		if (collection == null) {
			throw new ParamAssertionException("Assertion failed! Collection is empty!");
		}

		if (collection.size() < minimumNumberOfElements) {
			throw new CargoSimulatorRuntimeException(
					String.format("Assertion failed! Collection has'nt required minimum %d number of elements", minimumNumberOfElements));
		}
	}

	public static void checkIfTheSame(Object first, Object second) {
		if (!first.equals(second)) {
			throw new ParamAssertionException("Assertion failed! Objects are not the same");
		}
	}

	public static void isNumberRepresentation(String value) {
		try {
			Double.parseDouble(value);
		} catch (NumberFormatException nfe) {
			throw new ParamAssertionException(String.format("Given String value: %s is not numeric!", value));
		}
	}
	
	public static void isNotBlank(String text) {
		if (text == null || text == "") {
			throw new ParamAssertionException("String is blank!");
		}
	}
	
	public static void isNotBlank(String... texts) {
		for (int i = 0; i < texts.length; i++) {
			isNotBlank(texts[i]);
		}
	}
}
