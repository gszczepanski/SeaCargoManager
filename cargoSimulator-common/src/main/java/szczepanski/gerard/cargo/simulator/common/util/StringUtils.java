package szczepanski.gerard.cargo.simulator.common.util;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class StringUtils {
	
	public static final String EMPTY = "";
	public static final String SPACE = " ";
	private static final String WHITE_SPACE = "\\s+";
	
	private static final DecimalFormat DECIMAL_FORMATTER = new DecimalFormat("#,###.00");
	
	public static boolean isBlank(String string) {
		return !(string != null && string != EMPTY);
	}
	
	public static List<String> splitBySpace(String command) {
		String[] tokens = command.split(WHITE_SPACE);
		return new ArrayList<>(Arrays.asList(tokens));
	}
	
	public static String formatNumeric(double numeric) {
		return DECIMAL_FORMATTER.format(numeric);
	}
	
}
