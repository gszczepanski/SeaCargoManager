package szczepanski.gerard.cargo.simulator.common.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.StringUtils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DateTimeUtils {

	private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm"); 
	private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy"); 
	private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");

	public static String getDate(LocalDateTime dateTime) {
		return dateTime.format(DATE_FORMATTER);
	}

	public static String getTime(LocalDateTime dateTime) {
		return dateTime.format(TIME_FORMATTER);
	}
	
	public static String getDateTime(LocalDateTime dateTime) {
		return dateTime.format(DATE_TIME_FORMATTER);
	}
	
	public static String getDayOfWeek(LocalDateTime dateTime) {
		return StringUtils.capitalize(dateTime.getDayOfWeek().name().toLowerCase());
	}
	
	public static LocalTime parseTime(String time) {
		return LocalTime.parse(time, TIME_FORMATTER);
	}
	
	public static LocalDate parseDate(String time) {
		return LocalDate.parse(time, DATE_FORMATTER);
	}
	
	public static LocalDateTime parseDateTime(String dateTime) {
		return LocalDateTime.parse(dateTime, DATE_TIME_FORMATTER);
	}
}
