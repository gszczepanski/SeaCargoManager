package szczepanski.gerard.cargo.simulator.common.exception;

public class CargoSimulatorException extends Throwable {
	private static final long serialVersionUID = -6453752999550627078L;

	public CargoSimulatorException() {
		super();
	}

	public CargoSimulatorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public CargoSimulatorException(String message, Throwable cause) {
		super(message, cause);
	}

	public CargoSimulatorException(String message) {
		super(message);
	}

	public CargoSimulatorException(Throwable cause) {
		super(cause);
	}

}
