package szczepanski.gerard.cargo.simulator.common.context;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.config.GlobalVariables;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ApplicationContextProvider {
	
	private static AnnotationConfigApplicationContext context;
	
	public static <T> T getBean(Class<T> requiredType) {
		return context.getBean(requiredType);
	}
	
	public static void loadContext() {
		context = new AnnotationConfigApplicationContext();
		context.scan(new String[] { GlobalVariables.ROOT_PACKAGE });
		context.refresh();
	}
	
}
