package szczepanski.gerard.cargo.simulator.route.graph.editor.view;

import java.util.List;

import szczepanski.gerard.cargo.simulator.route.graph.editor.service.LocationMapPointDto;

final class WorldMapPointGenerator {

	private static final String POINT_TEMPLATE = "{latLng: [%s, %s], uuid: '%s', name: '%s', style: {r: 5, fill:'%s'}}";

	public String generatePoint(LocationMapPointDto dto) {
		return String.format("var m = " + POINT_TEMPLATE + ";", dto.getLatitude(), dto.getLongitude(), dto.getUuid(), dto.getLabel(), dto.getColor());
	}
	
	public String convertToJsPointsString(List<LocationMapPointDto> dtos) {
		StringBuilder builder = new StringBuilder("var markers = [");
		
		dtos.forEach(d -> {
			builder.append(generatePoint(d));
			builder.append(",");
		});
		
		String generatedString = builder.toString();
		String updatedString = generatedString.substring(0, generatedString.length() - 1) + "];";
		return updatedString;
	}

}
