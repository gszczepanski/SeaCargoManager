package szczepanski.gerard.cargo.simulator.route.graph.editor.view;

import javafx.scene.input.KeyCode;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import szczepanski.gerard.cargo.simulator.route.graph.editor.service.NewPointDirection;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
class KeyToPointDirectionConverter {
	
	public static NewPointDirection toPointDirection(KeyCode key) {
		NewPointDirection direction = NewPointDirection.UP;
		
		switch (key) {
		case W:
			direction = NewPointDirection.UP;
			break;
		case D:
			direction = NewPointDirection.RIGHT;
			break;
		case S:
			direction = NewPointDirection.DOWN;
			break;
		case A:
			direction = NewPointDirection.LEFT;
			break;
		default:
			break;
		}
		
		return direction;
	}
	
}
