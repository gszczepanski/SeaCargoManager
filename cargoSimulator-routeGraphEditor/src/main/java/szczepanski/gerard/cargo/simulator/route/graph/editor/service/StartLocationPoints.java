package szczepanski.gerard.cargo.simulator.route.graph.editor.service;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class StartLocationPoints {
	
	public static final LocationMapPointDto TOP_LEFT_POINT = new LocationMapPointDto("83.14619609954526", "-165.6161548123539");
	public static final LocationMapPointDto TOP_RIGHT_POINT = new LocationMapPointDto("83.14619609954526", "188.36262705839258");
	public static final LocationMapPointDto BOTTOM_LEFT_POINT = new LocationMapPointDto("-54.541856452327394", "-165.6161548123539");
	public static final LocationMapPointDto BOTTOM_RIGHT_POINT = new LocationMapPointDto("-54.541856452327594", "188.36262705839258");
	
}
