package szczepanski.gerard.cargo.simulator.route.graph.editor.service;

import java.util.concurrent.atomic.AtomicLong;

import com.google.common.base.Strings;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class LocationMapPointDto {
	
	private static final AtomicLong ID_GEN = new AtomicLong(0);
	
	private final String color = "#be29ec";
	private String uuid = generateUuid();
	private String label = "";
	
	private String latitude;
	private String longitude;
	
	public LocationMapPointDto(String latitude, String longitude) {
		this.setLatitude(latitude);
		this.setLongitude(longitude);
	}
	
	public LocationMapPointDto copy() {
		LocationMapPointDto dto = new LocationMapPointDto();
		
		dto.setLabel(label);
		dto.setLatitude(latitude);
		dto.setLongitude(longitude);
		
		return dto;
	}
	
	private static String generateUuid() {
		Long idSuffix = ID_GEN.incrementAndGet();
		return Strings.padStart(idSuffix.toString(), 5, '0');
	}
}
