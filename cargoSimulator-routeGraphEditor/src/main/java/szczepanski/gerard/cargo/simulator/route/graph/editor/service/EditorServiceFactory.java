package szczepanski.gerard.cargo.simulator.route.graph.editor.service;

public class EditorServiceFactory {
	
	public EditorService create() {
		return new EditorServiceImpl();
	}
	
}
