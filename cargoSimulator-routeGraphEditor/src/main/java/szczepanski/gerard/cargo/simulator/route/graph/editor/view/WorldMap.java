package szczepanski.gerard.cargo.simulator.route.graph.editor.view;

import java.util.List;

import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import lombok.Getter;
import szczepanski.gerard.cargo.simulator.route.graph.editor.service.LocationMapPointDto;

public class WorldMap {

	private static final String WORLD_MAP_VIEW_PATH = WorldMap.class.getResource("/component/worldMap/worldMap.html").toString();

	@Getter
	private final WebView webView;
	private final WorldMapPointGenerator worldMapPointGenerator;
	private final EditorSceneController sourceController;
	final WebEngine webEngine;

	private WorldMap(EditorSceneController sourceController) {
		this.webView = new WebView();
		this.webEngine = webView.getEngine();
		this.webEngine.setJavaScriptEnabled(true);
		this.worldMapPointGenerator = new WorldMapPointGenerator();
		this.sourceController = sourceController;
	}

	public static WorldMap initialize(EditorSceneController sourceController) {
		WorldMap worldMap = new WorldMap(sourceController);

		worldMap.webView.setMinWidth(1600);
		worldMap.webView.setMinHeight(850);
		worldMap.webView.setContextMenuEnabled(false);
		worldMap.webEngine.load(WORLD_MAP_VIEW_PATH);
		WorldMapJavaCaller.installCaller("javaCaller", worldMap);
		
		return worldMap;
	}

	public void updateMap(List<LocationMapPointDto> mapPoints) {
		String mapPointsInJs = worldMapPointGenerator.convertToJsPointsString(mapPoints);
		String script = mapPointsInJs + "generateMarkers(markers);";
		webEngine.executeScript(script);
	}
	
	public void removeSelectedPoints() {
		String script = "removeSelectedMarkers();";
		webEngine.executeScript(script);
	}
	
	void addPointByClick(String latitude, String longitude) {
		sourceController.addPointByClick(latitude, longitude);
	}
	
	void publishPointToMap(LocationMapPointDto newPointDto) {
		String pointJs = worldMapPointGenerator.generatePoint(newPointDto);
		String script = pointJs + "addMarker(m);";
		webEngine.executeScript(script);
	}
	
	public void clearAllPoints() {
		String script = "clearAllPoints();";
		webEngine.executeScript(script);
	}
	
	void callSelectPointByClickFromMap(String selectedPointUuid) {
		sourceController.setSelectedPoint(selectedPointUuid);
	}
	
	void callRemoveSelectedPointFromMap(String selectedPointUuid) {
		sourceController.removeSelectedPoint(selectedPointUuid);
	}
	
}
