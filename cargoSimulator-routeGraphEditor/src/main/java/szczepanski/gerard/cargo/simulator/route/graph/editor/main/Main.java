package szczepanski.gerard.cargo.simulator.route.graph.editor.main;

import org.apache.log4j.Logger;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import szczepanski.gerard.cargo.simulator.route.graph.editor.service.EditorService;
import szczepanski.gerard.cargo.simulator.route.graph.editor.service.EditorServiceFactory;
import szczepanski.gerard.cargo.simulator.route.graph.editor.view.EditorSceneController;
import szczepanski.gerard.cargo.simulator.route.graph.editor.view.EditorSceneFactory;

public class Main extends Application {
	private static final Logger LOG = Logger.getLogger(Main.class);
	
	private static final String TITLE = "SeaCargoSimulator Route Graph Editor"; 
	
	private static Scene EDITOR_SCENE;
	
	public static void main(String... args) {
		loadComponents();
		launch(args);
	}

	private static void loadComponents() {
		LOG.info("Loading components");
		EditorServiceFactory editorServiecFactory = new EditorServiceFactory();
		EditorService editorService = editorServiecFactory.create();
		EditorSceneController controller = new EditorSceneController(editorService);
		EditorSceneFactory editorSceneFactory = new EditorSceneFactory();
		EDITOR_SCENE = editorSceneFactory.createScene(controller);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent e) {
				Platform.exit();
				System.exit(0);
			}
		});
		
		primaryStage.setTitle(TITLE);
		primaryStage.setResizable(false);
		primaryStage.setScene(EDITOR_SCENE);
		primaryStage.show();
	}

}
