/**
 * 
 */
package szczepanski.gerard.cargo.simulator.route.graph.editor.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.stage.Stage;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class FxmlUtils {
	
	public static final Stage getStageFromActionEvent(ActionEvent event) {
		 return (Stage) ((Node) event.getSource()).getScene().getWindow();
	}
	
	public static final FXMLLoader getFxmlLoader(String fxmlPath) {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(FxmlUtils.class.getResource(fxmlPath));
		return loader;
	}
}
