package szczepanski.gerard.cargo.simulator.route.graph.editor.service;

/**
 * This exception is being thrown while add new point operation if point already exist with given coordinates.
 * May be also thrown if any point is selected.
 * @author Gerard Szczepanski
 */
public class PointCanNotBeAddedException extends Throwable {
	private static final long serialVersionUID = -5327358424575200488L;

}
