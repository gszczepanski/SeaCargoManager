package szczepanski.gerard.cargo.simulator.route.graph.editor.view;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import szczepanski.gerard.cargo.simulator.route.graph.editor.exception.CargoSimulatorRuntimeException;

public class EditorSceneFactory {
	
	private static final String SCENE_PATH = "/templates/scene/EditorScene.fxml";
	
	public Scene createScene(EditorSceneController worldMapPaneController) {
		FXMLLoader loader = FxmlUtils.getFxmlLoader(SCENE_PATH);
		loader.setController(worldMapPaneController);
		
		try {
			Pane rootPane= (Pane) loader.load();
			return new Scene(rootPane);
		} catch (IOException e) {
			throw new CargoSimulatorRuntimeException(e);
		}
	}
	
}
