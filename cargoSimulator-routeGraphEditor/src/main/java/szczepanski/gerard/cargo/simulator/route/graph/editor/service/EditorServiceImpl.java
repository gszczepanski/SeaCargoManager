package szczepanski.gerard.cargo.simulator.route.graph.editor.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

class EditorServiceImpl implements EditorService {
	private static final Logger LOG = Logger.getLogger(EditorService.class);

	private Map<String, LocationMapPointDto> currentLocationPoints = new HashMap<>(1000);
	private Map<Geocoordinates, String> occupiedCoordinatesMap = new HashMap<>(1000);
	private Double latitudePointDistance = 1.0;
	private Double longitudePointDistance = 1.0;
	private LocationMapPointDto selectedPoint;

	@Override
	public LocationMapPointDto addPoint(String latitude, String longitude) {
		LOG.info(String.format("Adding point with lat: %s, long: %s", latitude, longitude));
		LocationMapPointDto dto = new LocationMapPointDto(latitude, longitude);

		String uuid = dto.getUuid();
		Geocoordinates geoCoord = new Geocoordinates(dto.getLatitude(), dto.getLongitude());

		currentLocationPoints.put(uuid, dto);
		occupiedCoordinatesMap.put(geoCoord, uuid);
		
		selectedPoint = dto;
		
		return dto;
	}

	@Override
	public LocationMapPointDto addPoint(NewPointDirection newPointDirection) throws PointCanNotBeAddedException {
		LOG.info(String.format("Adding point with point direction: %s", newPointDirection));
		checkIfAnyPointIsSelected();
		Geocoordinates newPointCoordinates = calculateNewPointCoordinates(newPointDirection);
		checkIfCoordinatesNotOccupied(newPointCoordinates);
		
		return addPoint(newPointCoordinates.getLatitude(), newPointCoordinates.getLongitude());
	}

	private void checkIfAnyPointIsSelected() throws PointCanNotBeAddedException {
		if (selectedPoint == null) {
			LOG.info("No point selected. Can not create new point");
			throw new PointCanNotBeAddedException();
		}
	}

	private Geocoordinates calculateNewPointCoordinates(NewPointDirection newPointDirection) {
		Double doubleLatitude;
		Double newPointLatitude;
		Double doubleLongitude;
		Double newPointLongitude;
		
		String latitude = selectedPoint.getLatitude();
		String longitude = selectedPoint.getLongitude();

		switch (newPointDirection) {
		case UP:
			doubleLatitude = Double.valueOf(latitude);
			newPointLatitude = doubleLatitude + latitudePointDistance;
			latitude = newPointLatitude.toString();
			break;
		case RIGHT:
			doubleLongitude = Double.valueOf(longitude);
			newPointLongitude = doubleLongitude + longitudePointDistance;
			longitude = newPointLongitude.toString();
			break;

		case DOWN:
			doubleLatitude = Double.valueOf(latitude);
			newPointLatitude = doubleLatitude - latitudePointDistance;
			latitude = newPointLatitude.toString();
			break;

		case LEFT:
			doubleLongitude = Double.valueOf(longitude);
			newPointLongitude = doubleLongitude - longitudePointDistance;
			longitude = newPointLongitude.toString();
			break;

		default:
			break;
		}

		return new Geocoordinates(latitude, longitude);
	}
	
	private void checkIfCoordinatesNotOccupied(Geocoordinates geoCoordinates) throws PointCanNotBeAddedException {
		if (occupiedCoordinatesMap.containsKey(geoCoordinates)) {
			LOG.info("Coordinates of the new point are actualy occupied");
			throw new PointCanNotBeAddedException();
		}
	}

	@Override
	public void removePoint(String uuid) {
		LOG.info("Remove Point with uuid: " + uuid);
		LocationMapPointDto pointToRemove = currentLocationPoints.get(uuid);
		Geocoordinates geoCoord = new Geocoordinates(pointToRemove.getLatitude(), pointToRemove.getLongitude());
		
		currentLocationPoints.remove(uuid);
		currentLocationPoints.remove(geoCoord);
		
		selectedPoint = null;
	}
	
	@Override
	public void removeAllPoints() {
		LOG.info("Removing all points");
		currentLocationPoints.clear();
		occupiedCoordinatesMap.clear();
		selectedPoint = null;
	}

	@Override
	public void setPointDistances(Double latitudeDistance, Double longitudeDistance){
		LOG.info(String.format("Set point distance latitude: %s, longitude: %s", latitudeDistance, longitudeDistance));
		this.latitudePointDistance = latitudeDistance;
		this.longitudePointDistance = longitudeDistance;
	}

	@Override
	public void markPointAsSelect(String uuid) {
		LOG.info("Mark point selected with uuid: " + uuid);
		this.selectedPoint = currentLocationPoints.get(uuid);
	}

}
