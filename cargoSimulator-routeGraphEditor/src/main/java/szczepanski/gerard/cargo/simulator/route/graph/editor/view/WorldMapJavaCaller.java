package szczepanski.gerard.cargo.simulator.route.graph.editor.view;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker.State;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import netscape.javascript.JSObject;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class WorldMapJavaCaller {
	
	private final WorldMap source;

	static void installCaller(String callerName, WorldMap source) {
		WorldMapJavaCaller caller = new WorldMapJavaCaller(source);
		
		source.webEngine.getLoadWorker().stateProperty().addListener(new ChangeListener<State>() {

			@Override
			public void changed(ObservableValue<? extends State> ov, State oldState, State newState) {
				if (newState == State.SUCCEEDED) {
					JSObject window = (JSObject) source.webEngine.executeScript("window");
					window.setMember(callerName, caller);
				}
			}
		});
		
		JSObject window = (JSObject) source.webEngine.executeScript("window");
        window.setMember(callerName, caller);
	}
	
	public void addPointByClick(String latitude, String longitude) {
		source.addPointByClick(latitude, longitude);
	}
	
	public void selectPointByClick(String uuid) {
		source.callSelectPointByClickFromMap(uuid);
	}
	
	public void removeSelectedPoint(String uuid) {
		source.callRemoveSelectedPointFromMap(uuid);
	}
	
}
