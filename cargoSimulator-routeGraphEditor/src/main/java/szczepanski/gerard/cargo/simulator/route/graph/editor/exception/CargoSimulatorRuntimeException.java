package szczepanski.gerard.cargo.simulator.route.graph.editor.exception;

public class CargoSimulatorRuntimeException extends RuntimeException {

	private static final long serialVersionUID = -293455483678518063L;

	public CargoSimulatorRuntimeException() {
		super();
	}

	public CargoSimulatorRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public CargoSimulatorRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	public CargoSimulatorRuntimeException(String message) {
		super(message);
	}

	public CargoSimulatorRuntimeException(Throwable cause) {
		super(cause);
	}
	
	
	
}
