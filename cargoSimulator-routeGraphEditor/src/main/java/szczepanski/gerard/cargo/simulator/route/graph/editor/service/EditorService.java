package szczepanski.gerard.cargo.simulator.route.graph.editor.service;

public interface EditorService {
	
	LocationMapPointDto addPoint(String latitude, String longitude);
	
	void removePoint(String uuid);
	
	void removeAllPoints();
	
	void setPointDistances(Double latitudeDistance, Double longitudeDistance);
	
	void markPointAsSelect(String uuid);
	
	LocationMapPointDto addPoint(NewPointDirection newPointDirection) throws PointCanNotBeAddedException;
	
}
