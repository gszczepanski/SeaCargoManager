package szczepanski.gerard.cargo.simulator.route.graph.editor.view;

import org.apache.log4j.Logger;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.route.graph.editor.service.EditorService;
import szczepanski.gerard.cargo.simulator.route.graph.editor.service.LocationMapPointDto;
import szczepanski.gerard.cargo.simulator.route.graph.editor.service.NewPointDirection;
import szczepanski.gerard.cargo.simulator.route.graph.editor.service.PointCanNotBeAddedException;

@RequiredArgsConstructor
public class EditorSceneController {

	private static final Logger LOG = Logger.getLogger(EditorSceneController.class);

	private final EditorService editorService;

	@FXML
	private Pane worldMapPane;
	@FXML
	private TextField latitudePointDistanceTextField;
	@FXML
	private TextField longitudePointDistanceTextField;

	private WorldMap worldMap;

	@FXML
	public void initialize() {
		initWorldMapPane();
	}

	@FXML
	public void removeSelectedPoints() {
		worldMap.removeSelectedPoints();
	}

	@FXML
	public void setPointDistances() {
		Double latitudeDistance = Double.valueOf(latitudePointDistanceTextField.getText());
		Double longitudeDistance = Double.valueOf(longitudePointDistanceTextField.getText());
		editorService.setPointDistances(latitudeDistance, longitudeDistance);
	}

	@FXML
	public void handleKeyPressed(KeyEvent e) {
		KeyCode key = e.getCode();

		if (AllowedKeyboardKeys.isKeyAllowed(key)) {
			NewPointDirection newPointDirection = KeyToPointDirectionConverter.toPointDirection(key);
			addNewPoint(newPointDirection);
		}
	}

	@FXML
	public void clearAllPoints() {
		editorService.removeAllPoints();
		Platform.runLater(() -> {
			worldMap.clearAllPoints();
		});
	}

	private void addNewPoint(NewPointDirection newPointDirection) {
		try {
			LocationMapPointDto addedPoint = editorService.addPoint(newPointDirection);
			Platform.runLater(() -> {
				worldMap.publishPointToMap(addedPoint);
			});
		} catch (PointCanNotBeAddedException e) {
			LOG.info("Point can not be added");
		}
	}

	private void initWorldMapPane() {
		Platform.runLater(() -> {
			worldMap = WorldMap.initialize(this);
			worldMapPane.getChildren().addAll(worldMap.getWebView());
		});
	}

	void addPointByClick(String latitude, String longitude) {
		Platform.runLater(() -> {
			LocationMapPointDto addedPoint = editorService.addPoint(latitude, longitude);
			worldMap.publishPointToMap(addedPoint);
		});
	}

	void setSelectedPoint(String selectedPointUuid) {
		editorService.markPointAsSelect(selectedPointUuid);
	}

	void removeSelectedPoint(String selectedPointUuid) {
		editorService.removePoint(selectedPointUuid);
	}

}
