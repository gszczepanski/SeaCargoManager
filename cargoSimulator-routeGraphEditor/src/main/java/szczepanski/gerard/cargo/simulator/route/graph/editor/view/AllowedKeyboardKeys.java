package szczepanski.gerard.cargo.simulator.route.graph.editor.view;

import java.util.Arrays;
import java.util.List;

import javafx.scene.input.KeyCode;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
class AllowedKeyboardKeys {
	
	private static final List<KeyCode> ALLOWED_KEYS;
	
	static {
		ALLOWED_KEYS = Arrays.asList(KeyCode.A, KeyCode.W, KeyCode.S, KeyCode.D);
	}
	
	public static boolean isKeyAllowed(KeyCode key) {
		return ALLOWED_KEYS.contains(key);
	}
	
	
}
