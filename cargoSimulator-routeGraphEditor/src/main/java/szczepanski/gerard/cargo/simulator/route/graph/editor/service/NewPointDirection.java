package szczepanski.gerard.cargo.simulator.route.graph.editor.service;

public enum NewPointDirection {
	
	UP,
	RIGHT,
	DOWN,
	LEFT;
	
}
