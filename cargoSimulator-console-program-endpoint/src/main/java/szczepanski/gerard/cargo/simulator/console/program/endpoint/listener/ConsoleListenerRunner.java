package szczepanski.gerard.cargo.simulator.console.program.endpoint.listener;

public interface ConsoleListenerRunner {
	
	void startConsoleListening(Integer port);
	
}
