package szczepanski.gerard.cargo.simulator.console.program.endpoint.listener;

interface ConsoleListener {
	
	void listenToConsole(Integer port);

}
