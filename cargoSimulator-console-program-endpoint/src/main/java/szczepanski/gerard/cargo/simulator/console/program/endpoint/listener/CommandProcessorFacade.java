package szczepanski.gerard.cargo.simulator.console.program.endpoint.listener;

@FunctionalInterface
interface CommandProcessorFacade {
	
	String process(String command);
	
}
