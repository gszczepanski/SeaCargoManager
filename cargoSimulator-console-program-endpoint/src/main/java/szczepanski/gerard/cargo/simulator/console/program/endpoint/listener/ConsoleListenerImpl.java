package szczepanski.gerard.cargo.simulator.console.program.endpoint.listener;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.exception.CargoSimulatorRuntimeException;
import szczepanski.gerard.cargo.simulator.common.util.ParamAssertion;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class ConsoleListenerImpl implements ConsoleListener {
	private static final Logger LOG = Logger.getLogger(ConsoleListener.class);
	
	private final CommandProcessorFacade commandProcessorFacade;

	private ServerSocket serverSocket;
	private Socket consoleClientSocket;
	private DataInputStream in;
	private DataOutputStream out;

	@Override
	public void listenToConsole(Integer port) {
		ParamAssertion.isNotNull(port);
		startServerForListening(port);
		listen();
	}

	private void startServerForListening(Integer port) {
		serverSocket = createServerSocketOnPort(port);
		consoleClientSocket = createConsoleClientSocket();
		in = createInputStream();
		out = createOutputStream();
		LOG.info("Console Listener started. Listening on port " + port);
	}

	private ServerSocket createServerSocketOnPort(Integer port) {
		try {
			return new ServerSocket(port);
		} catch (IOException e) {
			throw new CargoSimulatorRuntimeException("Unable to create ConsoleListener ServerSocket", e);
		}
	}

	private Socket createConsoleClientSocket() {
		try {
			return serverSocket.accept();
		} catch (IOException e) {
			throw new CargoSimulatorRuntimeException("Unable to create ConsoleListener Socket", e);
		}
	}

	private DataOutputStream createOutputStream() {
		try {
			return new DataOutputStream(consoleClientSocket.getOutputStream());
		} catch (IOException e) {
			throw new CargoSimulatorRuntimeException("Unable to create ConsoleListener Input Stream", e);
		}
	}

	private DataInputStream createInputStream() {
		try {
			return new DataInputStream(consoleClientSocket.getInputStream());
		} catch (IOException e) {
			throw new CargoSimulatorRuntimeException("Unable to create ConsoleListener Input Stream", e);
		}
	}

	private void listen() {
		while (true) {
			try {
				String command = in.readUTF();
				LOG.info("Recieved command from console: " + command);
				String respondMessage = commandProcessorFacade.process(command);
				respond(respondMessage);
			} catch (IOException e) {
				LOG.info("Console disconected. Close Console Listener.");
				break;
			}
		}
	}

	private void respond(String message) throws IOException {
		out.writeUTF(message);
	}

}
