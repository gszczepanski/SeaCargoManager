package szczepanski.gerard.cargo.simulator.console.program.endpoint.listener;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class ConsoleListenerRunnerImpl implements ConsoleListenerRunner {
	private static final Logger LOG = Logger.getLogger(ConsoleListener.class);

	private final ConsoleListener consoleListener;
	
	@Override
	public void startConsoleListening(Integer port) {
		LOG.info("Running Console Listener...");
		
		Thread consoleListenerThread = new Thread(() -> {
			consoleListener.listenToConsole(port);
		});
		consoleListenerThread.start();
	}

}
