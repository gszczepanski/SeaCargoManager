package szczepanski.gerard.cargo.simulator.console.program.endpoint.listener;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.cargo.simulator.common.util.StringUtils;
import szczepanski.gerard.cargo.simulator.console.service.command.resolver.ConsoleCommandResolver;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class CommandProcessorFacadeImpl implements CommandProcessorFacade {
	private static final String COMMAND_IS_NOT_PROPER = "Command is not proper. Command need at least one word";
	
	private final ConsoleCommandResolver consoleCommandResolver;
	
	@Override
	public String process(String command) {
		if (StringUtils.isBlank(command)) {
			return COMMAND_IS_NOT_PROPER;
		}
		
		List<String> tokens = StringUtils.splitBySpace(command);
		if (tokens.isEmpty()) {
			return COMMAND_IS_NOT_PROPER;
		}
		
		return consoleCommandResolver.resolveTokens(tokens);
	}
}
